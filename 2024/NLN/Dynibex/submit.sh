#!/bin/bash
docker build . -t "dynibex-submit-$1" --no-cache
docker run --name "dynibex-submit-container-$1" "dynibex-submit-$1"
docker cp "dynibex-submit-container-$1":/benchmarks/results/ .
docker rm --force "dynibex-submit-container-$1"
docker image rm --force "dynibex-submit-$1"
