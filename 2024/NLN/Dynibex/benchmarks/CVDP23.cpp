#include <ibex.h>
#include <queue>
#include <chrono>
#include <iostream>
#include <fstream>
#include "vibes.cpp"

using namespace std::chrono;

using namespace ibex;
///Van der Pol

#define __PREC__ 1e-6
#define __METH__ RK4_SPEC
#define __DURATION__ 7.0 // 200.0

unsigned long int i = 0;
void toGunPlot (const simulation* sim) {

  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
    i++;
    std::cout << "set object " << i <<  " rect from " <<
      iterator_list->box_j1->operator[](0).lb() << ", " <<
      iterator_list->box_j1->operator[](1).lb() << " to " <<
      iterator_list->box_j1->operator[](0).ub() << ", " <<
      iterator_list->box_j1->operator[](1).ub() <<
      std::endl;
  }
}

void plot_simu(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
    vibes::drawBox(iterator_list->box_j1->operator[](0).lb(), iterator_list->box_j1->operator[](0).ub(),
		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(), "blue[blue]");
	vibes::drawBox(iterator_list->box_j1->operator[](2).lb(), iterator_list->box_j1->operator[](2).ub(),
		   iterator_list->box_j1->operator[](3).lb(), iterator_list->box_j1->operator[](3).ub(), "green[green]");

  }
}


int main(){

  const int n = 5;

  IntervalVector yinit(n);
  // yinit[0]= Interval(2.,2.);
  // yinit[1]= Interval(0.,0.);
  
  vibes::beginDrawing ();
  vibes::newFigure("Coupled_vdp");

  yinit[0]= Interval(1.25,1.55);
  yinit[1]= Interval(2.35,2.45);
  yinit[2]= Interval(1.25,1.55);
  yinit[3]= Interval(2.35,2.45);

  Variable y(n);

  double mu=1.0;
  Interval b(1.0,3.0);
  yinit[4] = b;

  Function ydot = Function (y, Return (y[1],
				       mu*(1.0-y[0]*y[0])*y[1] - y[0] + y[4]*(y[2]-y[0]),
				       //-y[0]*(mu * y[0] * y[1] + 1.0 + b) + mu*y[1] + b*y[2],
				       y[3],
				       mu*(1.0-y[2]*y[2])*y[3] - y[2] + y[4]*(y[0]-y[2]),
				       Interval(0.0)
				       //-y[2]*(mu * y[2] * y[3] + 1.0 + b) + mu*y[3] + b*y[0]
				       ));

  queue<IntervalVector> s;
  s.push (yinit);

  //AF_fAFFullI::setAffineTolerance (1e-16);
  // AF_fAFFullI::setAffineNoiseNumber (15*n);
  AF_fAFFullI::setAffineNoiseNumber (80);

  // Precision (boxes of size less than eps are not processed)
  double eps=1e-3;
  
  for (int i = 0; i < n; i++) { // 25 is a good number of bissections
    IntervalVector box = s.front();
    s.pop();
    if (box.max_diam() >= eps) {
      int j=box.extr_diam_index(false);
      IntervalVector first(box);
      first[j]=Interval(first[j].lb());
      IntervalVector second(box);
      second[j]=Interval(second[j].ub());
      s.push(first);
      s.push(second);
    }
    else {
      //std::cout << "Trop petit " << box << std::endl;
      s.push (box);
    }
  }
  //std::cout << "nb boxes : " << s.size() << std::endl;


  IntervalVector safebox(n, Interval::ALL_REALS);
  safebox[1] = Interval(NEG_INFINITY, 2.75);
  safebox[3] = Interval(NEG_INFINITY, 2.75);

  /*std::cout << "set terminal png" << std::endl;
  std::cout << "set output 'van-der-pol-mu1.png'" << std::endl;
  std::cout << "set xrange [-2.5:2.5]" << std::endl;
  std::cout << "set yrange [-4:4]" << std::endl;
  std::cout << "set nokey" << std::endl;
  std::cout << "set style rectangle back fc rgb 'cyan' fs solid 1.0 border -1" << std::endl;*/

  int len = s.size();
  int cpt_treated = 0;
  int cpt_success = 0;
  int cpt_failure = 0;
  int cpt_unkwon = 0;

  auto start = high_resolution_clock::now();

  #pragma parallel while
  while (!s.empty()) {
    IntervalVector box = s.front();
    s.pop();
    //std::cout << cpt_treated << " -- current box = " << box << std::endl;
    std::cout << "\rnumber of treated cases: " << cpt_treated++ << "/" << s.size() << " -- Succ.|Fail.|Unkn. = " <<
      cpt_success << "|" << cpt_failure << "|" << cpt_unkwon << std::flush;
    ivp_ode problem = ivp_ode (ydot, 0.0, box, SYMBOLIC);
    simulation simu = simulation (&problem, __DURATION__, __METH__, __PREC__,0.005);

    simu.run_simulation();
    
    if (simu.stayed_in(safebox)) {
      cpt_success++;
      plot_simu(&simu);
      //exit(0);
      std::cout << box << std::endl;
    }
    else {
      //std::cerr << "safety failure after "<< cpt_treated++ << std::endl;
      LargestFirst bbb(eps, 0.5);
      if (box.max_diam() >= eps) {
	cpt_failure++;
	std::pair<IntervalVector,IntervalVector> p = bbb.bisect(box);
	s.push(p.first);
	s.push(p.second);
      }
      else {
	cpt_unkwon++;
      }

    }
  }

auto stop = high_resolution_clock::now();
auto duration = duration_cast<seconds>(stop - start);

ofstream results_file;
  results_file.open("results/results.csv", std::ios_base::app);
results_file << "CVDP23" << "," << "," << "1," << duration.count() << "," << "," << "\n";
  results_file.close();
	  
  vibes::axisLimits(-2.5,2.5,-4.05,4.05);
  vibes::setFigureProperty("Coupled_vdp","width",800);
  vibes::setFigureProperty("Coupled_vdp","height",600);
  vibes::saveImage("Coupled_vdp_mu1.jpg","Coupled_vdp");
  
  vibes::closeFigure("Coupled_vdp");
  vibes::endDrawing();
  //std::cout << "plot 2.75 lt rgb 'red', 4 lt rgb 'red'" << std::endl;
  //std::cout << std::endl;

  return 0;
}

/* results 30/03/2023
number of treated cases: 363/0 -- Succ.|Fail.|Unkn. = 184|179|0([1.55, 1.55] ; [2.425, 2.45] ; [1.5125, 1.55] ; [2.425, 2.45] ; [3, 3])

real	17m53,561s
user	17m53,109s
sys	0m0,340s
*/
