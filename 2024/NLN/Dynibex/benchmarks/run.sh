#!/bin/bash

echo "Entering run.sh"

mkdir -p results/stats
echo "benchmark,instance,result,time,accuracy,timesteps" > results/results.csv

../tools/VIBES-0.2.3/viewer/VIBes-viewer &
process_id=$!

echo "Run Traffic"
(time ./TRAF23) > results/stats/traf-stats.txt 2>&1
# mkdir -p /results/solutions
# mkdir -p /data/scenarios
# mv BEL_Putte-4_2_T-1.xml /data/scenarios
# cp BEL_Putte-4_2_T-1_occupancies.csv /results/solutions
mv BEL_Putte-4_2_T-1_occupancies.csv results/
# python check_collision.py BEL_Putte-4_2_T-1 -s

echo "Run Robertson"
(time ./ROBE23) > results/stats/rob-stats.txt 2>&1

echo "Run CVDP mu=1"
(time ./CVDP23) > results/stats/cvdp-mu1-stats.txt 2>&1

echo "Run Laub Loomis Large"
(time ./laub-loomis-large) > results/stats/lll-stats.txt 2>&1
echo "Run Laub Loomis Mid"
(time ./laub-loomis-mid) > results/stats/llm-stats.txt 2>&1
echo "Run Laub Loomis Tight"
(time ./laub-loomis-tight) > results/stats/llt-stats.txt 2>&1

echo "Run Hybrid Lotka-Volterra v2021"
(time ./LOVO21) > results/stats/hlv-stats.txt 2>&1

echo "Run Space RDV v2021"
(time ./SPRE22) > results/stats/srdv-stats.txt 2>&1

kill $process_id
wait $process_id

#post treating
cp *.jpg results/

