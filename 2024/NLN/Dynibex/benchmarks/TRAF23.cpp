#include <ibex.h>
#include <queue>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "vibes.cpp"
#include <cmath>
#include <chrono>

using namespace std::chrono;

using namespace ibex;


#define __PREC__ 1e-12
#define __METH__ RK4
#define __DURATION__ 0.1

#include <cmath>

// This function check if z+2pi*k belonggs to [a,b] for aome k. Need it to check if sin or cos has a local max or min on [a,b]
bool intersect(double a, double b, double z)
{
    double tol = 1e-4;

    if (a > 0)
    {
        if (std::abs(std::fmod(a - z, 2*M_PI)) < tol)
        {
            return true;
        }
        else
        {
            return (z + 2*M_PI*(std::floor((a - z)/(2*M_PI)) + 1) <= b);
        }
    }
    else
    {
        if (b > z)
        {
            return true;
        }
        else
        {
            if (std::abs(std::fmod(z - b, 2*M_PI)) < tol)
            {
                return true;
            }
            else
            {
                return (z - 2*M_PI*(std::floor((z - b)/(2*M_PI)) + 1) >= a);
            }
        }
    }
}





vector<vector<double>> read_file(string filename){
 ifstream file(filename);
    string line;
    vector<vector<double>> data;

    while (getline(file, line))
    {
        stringstream ss(line);
        vector<double> row;

        while (ss.good())
        {
            string value;
            getline(ss, value, ';');
            row.push_back(stod(value));
        }

        data.push_back(row);
    }
    return data;
}

void plot_simu(simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {

  IntervalVector box(2);
  box[0] = iterator_list->box_j1->operator[](3);
  box[1] = iterator_list->box_j1->operator[](4);

  vibes::drawBox(box, "blue[blue]");

  }
}


void save_simu(simulation* sim, ostream* out)
{
    double l = 4.508, d = 1.61, l_wb = 2.578;
    Interval eps1(-l/2 + l_wb/2, l/2 + l_wb/2);
    Interval eps2(-d/2, d/2);

    std::list<solution_g>::const_iterator iterator_list=sim->list_solution_g.begin();
    iterator_list++;
    for(iterator_list;iterator_list!=sim->list_solution_g.end();iterator_list++) {
        // sx,psi,eps1,eps2 are given intervals, I assume that occ_x is an interval overaproximating sx + cos(psi)*eps1 - sin(psi)*eps2,
        Interval occ_xn = iterator_list->box_jn->operator[](3) + cos(iterator_list->box_jn->operator[](1))*eps1 - sin(iterator_list->box_jn->operator[](1))*eps2;
        // sy,psi,eps1,eps2 are given intervals, I assume that occ_x is an interval overaproximating sy + sin(psi)*eps1 + cos(psi)*eps2,
        Interval occ_yn = iterator_list->box_jn->operator[](4) + sin(iterator_list->box_jn->operator[](1))*eps1 +  cos(iterator_list->box_jn->operator[](1))*eps2;

        Interval occ_xnh = iterator_list->box_jnh->operator[](3) + cos(iterator_list->box_jnh->operator[](1))*eps1 - sin(iterator_list->box_jnh->operator[](1))*eps2;
        // sy,psi,eps1,eps2 are given intervals, I assume that occ_x is an interval overaproximating sy + sin(psi)*eps1 + cos(psi)*eps2,
        Interval occ_ynh = iterator_list->box_jnh->operator[](4) + sin(iterator_list->box_jnh->operator[](1))*eps1 +  cos(iterator_list->box_jnh->operator[](1))*eps2;


        *out << iterator_list->time_j.lb() << ";" << occ_xn.lb() << ";" << occ_xn.ub()<< ";" << occ_xnh.lb() << ";" << occ_xnh.ub() << endl;
        *out << iterator_list->time_j.ub() << ";" << occ_yn.lb() << ";" << occ_yn.ub()<< ";" << occ_ynh.lb() << ";" << occ_ynh.ub() << endl;

  }
}

int main(){
  // disturbance bounds
  IntervalVector w(2);
  w[0] = Interval(-0.02, 0.02);
  w[1] = Interval(-0.3, 0.3);

  IntervalVector d(5);
  d[0] = Interval(-0.0004, 0.0004);
  d[1] = Interval(-0.0004, 0.0004);
  d[2] = Interval(-0.006, 0.006);
  d[3] = Interval(-0.002, 0.002);
  d[4] = Interval(-0.002, 0.002);

  ofstream exportfile("BEL_Putte-4_2_T-1_occupancies.csv");

  Interval l_wb(2.578);
  vector<vector<double>> data = read_file("BEL_Putte-4_2_T-1_controls.csv");
  // first row is an initial value

  vector<double> init_value = data[0];
  //cout<<"**";
  //std::cout << init_value << std::endl;

  const int n = 10;
  IntervalVector yinit(n);
  yinit[0] = Interval(init_value[1])+ d[0];
  yinit[1] = Interval(init_value[2])+ d[1];
  yinit[2] = Interval(init_value[3])+ d[2];
  yinit[3] = Interval(init_value[4])+ d[3];
  yinit[4] = Interval(init_value[5])+ d[4];
  yinit[5] = Interval(init_value[1]);
  yinit[6] = Interval(init_value[2]);
  yinit[7] = Interval(init_value[3]);
  yinit[8] = Interval(init_value[4]);
  yinit[9] = Interval(init_value[5]);
  Affine2Vector yinit_aff(yinit,true);
  //ze then erase the first row drom the data
  data.erase(data.begin());

  vibes::beginDrawing ();
  vibes::newFigure("TRAF");
  vibes::axisLimits(-720,-690,-795,-775);

  vibes::setFigureProperty("TRAF","width",800);
  vibes::setFigureProperty("TRAF","height",600);


auto start = high_resolution_clock::now();
  double ti=0.0;
  // every row stores ti, uref0i, uref1i,K01i,J02i, K03i, K04i, K10i, K11i, K21i, K31i, K41i
  for (auto& row : data){

    //std::cout<<"uref0 "<<row[1]<<" uref1 "<<row[2]<<std::endl;
    //control refernce
    Interval uref0(row[1]);
    Interval uref1(row[2]);
    //control margin
    double K00 = row[3], K01 = row[4], K02 = row[5], K03 = row[6], K04 = row[7];
    double K10 = row[8], K11 = row[9], K12 = row[10], K13 = row[11], K14 = row[12];
    //std::cout<<"K"<<K00<<" "<<K01<<" "<<K02<<" "<<K03<<" "<<K04<<" "<<K10<<" "<<K11<<" "<<K12<<" "<<K13<<" "<<K14<<std::endl;

    Variable y(n);
    ibex::Array<const ibex::ExprNode> a(n);
    a.set_ref(0, uref0 + K00*(y[0] + d[0] - y[5]) + K01*(y[1] + d[1] - y[6])+ K02*(y[2] + d[2] - y[7]) + K03*(y[3] + d[3] - y[8]) + K04*(y[4] + d[4] - y[9]) + w[0]);
    a.set_ref(1, (y[2]/l_wb)*(sin(y[0])/cos(y[0])));
    a.set_ref(2, uref1 + K10*(y[0] + d[0] - y[5]) + K11*(y[1] + d[1] - y[6]) + K12*(y[2] + d[2] - y[7]) + K13*(y[3] + d[3] - y[8]) + K14*(y[4] + d[4] - y[9]) + w[1]);
    a.set_ref(3, y[2]*cos(y[1]));
    a.set_ref(4, y[2]*sin(y[1]));
    a.set_ref(5, uref0);
    a.set_ref(6, (y[7]/l_wb)*(sin(y[5])/cos(y[5])));
    a.set_ref(7, uref1);
    a.set_ref(8, y[7]*cos(y[6]));
    a.set_ref(9, y[7]*sin(y[6]));

    const ibex::ExprVector& _return = ibex::ExprVector::new_(a, true);
    ibex::Function ydot(y, _return);
    AF_fAFFullI::setAffineNoiseNumber (200);
    //cout << ydot << endl;
    ivp_ode problem = ivp_ode (ydot, ti, yinit_aff);//, SYMBOLIC);
    ti+=__DURATION__;
    simulation simu = simulation(&problem, ti, __METH__, __PREC__, 0.01);
    simu.run_simulation();
    //cout<<"sim_success"<<std::endl;
    yinit_aff=simu.get_last_aff();

    //cout << yinit_aff.itv() << endl;

    plot_simu(&simu);
    save_simu(&simu, &exportfile);
  }

  auto stop = high_resolution_clock::now();
auto duration = duration_cast<seconds>(stop - start);
ofstream results_file;
  results_file.open("results/results.csv", std::ios_base::app);
results_file << "TRAF22" << "," << "," << "1," << duration.count() << "," << "," << "\n";
  results_file.close();

  vibes::saveImage("ARCH-COMP24-Dynibex-TRAF22.jpg","TRAF");

  vibes::closeFigure("TRAF");

  vibes::endDrawing();

exportfile.close();
return 0;
}
