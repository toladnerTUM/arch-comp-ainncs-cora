/* ============================================================================
 * D Y N I B E X - Definition of the Runge-Kutta order 4 Method
 * ============================================================================
 * Copyright   : ENSTA ParisTech
 * License     : This program can be distributed under the terms of the GNU LGPL.
 *               See the file COPYING.LESSER.
 *
 * Author(s)   : Julien Alexandre dit Sandretto
 * Created     : Feb, 2023
 * Sponsored   : This research benefited from the support of the "Chair Complex Systems Engineering - Ecole Polytechnique, THALES, DGA, FX, DASSAULT AVIATION, DCNS Research, ENSTA ParisTech, Telecom ParisTech, Fondation ParisTech and FDO ENSTA"
 * ---------------------------------------------------------------------------- */
#ifndef IBEX_SOL_RK4_SPEC_H
#define IBEX_SOL_RK4_SPEC_H

#include <iomanip>
#include <stdlib.h>
#include "ibex_solution.h"


namespace ibex{

class solution_j_rk4_spec : public solution_j
{
  public:
	//method to define

	IntervalVector picard(IntervalVector y0, ivp_ode* _ode, int ordre)
	{
	  return picard_tayl(y0,_ode,ordre);
	}

	//the LTE
	Affine2Vector LTE(IntervalVector y0,ivp_ode* _ode, double h)
	{

	    Affine2Vector err_aff = Affine2Vector(y0,true);
	    Affine2 y1=err_aff[0];
	    Affine2 y2=err_aff[1];
	    Affine2 y3=err_aff[2];
	    Affine2 y4=err_aff[3];
	    Affine2 y5=err_aff[4];
	    
	    err_aff[0]= (1.0/480.0)*pow(h, 5)*(1.0 - pow(y1, 2))*(-4*y1*y2*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) - 2*pow(y2, 3)) - 1.0/720.0*pow(h, 5)*(-4*pow(y2, 2)*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + pow(y2, 2)*(2*y1 - 2*y2*(1.0 - pow(y1, 2)) - 2*y5*(-y1 + y3))) - 1.0/120.0*pow(h, 5)*(y5*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (1.0 - pow(y1, 2))*(y5*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + (1.0 - pow(y1, 2))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(-2*y1*y2 - y5 - 1)) + (-2*y1*y2 - y5 - 1)*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)))) - 1.0/240.0*pow(h, 5)*(-2*y1*y2*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) - 2*y1*pow(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3), 2) - 2*pow(y2, 2)*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)));

 err_aff[1]= (1.0/240.0)*pow(h, 5)*y1*y2*(-4*y1*y2*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) - 2*pow(y2, 3)) - 1.0/720.0*pow(h, 5)*(1.0 - pow(y1, 2))*(-4*pow(y2, 2)*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + pow(y2, 2)*(2*y1 - 2*y2*(1.0 - pow(y1, 2)) - 2*y5*(-y1 + y3))) - 1.0/240.0*pow(h, 5)*(1.0 - pow(y1, 2))*(-2*y1*y2*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) - 2*y1*pow(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3), 2) - 2*pow(y2, 2)*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (1.0/480.0)*pow(h, 5)*(pow(y2, 2)*(-2*y2*(-2*y1*y2 - y5 - 1) - 2*y4*y5 - (2.0 - 2*pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + 2*y2*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(2*y1 - 2*y2*(1.0 - pow(y1, 2)) - 2*y5*(-y1 + y3))) + (1.0/160.0)*pow(h, 5)*(-4*y1*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) - 2*y2*pow(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3), 2)) + (1.0/480.0)*pow(h, 5)*(y5*(-4*y3*y4*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) - 2*pow(y4, 3)) + pow(1.0 - pow(y1, 2), 2)*(-4*y1*y2*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) - 2*pow(y2, 3)) + (-4*y1*y2*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) - 2*pow(y2, 3))*(-2*y1*y2 - y5 - 1)) - 1.0/120.0*pow(h, 5)*(y5*(y5*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + (1.0 - pow(y3, 2))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(-2*y3*y4 - y5 - 1)) + (1.0 - pow(y1, 2))*(y5*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (1.0 - pow(y1, 2))*(y5*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + (1.0 - pow(y1, 2))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(-2*y1*y2 - y5 - 1)) + (-2*y1*y2 - y5 - 1)*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)))) + (-2*y1*y2 - y5 - 1)*(y5*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + (1.0 - pow(y1, 2))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(-2*y1*y2 - y5 - 1))) + (1.0/120.0)*pow(h, 5)*(-2*y1*y2*(y5*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + (1.0 - pow(y1, 2))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(-2*y1*y2 - y5 - 1)) - 2*y1*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) - 2*pow(y2, 2)*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))));
	    
	    err_aff[2]= (1.0/480.0)*pow(h, 5)*(1.0 - pow(y3, 2))*(-4*y3*y4*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) - 2*pow(y4, 3)) - 1.0/720.0*pow(h, 5)*(-4*pow(y4, 2)*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + pow(y4, 2)*(2*y3 - 2*y4*(1.0 - pow(y3, 2)) - 2*y5*(y1 - y3))) - 1.0/120.0*pow(h, 5)*(y5*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (1.0 - pow(y3, 2))*(y5*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + (1.0 - pow(y3, 2))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(-2*y3*y4 - y5 - 1)) + (y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)))*(-2*y3*y4 - y5 - 1)) - 1.0/240.0*pow(h, 5)*(-2*y3*y4*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) - 2*y3*pow(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3), 2) - 2*pow(y4, 2)*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)));
	    
	    err_aff[3]= (1.0/240.0)*pow(h, 5)*y3*y4*(-4*y3*y4*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) - 2*pow(y4, 3)) - 1.0/720.0*pow(h, 5)*(1.0 - pow(y3, 2))*(-4*pow(y4, 2)*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + pow(y4, 2)*(2*y3 - 2*y4*(1.0 - pow(y3, 2)) - 2*y5*(y1 - y3))) - 1.0/240.0*pow(h, 5)*(1.0 - pow(y3, 2))*(-2*y3*y4*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) - 2*y3*pow(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3), 2) - 2*pow(y4, 2)*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (1.0/480.0)*pow(h, 5)*(pow(y4, 2)*(-2*y2*y5 - 2*y4*(-2*y3*y4 - y5 - 1) - (2.0 - 2*pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + 2*y4*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(2*y3 - 2*y4*(1.0 - pow(y3, 2)) - 2*y5*(y1 - y3))) + (1.0/160.0)*pow(h, 5)*(-4*y3*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) - 2*y4*pow(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3), 2)) + (1.0/480.0)*pow(h, 5)*(y5*(-4*y1*y2*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) - 2*pow(y2, 3)) + pow(1.0 - pow(y3, 2), 2)*(-4*y3*y4*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) - 2*pow(y4, 3)) + (-4*y3*y4*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) - 2*pow(y4, 3))*(-2*y3*y4 - y5 - 1)) - 1.0/120.0*pow(h, 5)*(y5*(y5*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)) + (1.0 - pow(y1, 2))*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))*(-2*y1*y2 - y5 - 1)) + (1.0 - pow(y3, 2))*(y5*(y2*(-2*y1*y2 - y5 - 1) + y4*y5 + (1.0 - pow(y1, 2))*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3))) + (1.0 - pow(y3, 2))*(y5*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + (1.0 - pow(y3, 2))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(-2*y3*y4 - y5 - 1)) + (y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3)))*(-2*y3*y4 - y5 - 1)) + (-2*y3*y4 - y5 - 1)*(y5*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + (1.0 - pow(y3, 2))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(-2*y3*y4 - y5 - 1))) + (1.0/120.0)*pow(h, 5)*(-2*y3*y4*(y5*(-y1 + y2*(1.0 - pow(y1, 2)) + y5*(-y1 + y3)) + (1.0 - pow(y3, 2))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) + (-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(-2*y3*y4 - y5 - 1)) - 2*y3*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))) - 2*pow(y4, 2)*(y2*y5 + y4*(-2*y3*y4 - y5 - 1) + (1.0 - pow(y3, 2))*(-y3 + y4*(1.0 - pow(y3, 2)) + y5*(y1 - y3))));
	    
	    err_aff[4]= 0;
	    

	    return err_aff;
	  
	}

	//the factor for the next stepsize computation
	double factor_stepsize(double test)
	{
	  return std::min(1.8,std::max(0.4,0.95*std::pow(1.0/test,0.2)));
	}

	//compute the sharpest jn+1
	int calcul_jnh(ivp_ode* _ode){
	  //with RK4 and affine form
	  *box_jnh_aff = remainder_rk4(_ode);
	  return 1;
	};


	//constructor
	solution_j_rk4_spec(const Affine2Vector _box_jn, double tn, double h, ivp_ode* _ode,double a, double fac) : solution_j(_box_jn, tn, h, _ode, a, fac)
	{


	}


	//destructor
	~solution_j_rk4_spec(){

	}



private:

	  //rk4 with remainder
	  Affine2Vector remainder_rk4(ivp_ode* _ode)
	  {
	    double h=time_j.diam();

	    Affine2Vector boxj1(*box_jn_aff);
	    Affine2Vector k1 = _ode->compute_derivatives_aff(1, boxj1);
	    Affine2Vector boxj2(k1);
	    boxj2*=(Interval(0.5)*h);


	    Affine2Vector k2 = _ode->compute_derivatives_aff(1, *box_jn_aff+boxj2);


	    Affine2Vector boxj3(k2);
	    boxj3*=(Interval(0.5)*h);


	    Affine2Vector k3 = _ode->compute_derivatives_aff(1, *box_jn_aff+boxj3);


	    Affine2Vector boxj4(k3);
	    boxj4*=(h);


	    Affine2Vector k4 = _ode->compute_derivatives_aff(1, *box_jn_aff+boxj4);

	    k2*=(2.0);
	    k3*=(2.0);


	    Affine2Vector int_rk4 = k1+k2+k3+k4;
	    int_rk4*=(Interval(h)/6.0);
	    int_rk4+=*box_jn_aff;

	    return int_rk4+*box_err_aff;//df

	  };



};
}

#endif
