function [sys,params,spec,sat] = generateRandomVerify(varargin)
% generateRandomVerify - generates a random verification benchmark
%
% Syntax:
%    [sys,params,spec,sat] = generateRandomVerify(...
%                               "StateDimension",n,
%                               "InputDimension",m,
%                               "OutputDimension",r,
%                               "RealInterval",realInt,
%                               "ImaginaryInterval",imagInt,
%                               "ConvergenceFactor",convFactor,
%                               "NrSpecs",nrSpecs,
%                               "SetRepSpec",setRepSpec,
%                               "Satisfiable",sat,
%                               "Difficulty",difficulty)
%                       
% Inputs:
%    Name-Value pairs (all options, arbitrary order):
%       <'StateDimension',n> - state dimension
%       <'InputDimension',nrInputs> - input dimension
%       <'OutputDimension',nrOutputs> - output dimension
%       <'RealInterval',realInt> - interval for real part of eigenvalues
%       <'ImaginaryInterval',imagInt> - interval for imaginary part of
%                                           eigenvalues
%       <'ConvergenceFactor',convFactor> - convergence factor for time
%                                           horizon
%       <'NrSpecs',nrSpecs> - number of specifications
%       <'SetRepSpec',setRepSpec> - convex set representation class
%       <'Satisfiable',sat> - true/false whether specifications are
%                               satisfiable
%       <'Difficulty',difficulty> - difficulty of benchmark, i.e.,
%           tightness of reachable sets required to obtain correct result
%
% Outputs:
%    sys - linearSys object
%    params - model parameters
%       .R0 - initial set
%       .U - input set
%       .tFinal - time horizon
%    spec - array of specifications
%    sat - true/false whether specifications are satisfiable
%
% Example: 
%    [sys,params,spec,sat] = generateRandomVerify("StateDimension",5);

% Authors:       Mark Wetzlinger
% Written:       06-May-2024
% Last update:   ---
% Last revision: ---

% ------------------------------ BEGIN CODE -------------------------------

% 1. read name-value pairs
% name-value pairs -> number of input arguments is always a multiple of 2
if mod(nargin,2) ~= 0
    throw(CORAerror('CORA:evenNumberInputArgs'));
end
[n,nrInputs,nrOutputs,realInt,imagInt,convFactor,nrSpecs,setRepSpec,...
    sat,difficulty] = aux_readNameValuePairs(varargin);

% 2. generate random linear system (use default values from there)
sys = linearSys.generateRandom(...
    'StateDimension',n,...
    'InputDimension',nrInputs,...
    'OutputDimension',nrOutputs,...
    'RealInterval',realInt,...
    'ImaginaryInterval',imagInt);

% 3. set parameters
params.R0 = zonotope.generateRandom("Dimension",n,"Center",5*ones(n,1));
params.R0 = enlarge(params.R0,1./8*rad(interval(params.R0)));
params.U = 0.01 * interval.generateRandom("Dimension",nrInputs);
params.tFinal = aux_computeTimeHorizon(sys.A,convFactor);

% 4. compute reachable set
Rlist = aux_reach(sys,params,sat,difficulty);

% 5. place specifications
timeInt = interval(0,params.tFinal);
spec = aux_placeSpecs(Rlist,nrSpecs,setRepSpec,timeInt);

end

% Auxiliary functions -----------------------------------------------------

function [n,nrInputs,nrOutputs,realInt,imagInt,convFactor,nrSpecs,setRepSpec,...
    sat,difficulty] = aux_readNameValuePairs(NVpairs)

% check list of name-value pairs
checkNameValuePairs(NVpairs,{'StateDimension','InputDimension',...
    'OutputDimension','RealInterval','ImaginaryInterval',...
    'ConvergenceFactor','NrSpecs','SetRepSpec','Satisfiable','Difficulty'});

% state dimension given?
[NVpairs,n] = readNameValuePair(NVpairs,'StateDimension');

% input dimension given?
[NVpairs,nrInputs] = readNameValuePair(NVpairs,'InputDimension');

% output dimension given?
[NVpairs,nrOutputs] = readNameValuePair(NVpairs,'OutputDimension');

% interval for real part of eigenvalues given?
[NVpairs,realInt] = readNameValuePair(NVpairs,'RealInterval',...
    @(x) representsa_(x,'interval',eps),interval.empty(1));

% interval for imaginary part of eigenvalues given?
[NVpairs,imagInt] = readNameValuePair(NVpairs,'ImaginaryInterval',...
    @(x) representsa_(x,'interval',eps),interval.empty(1));

% convergence factor for time horizon given?
[NVpairs,convFactor] = readNameValuePair(NVpairs,'ConvergenceFactor',...
    @isnumeric,0.001);

% number of specifications given?
[NVpairs,nrSpecs] = readNameValuePair(NVpairs,'NrSpecs',...
    @isnumeric,1);

% set representation of specifications given?
admissibleContSet = {'interval','zonotope','ellipsoid','halfspace'};
[NVpairs,setRepSpec] = readNameValuePair(NVpairs,'SetRepSpec',...
    @(x) any(strcmp(x,admissibleContSet)),'interval');

% satisfiability prescribed?
[NVpairs,sat] = readNameValuePair(NVpairs,'Satisfiable',...
    @islogical,true);

% difficulty prescribed?
[NVpairs,difficulty] = readNameValuePair(NVpairs,'Difficulty',...
    @ischar,'medium');

end

function tFinal = aux_computeTimeHorizon(A,convFactor)
% compute the time horizon for the randomly generated system using the
% largest eigenvalue of the system matrix and a convergence factor

% compute maximum real-part of eigenvalues of system matrix
ev = eigs(A);
ev_max = max(real(ev));

% time horizon until convergence
tFinal = log(convFactor) / ev_max;

end

function Rlist = aux_reach(sys,params,sat,difficulty)
% compute an outer/inner approximation of the reachable set

if sat
    % outer approximation
    switch difficulty
        case 'easy'
            steps = 200;
        case 'medium'
            steps = 1000;
        case 'hard'
            steps = 10000;
        otherwise
            throw(CORAerror('CORA:notSupported'));
    end
    options.timeStep = params.tFinal/steps;
    options.taylorTerms = 8;
    options.zonotopeOrder = 100;
    options.linAlg = 'wrapping-free';
    options.verbose = false;
    
    % call reach
    R = reach(sys,params,options);
    Rlist = R.timeInterval.set;
else
    % inner approximation
    steps = 100;
    options.timeStep = params.tFinal/steps;
    options.zonotopeOrder = 100;

    Rin = reachInner(sys,params,options);
    Rlist = Rin.timePoint.set;

    % shrink sets for easier falsification
    switch difficulty
        case 'easy'
            shrinkFactor = 0.5;
        case 'medium'
            shrinkFactor = 0.75;
        case 'hard'
            shrinkFactor = 0.9;
        otherwise
            throw(CORAerror('CORA:notSupported'));
    end
    for k=1:length(Rlist)
        Rlist{k} = enlarge(Rlist{k},shrinkFactor);
    end

end

% figure; hold on;
% useCORAcolors("CORA:contDynamics",2);
% plot(R);
% plot(Rin);
% keyboard

end

function spec = aux_placeSpecs(Rlist,nrSpecs,setRepSpec,timeInt)
% place the specifications

% dimension
nrOutputs = dim(Rlist{1});
% list of directions (to avoid using similar directions twice)
ell_all = zeros(nrOutputs,nrSpecs);
% maximum tries per direction
max_iterations = 1000;
iteration = 1;
spec = [];

while length(spec) < nrSpecs && iteration < max_iterations
    % choose random direction (unit length)
    ell = randn(nrOutputs,1);
    ell = ell ./ vecnorm(ell);

    ell_all(:,iteration) = ell;
    if iteration > 1 && any(ell' * ell_all(:,1:length(spec)) >= 0.9)
        % the closer the dot product is to 1, the more similar the
        % current direction is to a previously chosen one
        iteration = iteration + 1;
        continue
    end

    % compute support function and associated support vector of the
    % reachable set in the chosen direction        
    [~,sV_R,hitsInitSet] = aux_supportFunc(Rlist,ell);
    if hitsInitSet
        iteration = iteration + 1;
        continue
    end

    % random set centered at the origin
    switch setRepSpec
        case 'interval'
            S = interval.generateRandom("Dimension",nrOutputs,...
                "Center",zeros(nrOutputs,1));
        case 'zonotope'
            S = zonotope.generateRandom("Dimension",nrOutputs,...
                "Center",zeros(nrOutputs,1));
        case 'ellipsoid'
            S = ellipsoid.generateRandom("Dimension",nrOutputs,...
                "Center",zeros(nrOutputs,1));
        case 'polytope'
            S = polytope.generateRandom("Dimension",nrOutputs);
            S = S - center(S);
        case 'halfspace'
            % no randomness here...
            S = halfspace(-ell,0);
            sV_S = 0;
        otherwise
            throw(CORAerror('CORA:notSupported'));
    end

    % compute support vector in opposite direction
    if ~strcmp(setRepSpec,'halfspace')
        [~,sV_S] = supportFunc_(S,-ell,'upper');
    end

    % move the center of the specification so that is is satisfiable or
    % unsatisfiable
    S = S + (sV_R - sV_S);

    % specifications should not intersect one another
    if ~strcmp(setRepSpec,'halfspace')
        intersectionFound = false;
        for i=1:length(spec)
            if isIntersecting_(S,spec(i).set,'approx')
                iteration = iteration + 1;
                intersectionFound = true;
            end
        end
        if intersectionFound
            continue
        end
    end

    % append to list of specifications
    if iteration == 1
        spec = specification(S,'unsafeSet',timeInt);
    else
        spec = [spec; specification(S,'unsafeSet',timeInt)];
    end

    % increment iteration counter
    iteration = iteration + 1;
end

if length(spec) < nrSpecs
    disp("Not enough specification could be generated!");
end

end

function [sF,sV,hitsInitSet] = aux_supportFunc(Rlist,ell)
% compute the support function value and support vector of the reachable
% set (given as the union of individual sets) in a given direction;
% depending on the satisfiability of the subsequently generated
% specifications, we have to use the outer or inner approximation,
% accordingly

% read out number of sets, init support function value
nrSets = length(Rlist);
sF = -Inf;
idx = 0;

% loop over all reachable sets
for k=1:nrSets
    % compute support function of k-th set
    [sF_i,sV_i] = supportFunc_(Rlist{k},ell,'upper');

%     % shift value of support function by Hausdorff distance with respect to
%     % the exact reachable set since we require an inner approximation
%     if ~sat
%         sF_i = sF_i - R.timeInterval.error(k);
%     end

    % check value, override if larger
    if sF_i > sF
        idx = k;
        sF = sF_i;
        sV = sV_i;
    end
end

% is the maximum extent of the reachable set in the chosen direction given
% by the initial set?
hitsInitSet = idx == 1;

end

% ------------------------------ END OF CODE ------------------------------
