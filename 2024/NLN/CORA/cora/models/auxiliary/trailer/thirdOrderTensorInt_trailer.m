function [Tf,ind] = thirdOrderTensorInt_trailer(x,u)



 Tf{1,1} = interval(sparse(7,7),sparse(7,7));



 Tf{1,2} = interval(sparse(7,7),sparse(7,7));



 Tf{1,3} = interval(sparse(7,7),sparse(7,7));



 Tf{1,4} = interval(sparse(7,7),sparse(7,7));

Tf{1,4}(5,5) = -cos(x(5));


 Tf{1,5} = interval(sparse(7,7),sparse(7,7));

Tf{1,5}(5,4) = -cos(x(5));
Tf{1,5}(4,5) = -cos(x(5));
Tf{1,5}(5,5) = x(4)*sin(x(5));


 Tf{1,6} = interval(sparse(7,7),sparse(7,7));



 Tf{1,7} = interval(sparse(7,7),sparse(7,7));



 Tf{2,1} = interval(sparse(7,7),sparse(7,7));



 Tf{2,2} = interval(sparse(7,7),sparse(7,7));



 Tf{2,3} = interval(sparse(7,7),sparse(7,7));



 Tf{2,4} = interval(sparse(7,7),sparse(7,7));

Tf{2,4}(5,5) = -sin(x(5));


 Tf{2,5} = interval(sparse(7,7),sparse(7,7));

Tf{2,5}(5,4) = -sin(x(5));
Tf{2,5}(4,5) = -sin(x(5));
Tf{2,5}(5,5) = -x(4)*cos(x(5));


 Tf{2,6} = interval(sparse(7,7),sparse(7,7));



 Tf{2,7} = interval(sparse(7,7),sparse(7,7));



 Tf{3,1} = interval(sparse(7,7),sparse(7,7));



 Tf{3,2} = interval(sparse(7,7),sparse(7,7));



 Tf{3,3} = interval(sparse(7,7),sparse(7,7));

Tf{3,3}(3,3) = - (1484344215934809*x(4)*(tan(x(3))^2 + 1)^2)/70368744177664 - (1484344215934809*x(4)*tan(x(3))^2*(tan(x(3))^2 + 1))/35184372088832;
Tf{3,3}(4,3) = -(1484344215934809*tan(x(3))*(tan(x(3))^2 + 1))/70368744177664;
Tf{3,3}(3,4) = -(1484344215934809*tan(x(3))*(tan(x(3))^2 + 1))/70368744177664;


 Tf{3,4} = interval(sparse(7,7),sparse(7,7));

Tf{3,4}(3,3) = -(1484344215934809*tan(x(3))*(tan(x(3))^2 + 1))/70368744177664;


 Tf{3,5} = interval(sparse(7,7),sparse(7,7));



 Tf{3,6} = interval(sparse(7,7),sparse(7,7));



 Tf{3,7} = interval(sparse(7,7),sparse(7,7));



 Tf{4,1} = interval(sparse(7,7),sparse(7,7));



 Tf{4,2} = interval(sparse(7,7),sparse(7,7));



 Tf{4,3} = interval(sparse(7,7),sparse(7,7));



 Tf{4,4} = interval(sparse(7,7),sparse(7,7));



 Tf{4,5} = interval(sparse(7,7),sparse(7,7));



 Tf{4,6} = interval(sparse(7,7),sparse(7,7));



 Tf{4,7} = interval(sparse(7,7),sparse(7,7));



 Tf{5,1} = interval(sparse(7,7),sparse(7,7));



 Tf{5,2} = interval(sparse(7,7),sparse(7,7));



 Tf{5,3} = interval(sparse(7,7),sparse(7,7));

Tf{5,3}(3,3) = 2*x(4)*(tan(x(3))^2 + 1)^2 + 4*x(4)*tan(x(3))^2*(tan(x(3))^2 + 1);
Tf{5,3}(4,3) = 2*tan(x(3))*(tan(x(3))^2 + 1);
Tf{5,3}(3,4) = 2*tan(x(3))*(tan(x(3))^2 + 1);


 Tf{5,4} = interval(sparse(7,7),sparse(7,7));

Tf{5,4}(3,3) = 2*tan(x(3))*(tan(x(3))^2 + 1);


 Tf{5,5} = interval(sparse(7,7),sparse(7,7));



 Tf{5,6} = interval(sparse(7,7),sparse(7,7));



 Tf{5,7} = interval(sparse(7,7),sparse(7,7));



 Tf{6,1} = interval(sparse(7,7),sparse(7,7));



 Tf{6,2} = interval(sparse(7,7),sparse(7,7));



 Tf{6,3} = interval(sparse(7,7),sparse(7,7));

Tf{6,3}(3,3) = - 2*x(4)*(tan(x(3))^2 + 1)^2 - 4*x(4)*tan(x(3))^2*(tan(x(3))^2 + 1);
Tf{6,3}(4,3) = -2*tan(x(3))*(tan(x(3))^2 + 1);
Tf{6,3}(3,4) = -2*tan(x(3))*(tan(x(3))^2 + 1);


 Tf{6,4} = interval(sparse(7,7),sparse(7,7));

Tf{6,4}(3,3) = -2*tan(x(3))*(tan(x(3))^2 + 1);
Tf{6,4}(6,6) = sin(x(6));


 Tf{6,5} = interval(sparse(7,7),sparse(7,7));



 Tf{6,6} = interval(sparse(7,7),sparse(7,7));

Tf{6,6}(6,4) = sin(x(6));
Tf{6,6}(4,6) = sin(x(6));
Tf{6,6}(6,6) = x(4)*cos(x(6));


 Tf{6,7} = interval(sparse(7,7),sparse(7,7));



 Tg{1,1} = interval(sparse(7,7),sparse(7,7));



 Tg{1,2} = interval(sparse(7,7),sparse(7,7));



 Tg{1,3} = interval(sparse(7,7),sparse(7,7));



 Tg{1,4} = interval(sparse(7,7),sparse(7,7));



 Tg{1,5} = interval(sparse(7,7),sparse(7,7));



 Tg{1,6} = interval(sparse(7,7),sparse(7,7));



 Tg{1,7} = interval(sparse(7,7),sparse(7,7));



 Tg{2,1} = interval(sparse(7,7),sparse(7,7));



 Tg{2,2} = interval(sparse(7,7),sparse(7,7));



 Tg{2,3} = interval(sparse(7,7),sparse(7,7));



 Tg{2,4} = interval(sparse(7,7),sparse(7,7));



 Tg{2,5} = interval(sparse(7,7),sparse(7,7));



 Tg{2,6} = interval(sparse(7,7),sparse(7,7));



 Tg{2,7} = interval(sparse(7,7),sparse(7,7));



 Tg{3,1} = interval(sparse(7,7),sparse(7,7));



 Tg{3,2} = interval(sparse(7,7),sparse(7,7));



 Tg{3,3} = interval(sparse(7,7),sparse(7,7));



 Tg{3,4} = interval(sparse(7,7),sparse(7,7));



 Tg{3,5} = interval(sparse(7,7),sparse(7,7));



 Tg{3,6} = interval(sparse(7,7),sparse(7,7));



 Tg{3,7} = interval(sparse(7,7),sparse(7,7));



 Tg{4,1} = interval(sparse(7,7),sparse(7,7));



 Tg{4,2} = interval(sparse(7,7),sparse(7,7));



 Tg{4,3} = interval(sparse(7,7),sparse(7,7));



 Tg{4,4} = interval(sparse(7,7),sparse(7,7));



 Tg{4,5} = interval(sparse(7,7),sparse(7,7));



 Tg{4,6} = interval(sparse(7,7),sparse(7,7));



 Tg{4,7} = interval(sparse(7,7),sparse(7,7));



 Tg{5,1} = interval(sparse(7,7),sparse(7,7));



 Tg{5,2} = interval(sparse(7,7),sparse(7,7));



 Tg{5,3} = interval(sparse(7,7),sparse(7,7));



 Tg{5,4} = interval(sparse(7,7),sparse(7,7));



 Tg{5,5} = interval(sparse(7,7),sparse(7,7));



 Tg{5,6} = interval(sparse(7,7),sparse(7,7));



 Tg{5,7} = interval(sparse(7,7),sparse(7,7));



 Tg{6,1} = interval(sparse(7,7),sparse(7,7));



 Tg{6,2} = interval(sparse(7,7),sparse(7,7));



 Tg{6,3} = interval(sparse(7,7),sparse(7,7));



 Tg{6,4} = interval(sparse(7,7),sparse(7,7));



 Tg{6,5} = interval(sparse(7,7),sparse(7,7));



 Tg{6,6} = interval(sparse(7,7),sparse(7,7));



 Tg{6,7} = interval(sparse(7,7),sparse(7,7));


 ind = cell(6,1);
 ind{1} = [4;5];


 ind{2} = [4;5];


 ind{3} = [3;4];


 ind{4} = [];


 ind{5} = [3;4];


 ind{6} = [3;4;6];

end

