function [Tf,ind] = thirdOrderTensorInt_trailer_controlled(x,u)



 Tf{1,1} = interval(sparse(13,13),sparse(13,13));



 Tf{1,2} = interval(sparse(13,13),sparse(13,13));



 Tf{1,3} = interval(sparse(13,13),sparse(13,13));



 Tf{1,4} = interval(sparse(13,13),sparse(13,13));

Tf{1,4}(5,5) = -cos(x(5));


 Tf{1,5} = interval(sparse(13,13),sparse(13,13));

Tf{1,5}(5,4) = -cos(x(5));
Tf{1,5}(4,5) = -cos(x(5));
Tf{1,5}(5,5) = x(4)*sin(x(5));


 Tf{1,6} = interval(sparse(13,13),sparse(13,13));



 Tf{1,7} = interval(sparse(13,13),sparse(13,13));



 Tf{1,8} = interval(sparse(13,13),sparse(13,13));



 Tf{1,9} = interval(sparse(13,13),sparse(13,13));



 Tf{1,10} = interval(sparse(13,13),sparse(13,13));



 Tf{1,11} = interval(sparse(13,13),sparse(13,13));



 Tf{1,12} = interval(sparse(13,13),sparse(13,13));



 Tf{1,13} = interval(sparse(13,13),sparse(13,13));



 Tf{2,1} = interval(sparse(13,13),sparse(13,13));



 Tf{2,2} = interval(sparse(13,13),sparse(13,13));



 Tf{2,3} = interval(sparse(13,13),sparse(13,13));



 Tf{2,4} = interval(sparse(13,13),sparse(13,13));

Tf{2,4}(5,5) = -sin(x(5));


 Tf{2,5} = interval(sparse(13,13),sparse(13,13));

Tf{2,5}(5,4) = -sin(x(5));
Tf{2,5}(4,5) = -sin(x(5));
Tf{2,5}(5,5) = -x(4)*cos(x(5));


 Tf{2,6} = interval(sparse(13,13),sparse(13,13));



 Tf{2,7} = interval(sparse(13,13),sparse(13,13));



 Tf{2,8} = interval(sparse(13,13),sparse(13,13));



 Tf{2,9} = interval(sparse(13,13),sparse(13,13));



 Tf{2,10} = interval(sparse(13,13),sparse(13,13));



 Tf{2,11} = interval(sparse(13,13),sparse(13,13));



 Tf{2,12} = interval(sparse(13,13),sparse(13,13));



 Tf{2,13} = interval(sparse(13,13),sparse(13,13));



 Tf{3,1} = interval(sparse(13,13),sparse(13,13));



 Tf{3,2} = interval(sparse(13,13),sparse(13,13));



 Tf{3,3} = interval(sparse(13,13),sparse(13,13));

Tf{3,3}(3,3) = - (6597085404154707*x(4)*(tan(x(3))^2 + 1)^2)/1125899906842624 - (6597085404154707*x(4)*tan(x(3))^2*(tan(x(3))^2 + 1))/562949953421312;
Tf{3,3}(4,3) = -(6597085404154707*tan(x(3))*(tan(x(3))^2 + 1))/1125899906842624;
Tf{3,3}(3,4) = -(6597085404154707*tan(x(3))*(tan(x(3))^2 + 1))/1125899906842624;


 Tf{3,4} = interval(sparse(13,13),sparse(13,13));

Tf{3,4}(3,3) = -(6597085404154707*tan(x(3))*(tan(x(3))^2 + 1))/1125899906842624;


 Tf{3,5} = interval(sparse(13,13),sparse(13,13));



 Tf{3,6} = interval(sparse(13,13),sparse(13,13));



 Tf{3,7} = interval(sparse(13,13),sparse(13,13));



 Tf{3,8} = interval(sparse(13,13),sparse(13,13));



 Tf{3,9} = interval(sparse(13,13),sparse(13,13));



 Tf{3,10} = interval(sparse(13,13),sparse(13,13));



 Tf{3,11} = interval(sparse(13,13),sparse(13,13));



 Tf{3,12} = interval(sparse(13,13),sparse(13,13));



 Tf{3,13} = interval(sparse(13,13),sparse(13,13));



 Tf{4,1} = interval(sparse(13,13),sparse(13,13));



 Tf{4,2} = interval(sparse(13,13),sparse(13,13));



 Tf{4,3} = interval(sparse(13,13),sparse(13,13));



 Tf{4,4} = interval(sparse(13,13),sparse(13,13));



 Tf{4,5} = interval(sparse(13,13),sparse(13,13));



 Tf{4,6} = interval(sparse(13,13),sparse(13,13));



 Tf{4,7} = interval(sparse(13,13),sparse(13,13));



 Tf{4,8} = interval(sparse(13,13),sparse(13,13));



 Tf{4,9} = interval(sparse(13,13),sparse(13,13));



 Tf{4,10} = interval(sparse(13,13),sparse(13,13));



 Tf{4,11} = interval(sparse(13,13),sparse(13,13));



 Tf{4,12} = interval(sparse(13,13),sparse(13,13));



 Tf{4,13} = interval(sparse(13,13),sparse(13,13));



 Tf{5,1} = interval(sparse(13,13),sparse(13,13));



 Tf{5,2} = interval(sparse(13,13),sparse(13,13));



 Tf{5,3} = interval(sparse(13,13),sparse(13,13));

Tf{5,3}(3,3) = (5*x(4)*(tan(x(3))^2 + 1)^2)/9 + (10*x(4)*tan(x(3))^2*(tan(x(3))^2 + 1))/9;
Tf{5,3}(4,3) = (5*tan(x(3))*(tan(x(3))^2 + 1))/9;
Tf{5,3}(3,4) = (5*tan(x(3))*(tan(x(3))^2 + 1))/9;


 Tf{5,4} = interval(sparse(13,13),sparse(13,13));

Tf{5,4}(3,3) = (5*tan(x(3))*(tan(x(3))^2 + 1))/9;


 Tf{5,5} = interval(sparse(13,13),sparse(13,13));



 Tf{5,6} = interval(sparse(13,13),sparse(13,13));



 Tf{5,7} = interval(sparse(13,13),sparse(13,13));



 Tf{5,8} = interval(sparse(13,13),sparse(13,13));



 Tf{5,9} = interval(sparse(13,13),sparse(13,13));



 Tf{5,10} = interval(sparse(13,13),sparse(13,13));



 Tf{5,11} = interval(sparse(13,13),sparse(13,13));



 Tf{5,12} = interval(sparse(13,13),sparse(13,13));



 Tf{5,13} = interval(sparse(13,13),sparse(13,13));



 Tf{6,1} = interval(sparse(13,13),sparse(13,13));



 Tf{6,2} = interval(sparse(13,13),sparse(13,13));



 Tf{6,3} = interval(sparse(13,13),sparse(13,13));

Tf{6,3}(3,3) = - (5*x(4)*(tan(x(3))^2 + 1)^2)/9 - (10*x(4)*tan(x(3))^2*(tan(x(3))^2 + 1))/9;
Tf{6,3}(4,3) = -(5*tan(x(3))*(tan(x(3))^2 + 1))/9;
Tf{6,3}(3,4) = -(5*tan(x(3))*(tan(x(3))^2 + 1))/9;


 Tf{6,4} = interval(sparse(13,13),sparse(13,13));

Tf{6,4}(3,3) = -(5*tan(x(3))*(tan(x(3))^2 + 1))/9;
Tf{6,4}(6,6) = (10*sin(x(6)))/81;


 Tf{6,5} = interval(sparse(13,13),sparse(13,13));



 Tf{6,6} = interval(sparse(13,13),sparse(13,13));

Tf{6,6}(6,4) = (10*sin(x(6)))/81;
Tf{6,6}(4,6) = (10*sin(x(6)))/81;
Tf{6,6}(6,6) = (10*x(4)*cos(x(6)))/81;


 Tf{6,7} = interval(sparse(13,13),sparse(13,13));



 Tf{6,8} = interval(sparse(13,13),sparse(13,13));



 Tf{6,9} = interval(sparse(13,13),sparse(13,13));



 Tf{6,10} = interval(sparse(13,13),sparse(13,13));



 Tf{6,11} = interval(sparse(13,13),sparse(13,13));



 Tf{6,12} = interval(sparse(13,13),sparse(13,13));



 Tf{6,13} = interval(sparse(13,13),sparse(13,13));



 Tg{1,1} = interval(sparse(13,13),sparse(13,13));



 Tg{1,2} = interval(sparse(13,13),sparse(13,13));



 Tg{1,3} = interval(sparse(13,13),sparse(13,13));



 Tg{1,4} = interval(sparse(13,13),sparse(13,13));



 Tg{1,5} = interval(sparse(13,13),sparse(13,13));



 Tg{1,6} = interval(sparse(13,13),sparse(13,13));



 Tg{1,7} = interval(sparse(13,13),sparse(13,13));



 Tg{1,8} = interval(sparse(13,13),sparse(13,13));



 Tg{1,9} = interval(sparse(13,13),sparse(13,13));



 Tg{1,10} = interval(sparse(13,13),sparse(13,13));



 Tg{1,11} = interval(sparse(13,13),sparse(13,13));



 Tg{1,12} = interval(sparse(13,13),sparse(13,13));



 Tg{1,13} = interval(sparse(13,13),sparse(13,13));



 Tg{2,1} = interval(sparse(13,13),sparse(13,13));



 Tg{2,2} = interval(sparse(13,13),sparse(13,13));



 Tg{2,3} = interval(sparse(13,13),sparse(13,13));



 Tg{2,4} = interval(sparse(13,13),sparse(13,13));



 Tg{2,5} = interval(sparse(13,13),sparse(13,13));



 Tg{2,6} = interval(sparse(13,13),sparse(13,13));



 Tg{2,7} = interval(sparse(13,13),sparse(13,13));



 Tg{2,8} = interval(sparse(13,13),sparse(13,13));



 Tg{2,9} = interval(sparse(13,13),sparse(13,13));



 Tg{2,10} = interval(sparse(13,13),sparse(13,13));



 Tg{2,11} = interval(sparse(13,13),sparse(13,13));



 Tg{2,12} = interval(sparse(13,13),sparse(13,13));



 Tg{2,13} = interval(sparse(13,13),sparse(13,13));



 Tg{3,1} = interval(sparse(13,13),sparse(13,13));



 Tg{3,2} = interval(sparse(13,13),sparse(13,13));



 Tg{3,3} = interval(sparse(13,13),sparse(13,13));



 Tg{3,4} = interval(sparse(13,13),sparse(13,13));



 Tg{3,5} = interval(sparse(13,13),sparse(13,13));



 Tg{3,6} = interval(sparse(13,13),sparse(13,13));



 Tg{3,7} = interval(sparse(13,13),sparse(13,13));



 Tg{3,8} = interval(sparse(13,13),sparse(13,13));



 Tg{3,9} = interval(sparse(13,13),sparse(13,13));



 Tg{3,10} = interval(sparse(13,13),sparse(13,13));



 Tg{3,11} = interval(sparse(13,13),sparse(13,13));



 Tg{3,12} = interval(sparse(13,13),sparse(13,13));



 Tg{3,13} = interval(sparse(13,13),sparse(13,13));



 Tg{4,1} = interval(sparse(13,13),sparse(13,13));



 Tg{4,2} = interval(sparse(13,13),sparse(13,13));



 Tg{4,3} = interval(sparse(13,13),sparse(13,13));



 Tg{4,4} = interval(sparse(13,13),sparse(13,13));



 Tg{4,5} = interval(sparse(13,13),sparse(13,13));



 Tg{4,6} = interval(sparse(13,13),sparse(13,13));



 Tg{4,7} = interval(sparse(13,13),sparse(13,13));



 Tg{4,8} = interval(sparse(13,13),sparse(13,13));



 Tg{4,9} = interval(sparse(13,13),sparse(13,13));



 Tg{4,10} = interval(sparse(13,13),sparse(13,13));



 Tg{4,11} = interval(sparse(13,13),sparse(13,13));



 Tg{4,12} = interval(sparse(13,13),sparse(13,13));



 Tg{4,13} = interval(sparse(13,13),sparse(13,13));



 Tg{5,1} = interval(sparse(13,13),sparse(13,13));



 Tg{5,2} = interval(sparse(13,13),sparse(13,13));



 Tg{5,3} = interval(sparse(13,13),sparse(13,13));



 Tg{5,4} = interval(sparse(13,13),sparse(13,13));



 Tg{5,5} = interval(sparse(13,13),sparse(13,13));



 Tg{5,6} = interval(sparse(13,13),sparse(13,13));



 Tg{5,7} = interval(sparse(13,13),sparse(13,13));



 Tg{5,8} = interval(sparse(13,13),sparse(13,13));



 Tg{5,9} = interval(sparse(13,13),sparse(13,13));



 Tg{5,10} = interval(sparse(13,13),sparse(13,13));



 Tg{5,11} = interval(sparse(13,13),sparse(13,13));



 Tg{5,12} = interval(sparse(13,13),sparse(13,13));



 Tg{5,13} = interval(sparse(13,13),sparse(13,13));



 Tg{6,1} = interval(sparse(13,13),sparse(13,13));



 Tg{6,2} = interval(sparse(13,13),sparse(13,13));



 Tg{6,3} = interval(sparse(13,13),sparse(13,13));



 Tg{6,4} = interval(sparse(13,13),sparse(13,13));



 Tg{6,5} = interval(sparse(13,13),sparse(13,13));



 Tg{6,6} = interval(sparse(13,13),sparse(13,13));



 Tg{6,7} = interval(sparse(13,13),sparse(13,13));



 Tg{6,8} = interval(sparse(13,13),sparse(13,13));



 Tg{6,9} = interval(sparse(13,13),sparse(13,13));



 Tg{6,10} = interval(sparse(13,13),sparse(13,13));



 Tg{6,11} = interval(sparse(13,13),sparse(13,13));



 Tg{6,12} = interval(sparse(13,13),sparse(13,13));



 Tg{6,13} = interval(sparse(13,13),sparse(13,13));


 ind = cell(6,1);
 ind{1} = [4;5];


 ind{2} = [4;5];


 ind{3} = [3;4];


 ind{4} = [];


 ind{5} = [3;4];


 ind{6} = [3;4;6];

end

