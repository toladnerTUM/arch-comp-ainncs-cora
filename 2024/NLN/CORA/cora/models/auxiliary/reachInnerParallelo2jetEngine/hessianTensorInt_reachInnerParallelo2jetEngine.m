function Hf=hessianTensorInt_reachInnerParallelo2jetEngine(x,u)



 Hf{1} = interval(sparse(7,7),sparse(7,7));

Hf{1}(1,1) = - 3*x(1) - 3;


 Hf{2} = interval(sparse(7,7),sparse(7,7));



 Hf{3} = interval(sparse(7,7),sparse(7,7));

Hf{3}(1,1) = -3*x(3);
Hf{3}(3,1) = - 3*x(1) - 3;
Hf{3}(1,3) = - 3*x(1) - 3;


 Hf{4} = interval(sparse(7,7),sparse(7,7));



 Hf{5} = interval(sparse(7,7),sparse(7,7));

Hf{5}(1,1) = -3*x(5);
Hf{5}(5,1) = - 3*x(1) - 3;
Hf{5}(1,5) = - 3*x(1) - 3;


 Hf{6} = interval(sparse(7,7),sparse(7,7));


end