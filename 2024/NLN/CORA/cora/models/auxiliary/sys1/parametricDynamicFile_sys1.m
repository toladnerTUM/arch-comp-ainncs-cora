function f=parametricDynamicFile_sys1(x,u)

f{1}(1,1)=x(1)^2 - x(2);
f{1}(2,1)=x(2) - u(1);
f{2}(1,1)=1;
f{2}(2,1)=0;
