function Hf=hessianTensorInt_unicycle(x,u)



 Hf{1} = interval(sparse(4,4),sparse(4,4));

Hf{1}(3,3) = -5*cos(x(3));


 Hf{2} = interval(sparse(4,4),sparse(4,4));

Hf{2}(3,3) = -5*sin(x(3));


 Hf{3} = interval(sparse(4,4),sparse(4,4));


end