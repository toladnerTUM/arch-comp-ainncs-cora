function Hf=hessianTensorInt_dynamics_attitudeControlControlled(x,u)



 Hf{1} = interval(sparse(10,10),sparse(10,10));

Hf{1}(3,2) = 1/4;
Hf{1}(2,3) = 1/4;


 Hf{2} = interval(sparse(10,10),sparse(10,10));

Hf{2}(3,1) = -3/2;
Hf{2}(1,3) = -3/2;


 Hf{3} = interval(sparse(10,10),sparse(10,10));

Hf{3}(2,1) = 2;
Hf{3}(1,2) = 2;


 Hf{4} = interval(sparse(10,10),sparse(10,10));

Hf{4}(4,1) = x(4);
Hf{4}(5,1) = x(5);
Hf{4}(6,1) = x(6);
Hf{4}(4,2) = x(4);
Hf{4}(5,2) = x(5);
Hf{4}(6,2) = x(6) - 1/2;
Hf{4}(4,3) = x(4);
Hf{4}(5,3) = x(5) + 1/2;
Hf{4}(6,3) = x(6);
Hf{4}(1,4) = x(4);
Hf{4}(2,4) = x(4);
Hf{4}(3,4) = x(4);
Hf{4}(4,4) = x(1) + x(2) + x(3);
Hf{4}(1,5) = x(5);
Hf{4}(2,5) = x(5);
Hf{4}(3,5) = x(5) + 1/2;
Hf{4}(5,5) = x(1) + x(2) + x(3);
Hf{4}(1,6) = x(6);
Hf{4}(2,6) = x(6) - 1/2;
Hf{4}(3,6) = x(6);
Hf{4}(6,6) = x(1) + x(2) + x(3);


 Hf{5} = interval(sparse(10,10),sparse(10,10));

Hf{5}(4,1) = x(4);
Hf{5}(5,1) = x(5);
Hf{5}(6,1) = x(6) + 1/2;
Hf{5}(4,2) = x(4);
Hf{5}(5,2) = x(5);
Hf{5}(6,2) = x(6);
Hf{5}(4,3) = x(4) - 1/2;
Hf{5}(5,3) = x(5);
Hf{5}(6,3) = x(6);
Hf{5}(1,4) = x(4);
Hf{5}(2,4) = x(4);
Hf{5}(3,4) = x(4) - 1/2;
Hf{5}(4,4) = x(1) + x(2) + x(3);
Hf{5}(1,5) = x(5);
Hf{5}(2,5) = x(5);
Hf{5}(3,5) = x(5);
Hf{5}(5,5) = x(1) + x(2) + x(3);
Hf{5}(1,6) = x(6) + 1/2;
Hf{5}(2,6) = x(6);
Hf{5}(3,6) = x(6);
Hf{5}(6,6) = x(1) + x(2) + x(3);


 Hf{6} = interval(sparse(10,10),sparse(10,10));

Hf{6}(4,1) = x(4);
Hf{6}(5,1) = x(5) - 1/2;
Hf{6}(6,1) = x(6);
Hf{6}(4,2) = x(4) + 1/2;
Hf{6}(5,2) = x(5);
Hf{6}(6,2) = x(6);
Hf{6}(4,3) = x(4);
Hf{6}(5,3) = x(5);
Hf{6}(6,3) = x(6);
Hf{6}(1,4) = x(4);
Hf{6}(2,4) = x(4) + 1/2;
Hf{6}(3,4) = x(4);
Hf{6}(4,4) = x(1) + x(2) + x(3);
Hf{6}(1,5) = x(5) - 1/2;
Hf{6}(2,5) = x(5);
Hf{6}(3,5) = x(5);
Hf{6}(5,5) = x(1) + x(2) + x(3);
Hf{6}(1,6) = x(6);
Hf{6}(2,6) = x(6);
Hf{6}(3,6) = x(6);
Hf{6}(6,6) = x(1) + x(2) + x(3);


 Hf{7} = interval(sparse(10,10),sparse(10,10));



 Hf{8} = interval(sparse(10,10),sparse(10,10));



 Hf{9} = interval(sparse(10,10),sparse(10,10));


end