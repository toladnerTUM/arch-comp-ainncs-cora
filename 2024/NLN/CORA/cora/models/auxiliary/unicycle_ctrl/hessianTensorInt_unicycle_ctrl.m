function Hf=hessianTensorInt_unicycle_ctrl(x,u)



 Hf{1} = interval(sparse(5,5),sparse(5,5));

Hf{1}(3,3) = -5*cos(x(3));


 Hf{2} = interval(sparse(5,5),sparse(5,5));

Hf{2}(3,3) = -5*sin(x(3));


 Hf{3} = interval(sparse(5,5),sparse(5,5));


end