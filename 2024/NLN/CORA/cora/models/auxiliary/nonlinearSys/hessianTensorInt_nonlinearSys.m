function Hf=hessianTensorInt_nonlinearSys(x,u)



 Hf{1} = interval(sparse(4,4),sparse(4,4));

Hf{1}(3,2) = -1;
Hf{1}(2,3) = -1;


 Hf{2} = interval(sparse(4,4),sparse(4,4));



 Hf{3} = interval(sparse(4,4),sparse(4,4));

Hf{3}(2,1) = -1;
Hf{3}(1,2) = -1;

end