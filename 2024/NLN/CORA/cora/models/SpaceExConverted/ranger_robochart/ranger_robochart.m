function HA = ranger_robochart(~)


%% Generated on 30-Nov-2023 14:25:28

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.RoboChart_1):
%  state x := [lv; av]
%  input u := [uDummy]
%  output y := [lv; av]

% Component 2 (system.Dummy_1):
%  state x := [dummy]
%  input u := [lv; av]

%---------------------Component system.RoboChart_1-------------------------

clear loc

%------------------------Location Moving_SetArg----------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   lv := LS &
%   av := 0
resetA = ...
[0,0;0,0];
resetc = ...
[1;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 2, 'moveCall_4_5_start');

loc(1) = location('Moving_SetArg', inv, trans, dynamics);



%-------------------------Location Moving_Call-----------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 3, 'moveCall_4_5_end');

loc(2) = location('Moving_Call', inv, trans, dynamics);



%-------------------------Location Moving_Wait-----------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 4, 'tock_1');

loc(3) = location('Moving_Wait', inv, trans, dynamics);



%----------------------------Location Moving-------------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 5, 'obstacle_2_in');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 4, 'tock_1');

loc(4) = location('Moving', inv, trans, dynamics);



%-----------------------Location Turning_SetArgs---------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   lv := 0 &
%   av := AS
resetA = ...
[0,0;0,0];
resetc = ...
[0;1.5707963267900000303711749438662];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 6, 'moveCall_4_5_start');

loc(5) = location('Turning_SetArgs', inv, trans, dynamics);



%-------------------------Location Turning_Call----------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 7, 'moveCall_4_5_end');

loc(6) = location('Turning_Call', inv, trans, dynamics);



%-------------------------Location Turning_Wait----------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 8, 'tock_1');

loc(7) = location('Turning_Wait', inv, trans, dynamics);



%------------------------Location Turning_Wait2----------------------------

%% flow equation:
%   lv' == 0 & av' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 1, 'tock_1');

loc(8) = location('Turning_Wait2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% only dummy input
iBinds{1} = [0 1];

%-----------------------Component system.Dummy_1---------------------------

clear loc

%----------------------------Location Dummy1-------------------------------

%% flow equation:
%   dummy' == lv + av
dynA = ...
[0];
dynB = ...
[1,1];
dync = ...
[0];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = fullspace(1);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(1) = transition(guard, reset, 1, 'obstacle_2_in');

%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(2) = transition(guard, reset, 1, 'moveCall_4_5_start');

%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(3) = transition(guard, reset, 1, 'moveCall_4_5_end');

%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(4) = transition(guard, reset, 1, 'tock_1');

loc(1) = location('Dummy1', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% input names: lv, av
iBinds{2} = [[1,1];[1,2]];

HA = parallelHybridAutomaton(comp,iBinds);

end