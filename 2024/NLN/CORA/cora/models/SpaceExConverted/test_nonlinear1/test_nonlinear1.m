function sys = test_nonlinear1(~)


%% Generated on 27-May-2024

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component (system.cont_1):
%  state x := [x1; x2]
%  input u := [uDummy]

%------------------------Component system.cont_1---------------------------

%-----------------------------State always---------------------------------

%% equation:
%   x1' = 2*x1^2 + x2^(1/2) &&
%   x2' = 4*exp(x1) - 3*sin(x2)
sys = nonlinearSys(@test_nonlinear1_St1_FlowEq,2,1); 


end