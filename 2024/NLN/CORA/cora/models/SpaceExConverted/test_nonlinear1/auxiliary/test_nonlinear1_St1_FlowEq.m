function [dx]=test_nonlinear1_St1_FlowEq(x,u)

dx(1,1) = 2*x(1)^2 + x(2)^(1/2);

dx(2,1) = 4*exp(x(1)) - 3*sin(x(2));
