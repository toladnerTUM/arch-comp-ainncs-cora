function HA = ranger(~)


%% Generated on 06-Jun-2023 13:18:45

%-------------Automaton created from Component 'Constants'-----------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (Constants.Composition_1.RoboChart_1):
%  state x := [lv; av; stepCount]
%  input u := [uDummy]
%  output y := [lv; av]

% Component 2 (Constants.Composition_1.RoboWorld_1.Move_OperationMapping_1):
%  state x := [arg_x; arg_y]
%  input u := [yaw; lv; av]
%  output y := [arg_x; arg_y]

% Component 3 (Constants.Composition_1.RoboWorld_1.Environment_1.EnvironmentLoop_1):
%  state x := [pos_x; pos_y; vel_x; vel_y; acc_x; acc_y; yaw; yawVel; yawAcc; timer]
%  input u := [arg_x; arg_y]
%  output y := [yaw]

% Component 4 (Constants.Composition_1.RoboWorld_1.Environment_1.ObstacleBuffer_1):
%  state x := [dummy]
%  input u := [uDummy]

%-------------Component Constants.Composition_1.RoboChart_1----------------

clear loc

%------------------------Location Moving_SetArg----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(3);

trans = transition();
%% reset equation:
%   lv := LS &
%   av := 0
resetA = ...
[0,0,0;0,0,0;0,0,1];
resetc = ...
[1;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 2);

loc(1) = location('Moving_SetArg', inv, trans, dynamics);



%-------------------------Location Moving_Call-----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(3);

trans = transition();
%% reset equation:
%   stepCount := 0
resetA = ...
[1,0,0;0,1,0;0,0,0];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 3, 'moveCall');

loc(2) = location('Moving_Call', inv, trans, dynamics);



%-------------------------Location Moving_Wait-----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(3);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount < 1
A = ...
[0,0,1];
b = ...
[1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 5);

%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount >= 1
A = ...
[0,0,-1];
b = ...
[-1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 4);

loc(3) = location('Moving_Wait', inv, trans, dynamics);



%----------------------------Location Moving-------------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(3);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 4, 'tock');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(2) = transition(guard, reset, 6, 'obstacle');

loc(4) = location('Moving', inv, trans, dynamics);



%------------------------Location Moving_Update----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(3);

trans = transition();
%% reset equation:
%   stepCount := stepCount + timeStep
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0.25];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 3, 'tock');

loc(5) = location('Moving_Update', inv, trans, dynamics);



%-----------------------Location Turning_SetArgs---------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(3);

trans = transition();
%% reset equation:
%   lv := 0 &
%   av := AS
resetA = ...
[0,0,0;0,0,0;0,0,1];
resetc = ...
[0;1.5707963267900000303711749438662;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 7);

loc(6) = location('Turning_SetArgs', inv, trans, dynamics);



%-------------------------Location Turning_Call----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(3);

trans = transition();
%% reset equation:
%   stepCount := 0
resetA = ...
[1,0,0;0,1,0;0,0,0];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 8, 'moveCall');

loc(7) = location('Turning_Call', inv, trans, dynamics);



%-------------------------Location Turning_Wait----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(3);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount < 1
A = ...
[0,0,1];
b = ...
[1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 9);

%% reset equation:
%   arg := 0
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount >= 1
A = ...
[0,0,-1];
b = ...
[-1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 10);

loc(8) = location('Turning_Wait', inv, trans, dynamics);



%------------------------Location Turning_Update---------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(3);

trans = transition();
%% reset equation:
%   stepCount := stepCount + timeStep
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0.25];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 8, 'tock');

loc(9) = location('Turning_Update', inv, trans, dynamics);



%-------------------------Location Turning_End-----------------------------

%% flow equation:
%   stepCount' == 0 &
%   lv' == 0 & av' == 0
dynA = ...
[0,0,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;0];

%% output equation:
%   
dynC = ...
[1,0,0;0,1,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(3);

trans = transition();
%% reset equation:
%   stepCount := 0
resetA = ...
[1,0,0;0,1,0;0,0,0];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 3, 'moveCall');

loc(10) = location('Turning_End', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% only dummy input
iBinds{1} = [0 1];

%-Component Constants.Composition_1.RoboWorld_1.Move_OperationMapping_1----

clear loc

%-------------------------Location WaitForCall-----------------------------

%% flow equation:
%   arg_x' == 0 &
%   arg_y' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0,0,0;0,0,0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0,0,0;0,0,0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 2, 'moveCall');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 1, 'proceed');

loc(1) = location('WaitForCall', inv, trans, dynamics);



%----------------------Location SetLinearSpeedArgs-------------------------

%% flow equation:
%   arg_x' == 0 &
%   arg_y' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0,0,0;0,0,0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0,0,0;0,0,0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   arg_x := lv * cos(yaw) &
%   arg_y := lv * sin(yaw)
reset = struct('f', @ranger_Comp2_Loc2_Trans1_ResetEq);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 3);

loc(2) = location('SetLinearSpeedArgs', inv, trans, dynamics);



%------------------------Location SetLinearSpeed---------------------------

%% flow equation:
%   arg_x' == 0 &
%   arg_y' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0,0,0;0,0,0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0,0,0;0,0,0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 4, 'set_vel');

loc(3) = location('SetLinearSpeed', inv, trans, dynamics);



%---------------------Location SetAngularSpeedArgs-------------------------

%% flow equation:
%   arg_x' == 0 &
%   arg_y' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0,0,0;0,0,0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0,0,0;0,0,0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   arg_x := av
resetA = ...
[0,0;0,1];
resetB = ...
[0,0,1;0,0,0];
resetc = ...
[0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 5);

loc(4) = location('SetAngularSpeedArgs', inv, trans, dynamics);



%-----------------------Location SetAngularSpeed---------------------------

%% flow equation:
%   arg_x' == 0 &
%   arg_y' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0,0,0;0,0,0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0;0,1];
dynD = ...
[0,0,0;0,0,0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 1, 'set_yawVel');

loc(5) = location('SetAngularSpeed', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% input names: yaw, lv, av
iBinds{2} = [[3,1];[1,1];[1,2]];

%Component Constants.Composition_1.RoboWorld_1.Environment_1.EnvironmentLoop_1

clear loc

%------------------------Location RobotMovement----------------------------

%% flow equation:
%   pos_x' == vel_x &
%   pos_y' == vel_y &
%   vel_x' == acc_x &
%   vel_y' == acc_y &
%   acc_x' == 0 &
%   acc_y' == 0 &
%   yaw' == yawVel &
%   yawVel' == yawAcc &
%   yawAcc' == 0 &
%   timer' == 1
dynA = ...
[0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,...
1,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,1,0,0;0,...
0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0];
dynB = ...
[0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0];
dync = ...
[0;0;0;0;0;0;0;0;0;1];

%% output equation:
%   
dynC = ...
[0,0,0,0,0,0,1,0,0,0];
dynD = ...
[0,0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   timer < timeStep
A = ...
[0,0,0,0,0,0,0,0,0,1];
b = ...
[0.25];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,1];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   timer >= timeStep
c = [0;0;0;0;0;0;0;0;0;1];
d = 0.25;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2, 'tock');

loc(1) = location('RobotMovement', inv, trans, dynamics);



%-----------------------Location CheckForObstacle--------------------------

%% flow equation:
%   pos_x' == vel_x &
%   pos_y' == vel_y &
%   vel_x' == acc_x &
%   vel_y' == acc_y &
%   acc_x' == 0 &
%   acc_y' == 0 &
%   yaw' == yawVel &
%   yawVel' == yawAcc &
%   yawAcc' == 0 &
%   timer' == 1
dynA = ...
[0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,...
1,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,1,0,0;0,...
0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0];
dynB = ...
[0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0];
dync = ...
[0;0;0;0;0;0;0;0;0;1];

%% output equation:
%   
dynC = ...
[0,0,0,0,0,0,1,0,0,0];
dynD = ...
[0,0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(10);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,1];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos_x - obstacle_pos_x)^2 + (pos_y - obstacle_pos_y)^2 > detectionRadius^2
vars = sym('x',[10,1]);
syms x1 x2 x3 x4 x5 x6 x7 x8 x9 x10;
eq = 1 - (x2 - 5)^2 - (x1 - 5)^2;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 3, 'obstacleTrue');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,1];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos_x - obstacle_pos_x)^2 + (pos_y - obstacle_pos_y)^2 <= detectionRadius^2
vars = sym('x',[10,1]);
syms x1 x2 x3 x4 x5 x6 x7 x8 x9 x10;
eq = (x1 - 5)^2 + (x2 - 5)^2 - 1;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(2) = transition(guard, reset, 3, 'obstacleFalse');

loc(2) = location('CheckForObstacle', inv, trans, dynamics);



%------------------------Location Communication----------------------------

%% flow equation:
%   pos_x' == vel_x &
%   pos_y' == vel_y &
%   vel_x' == acc_x &
%   vel_y' == acc_y &
%   acc_x' == 0 &
%   acc_y' == 0 &
%   yaw' == yawVel &
%   yawVel' == yawAcc &
%   yawAcc' == 0 &
%   timer' == 1
dynA = ...
[0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,...
1,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,1,0,0;0,...
0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0];
dynB = ...
[0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0];
dync = ...
[0;0;0;0;0;0;0;0;0;1];

%% output equation:
%   
dynC = ...
[0,0,0,0,0,0,1,0,0,0];
dynD = ...
[0,0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(10);

trans = transition();
%% reset equation:
%   vel_x := arg_x &
%   vel_y := arg_y
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,1];
resetB = ...
[0,0;0,0;1,0;0,1;0,0;0,0;0,0;0,0;0,0;0,0];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(10);

trans(1) = transition(guard, reset, 3, 'set_vel');

%% reset equation:
%   timer := 0
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,0];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(10);

trans(2) = transition(guard, reset, 1, 'proceed');

%% reset equation:
%   acc_x := arg_x &
%   acc_y := arg_y
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,1];
resetB = ...
[0,0;0,0;0,0;0,0;1,0;0,1;0,0;0,0;0,0;0,0];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(10);

trans(3) = transition(guard, reset, 3, 'set_acc');

%% reset equation:
%   yawVel := arg_x
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0,1];
resetB = ...
[0,0;0,0;0,0;0,0;0,0;0,0;0,0;1,0;0,0;0,0];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(10);

trans(4) = transition(guard, reset, 3, 'set_yawVel');

%% reset equation:
%   yawAcc := arg_x
resetA = ...
[1,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0;0,0,1,0,0,0,0,0,0,0;0,0,0,1,0,...
0,0,0,0,0;0,0,0,0,1,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0;0,0,0,0,0,0,1,0,0,0;0,...
0,0,0,0,0,0,1,0,0;0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,1];
resetB = ...
[0,0;0,0;0,0;0,0;0,0;0,0;0,0;0,0;1,0;0,0];
resetc = ...
[0;0;0;0;0;0;0;0;0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(10);

trans(5) = transition(guard, reset, 3, 'set_yawAcc');

loc(3) = location('Communication', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(3) = hybridAutomaton(loc);

% input names: arg_x, arg_y
iBinds{3} = [[2,1];[2,2]];

%Component Constants.Composition_1.RoboWorld_1.Environment_1.ObstacleBuffer_1-

clear loc

%-------------------------Location ObstacleTrue----------------------------

%% flow equation:
%   dummy' == 0
dynA = ...
[0];
dynB = ...
[0];
dync = ...
[0];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = fullspace(1);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(1) = transition(guard, reset, 1, 'obstacleTrue');

%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(2) = transition(guard, reset, 2, 'obstacleFalse');

%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(3) = transition(guard, reset, 1, 'obstacle');

loc(1) = location('ObstacleTrue', inv, trans, dynamics);



%------------------------Location ObstacleFalse----------------------------

%% flow equation:
%   dummy' == 0
dynA = ...
[0];
dynB = ...
[0];
dync = ...
[0];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = fullspace(1);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(1) = transition(guard, reset, 1, 'obstacleTrue');

%% reset equation:
%   no reset equation given
resetA = ...
[1];
resetc = ...
[0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(1);

trans(2) = transition(guard, reset, 2, 'obstacleFalse');

loc(2) = location('ObstacleFalse', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(4) = hybridAutomaton(loc);

% only dummy input
iBinds{4} = [0 1];

HA = parallelHybridAutomaton(comp,iBinds);

end