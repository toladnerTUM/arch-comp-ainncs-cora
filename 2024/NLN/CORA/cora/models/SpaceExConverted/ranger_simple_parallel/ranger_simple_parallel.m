function HA = ranger_simple_parallel(~)


%% Generated on 03-Jun-2023 09:12:39

%-------------Automaton created from Component 'Constants'-----------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (Constants.Composition_1.EnvironmentLoop_1):
%  state x := [pos; vel; acc; timer]
%  input u := [arg]

% Component 2 (Constants.Composition_1.Controller_1):
%  state x := [arg; stepCount]
%  input u := [uDummy]
%  output y := [arg]

%----------Component Constants.Composition_1.EnvironmentLoop_1-------------

clear loc

%------------------------Location RobotMovement----------------------------

%% flow equation:
%   pos' == vel &
%   vel' == acc &
%   acc' == 0 &
%   timer' == 1
dynA = ...
[0,1,0,0;0,0,1,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   timer < timeStep
A = ...
[0,0,0,1];
b = ...
[0.25];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   timer >= timeStep
c = [0;0;0;1];
d = 0.25;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2);

loc(1) = location('RobotMovement', inv, trans, dynamics);



%-----------------------Location CheckForObstacle--------------------------

%% flow equation:
%   pos' == vel &
%   vel' == acc &
%   acc' == 0 &
%   timer' == 1
dynA = ...
[0,1,0,0;0,0,1,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = (x1 - 5)^2 - 1;
compOp = '<';

inv = levelSet(eq,vars,compOp);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos - obstacle_pos)^2 > detectionRadius^2
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = 1 - (x1 - 5)^2;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 3);

%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos - obstacle_pos)^2 <= detectionRadius^2
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = (x1 - 5)^2 - 1;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(2) = transition(guard, reset, 3, 'obstacle');

loc(2) = location('CheckForObstacle', inv, trans, dynamics);



%------------------------Location Communication----------------------------

%% flow equation:
%   pos' == vel &
%   vel' == acc &
%   acc' == 0 &
%   timer' == 1
dynA = ...
[0,1,0,0;0,0,1,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = fullspace(4);

trans = transition();
%% reset equation:
%   vel := arg
resetA = ...
[1,0,0,0;0,0,0,0;0,0,1,0;0,0,0,1];
resetB = ...
[0;1;0;0];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(4);

trans(1) = transition(guard, reset, 3, 'set_vel');

%% reset equation:
%   timer := 0
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,0];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(4);

trans(2) = transition(guard, reset, 1, 'proceed');

%% reset equation:
%   acc := arg
resetA = ...
[1,0,0,0;0,1,0,0;0,0,0,0;0,0,0,1];
resetB = ...
[0;0;1;0];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'B', resetB, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(4);

trans(3) = transition(guard, reset, 3, 'set_acc');

loc(3) = location('Communication', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% input names: arg
iBinds{1} = [[2,1]];

%------------Component Constants.Composition_1.Controller_1----------------

clear loc

%------------------------Location Moving_SetArg----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   arg := speed
resetA = ...
[0,0;0,1];
resetc = ...
[1;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 2);

loc(1) = location('Moving_SetArg', inv, trans, dynamics);



%-------------------------Location Moving_Call-----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   stepCount := 0
resetA = ...
[1,0;0,0];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 3, 'set_vel');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 2, 'obstacle');

loc(2) = location('Moving_Call', inv, trans, dynamics);



%-------------------------Location Moving_Wait-----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount < 1
A = ...
[0,1];
b = ...
[1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 5);

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount >= 1
A = ...
[0,-1];
b = ...
[-1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 4);

loc(3) = location('Moving_Wait', inv, trans, dynamics);



%----------------------------Location Moving-------------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 4, 'proceed');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 6, 'obstacle');

loc(4) = location('Moving', inv, trans, dynamics);



%------------------------Location Moving_Update----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   stepCount := stepCount + timeStep
resetA = ...
[1,0;0,1];
resetc = ...
[0;0.25];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 3, 'proceed');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 5, 'obstacle');

loc(5) = location('Moving_Update', inv, trans, dynamics);



%-----------------------Location Turning_SetArgs---------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   arg := -2
resetA = ...
[0,0;0,1];
resetc = ...
[-2;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 7);

loc(6) = location('Turning_SetArgs', inv, trans, dynamics);



%-------------------------Location Turning_Call----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   stepCount := 0
resetA = ...
[1,0;0,0];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 8, 'set_acc');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 7, 'obstacle');

loc(7) = location('Turning_Call', inv, trans, dynamics);



%-------------------------Location Turning_Wait----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount < 1
A = ...
[0,1];
b = ...
[1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 9);

%% reset equation:
%   arg := 0
resetA = ...
[0,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   stepCount >= 1
A = ...
[0,-1];
b = ...
[-1];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 10);

loc(8) = location('Turning_Wait', inv, trans, dynamics);



%------------------------Location Turning_Update---------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   stepCount := stepCount + timeStep
resetA = ...
[1,0;0,1];
resetc = ...
[0;0.25];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 8, 'proceed');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 9, 'obstacle');

loc(9) = location('Turning_Update', inv, trans, dynamics);



%-------------------------Location Turning_End-----------------------------

%% flow equation:
%   stepCount' == 0 &
%   arg' == 0
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;0];

%% output equation:
%   
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
%% reset equation:
%   stepCount := 0
resetA = ...
[1,0;0,0];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(1) = transition(guard, reset, 3, 'set_acc');

%% reset equation:
%   no reset equation given
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(2);

trans(2) = transition(guard, reset, 10, 'obstacle');

loc(10) = location('Turning_End', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% only dummy input
iBinds{2} = [0 1];

HA = parallelHybridAutomaton(comp,iBinds);

end