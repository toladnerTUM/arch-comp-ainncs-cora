function HA = ranger_simple(~)


%% Generated on 01-Jun-2023 17:37:51

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.EnvironmentLoop_1):
%  state x := [pos; vel; timer]
%  input u := [uDummy]

%------------------Component system.EnvironmentLoop_1----------------------

clear loc

%------------------------Location RobotMovement----------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1
dynA = ...
[0,1,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   timer < timeStep
A = ...
[0,0,1];
b = ...
[0.25];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   timer >= timeStep
c = [0;0;1];
d = 0.25;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2);

loc(1) = location('RobotMovement', inv, trans, dynamics);



%-----------------------Location CheckForObstacle--------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1
dynA = ...
[0,1,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = emptySet(3);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos - obstacle_pos)^2 > detectionRadius^2
vars = sym('x',[3,1]);
syms x1 x2 x3;
eq = 1 - (x1 - 5)^2;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 3);

%% reset equation:
%   vel := -vel
resetA = ...
[1,0,0;0,-1,0;0,0,1];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos - obstacle_pos)^2 <= detectionRadius^2
vars = sym('x',[3,1]);
syms x1 x2 x3;
eq = (x1 - 5)^2 - 1;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(2) = transition(guard, reset, 3);

loc(2) = location('CheckForObstacle', inv, trans, dynamics);



%------------------------Location Communication----------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1
dynA = ...
[0,1,0;0,0,0;0,0,0];
dynB = ...
[0;0;0];
dync = ...
[0;0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = emptySet(3);

trans = transition();
%% reset equation:
%   timer := 0
resetA = ...
[1,0,0;0,1,0;0,0,0];
resetc = ...
[0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(3);

trans(1) = transition(guard, reset, 1);

loc(3) = location('Communication', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end