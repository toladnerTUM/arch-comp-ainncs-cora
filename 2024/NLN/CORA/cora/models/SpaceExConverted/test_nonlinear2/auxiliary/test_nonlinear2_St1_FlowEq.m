function [dx]=test_nonlinear2_St1_FlowEq(x,u)

dx(1,1) = 2*x(1)^2 - cos(u(1)) + x(2)^(1/2);

dx(2,1) = 4*exp(x(1)) + log(u(2)) - 3*sin(x(2));
