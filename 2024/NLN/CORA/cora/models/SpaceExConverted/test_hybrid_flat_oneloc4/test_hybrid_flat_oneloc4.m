function HA = test_hybrid_flat_oneloc4(~)


%% Generated on 22-May-2023 18:18:24

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont_1):
%  state x := [x1; x2]
%  input u := [u1]

%------------------------Component system.cont_1---------------------------

clear loc

%----------------------------Location always-------------------------------

%% flow equation:
%   x1' = -2*sin(x1) + u1 &&
%   x2' = log(x1) - x2
dynamics = nonlinearSys(@test_hybrid_flat_oneloc4_Loc1_FlowEq,2,1); 
%% invariant equation:
%   x1 >= 0 && x2 <= 0
A = ...
[-1,0;0,1];
b = ...
[0;0];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1' := -x1 && x2' := sin(x2) + u1
reset = struct('f', @test_hybrid_flat_oneloc4_Loc1_Trans1_ResetEq);

%% guard equation:
%   x1 < 0
c = [-1;0];
d = 0;C = ...
[0,1];
D = [0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 1);

%% reset equation:
%   x1' := x2 - 1 && x2' := -x1
resetA = ...
[0,1;-1,0];
resetc = ...
[-1;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 > 0
c = [0;1];
d = 0;C = ...
[-1,0];
D = [0];

guard = conHyperplane(c,d,C,D);

trans(2) = transition(guard, reset, 1);

loc(1) = location('always', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end