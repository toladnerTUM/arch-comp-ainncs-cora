function [x_R]=test_hybrid_flat_oneloc4_Loc1_Trans1_ResetEq(x,u)

% Assign type of input to output variable, type safety feature
if isa(x,'double')
	x_R(1,1) = 0;
else
	x_R(1,1) = sym(0);
end


% reset assignments:
x_R(1,1) = -x(1);

x_R(2,1) = u(1) + sin(x(2));
