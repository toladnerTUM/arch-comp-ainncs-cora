function HA = ranger_simple2(~)


%% Generated on 03-Jun-2023 08:47:48

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.EnvironmentLoop_1):
%  state x := [pos; vel; timer; resetTimer]
%  input u := [uDummy]

%------------------Component system.EnvironmentLoop_1----------------------

clear loc

%------------------------Location RobotMovement----------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1 &
%   resetTimer' = 1
dynA = ...
[0,1,0,0;0,0,0,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;1;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   timer < timeStep
A = ...
[0,0,1,0];
b = ...
[0.25];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   timer >= timeStep
c = [0;0;1;0];
d = 0.25;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2);

loc(1) = location('RobotMovement', inv, trans, dynamics);



%-----------------------Location CheckForObstacle--------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1 &
%   resetTimer' = -1
dynA = ...
[0,1,0,0;0,0,0,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;1;-1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = emptySet(4);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos - obstacle_pos)^2 > detectionRadius^2
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = 1 - (x1 - 5)^2;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 3);

%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   (pos - obstacle_pos)^2 <= detectionRadius^2
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = (x1 - 5)^2 - 1;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(2) = transition(guard, reset, 4);

loc(2) = location('CheckForObstacle', inv, trans, dynamics);



%------------------------Location Communication----------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1 &
%   resetTimer' = -1
dynA = ...
[0,1,0,0;0,0,0,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;1;-1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = emptySet(4);

trans = transition();
%% reset equation:
%   timer := 0
resetA = ...
[1,0,0,0;0,1,0,0;0,0,0,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(4);

trans(1) = transition(guard, reset, 1);

loc(3) = location('Communication', inv, trans, dynamics);



%--------------------------Location CheckTurn------------------------------

%% flow equation:
%   pos' == vel &
%   vel' == 0 &
%   timer' == 1 &
%   resetTimer' = -1
dynA = ...
[0,1,0,0;0,0,0,0;0,0,0,0;0,0,0,0];
dynB = ...
[0;0;0;0];
dync = ...
[0;0;1;-1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = emptySet(4);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   resetTimer < 2
A = ...
[0,0,0,1];
b = ...
[2];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 3);

%% reset equation:
%   vel := -vel &
%   resetTimer := 0
resetA = ...
[1,0,0,0;0,-1,0,0;0,0,1,0;0,0,0,0];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   resetTimer >= 2
A = ...
[0,0,0,-1];
b = ...
[-2];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 3);

loc(4) = location('CheckTurn', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end