function sys = vanDerPolSys(~)


%% Generated on 22-May-2023

%---------------Automaton created from Component 'model'-------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component (model):
%  state x := [x1; x2]
%  input u := [u1]

%----------------------------Component model-------------------------------

%-----------------------------State always---------------------------------

%% equation:
%   x1' == x2
%    & x2' == u1 - x1 - x2*(x1^2 - 1)
sys = nonlinearSys(@vanDerPolSys_St1_FlowEq,2,1); 


end