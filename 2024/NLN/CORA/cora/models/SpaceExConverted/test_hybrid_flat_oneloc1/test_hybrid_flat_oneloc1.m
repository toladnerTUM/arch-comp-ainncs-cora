function HA = test_hybrid_flat_oneloc1(~)


%% Generated on 27-May-2024 13:30:22

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont_1):
%  state x := [x1; x2]
%  input u := [uDummy]

%------------------------Component system.cont_1---------------------------

clear loc

%----------------------------Location always-------------------------------

%% flow equation:
%   x1' = -2*x1 + x2 &&
%   x2' = x1 - 2*x2
dynA = ...
[-2,1;1,-2];
dynB = ...
[0;0];
dync = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x1 >= 0 && x2 <= 0
P_A = ...
[-1,0;0,1];
P_b = ...
[-0;-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := -x1 && x2' := x2
resetA = ...
[-1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 < 0
c = [1;0];
d = 0;C = ...
[0,1];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 1);

loc(1) = location('always', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end