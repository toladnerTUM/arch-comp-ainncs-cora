function [dx]=test_hybrid_flat_twoloc3_Loc1_FlowEq(x,u)

dx(1,1) = u(1) - 2*sin(x(1));

dx(2,1) = x(1) - x(2);
