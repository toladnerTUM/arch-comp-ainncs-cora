function HA = test_hybrid_flat_fourloc(~)


%% Generated on 27-May-2024 13:30:33

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont_1):
%  state x := [x1; x2]
%  input u := [u1]

%------------------------Component system.cont_1---------------------------

clear loc

%---------------------------Location topleft-------------------------------

%% flow equation:
%   x1' = u1 && x2' = -1
dynA = ...
[0,0;0,0];
dynB = ...
[1;0];
dync = ...
[0;-1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x1 <= 0 && x2 >= 0
P_A = ...
[1,0;0,-1];
P_b = ...
[-0;-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := x1 + 10 && x2' := x2 + 5
resetA = ...
[1,0;0,1];
resetc = ...
[10;5];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 >= 0
c = [1;0];
d = 0;C = ...
[0,-1];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 2);

%% reset equation:
%   x1' := x1 - 5 && x2' := x2 - 10
resetA = ...
[1,0;0,1];
resetc = ...
[-5;-10];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 <= 0
c = [0;-1];
d = 0;C = ...
[1,0];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(2) = transition(guard, reset, 3);

loc(1) = location('topleft', inv, trans, dynamics);



%---------------------------Location topright------------------------------

%% flow equation:
%   x1' = -1 && x2' = u1
dynA = ...
[0,0;0,0];
dynB = ...
[0;1];
dync = ...
[-1;0];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x1 >= 0 && x2 >= 0
P_A = ...
[-1,0;0,-1];
P_b = ...
[-0;-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := x1 - 10 && x2' := x2 + 5
resetA = ...
[1,0;0,1];
resetc = ...
[-10;5];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 <= 0
c = [1;0];
d = 0;C = ...
[0,-1];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 1);

%% reset equation:
%   x1' := x1 + 5 && x2' := x2 - 10
resetA = ...
[1,0;0,1];
resetc = ...
[5;-10];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 <= 0
c = [0;-1];
d = 0;C = ...
[-1,0];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(2) = transition(guard, reset, 4);

loc(2) = location('topright', inv, trans, dynamics);



%--------------------------Location bottomleft-----------------------------

%% flow equation:
%   x1' = 1 && x2' = u1
dynA = ...
[0,0;0,0];
dynB = ...
[0;1];
dync = ...
[1;0];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x1 <= 0 && x2 <= 0
P_A = ...
[1,0;0,1];
P_b = ...
[-0;-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := x1 + 10 && x2' := x2 - 5
resetA = ...
[1,0;0,1];
resetc = ...
[10;-5];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 >= 0
c = [1;0];
d = 0;C = ...
[0,1];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 4);

%% reset equation:
%   x1' := x1 - 5 && x2' := x2 + 10
resetA = ...
[1,0;0,1];
resetc = ...
[-5;10];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 >= 0
c = [0;-1];
d = 0;C = ...
[1,0];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(2) = transition(guard, reset, 1);

loc(3) = location('bottomleft', inv, trans, dynamics);



%-------------------------Location bottomright-----------------------------

%% flow equation:
%   x1' = u1 && x2' = 1
dynA = ...
[0,0;0,0];
dynB = ...
[1;0];
dync = ...
[0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x1 >= 0 && x2 <= 0
P_A = ...
[-1,0;0,1];
P_b = ...
[-0;-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := x1 + 5 && x2' := x2 + 10
resetA = ...
[1,0;0,1];
resetc = ...
[5;10];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 >= 0
c = [0;-1];
d = 0;C = ...
[-1,0];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 2);

%% reset equation:
%   x1' := x1 - 10 && x2' := x2 - 5
resetA = ...
[1,0;0,1];
resetc = ...
[-10;-5];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 <= 0
c = [1;0];
d = 0;C = ...
[0,1];
D = [-0];

guard = conHyperplane(c,d,C,D);

trans(2) = transition(guard, reset, 3);

loc(4) = location('bottomright', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end