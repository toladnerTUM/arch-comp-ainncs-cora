function [dx]=test_hybrid_parallel_twocomp2_Comp2_Loc2_FlowEq(x,u)

dx(1,1) = x(3) - x(2)^2;

dx(2,1) = sin(x(1));

dx(3,1) = u(1) + cos(x(2));
