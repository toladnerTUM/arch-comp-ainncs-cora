function [dx]=test_hybrid_parallel_twocomp2_Comp1_Loc2_OutputEq(x,u)

dx(1,1) = 0.05*x(1) - 0.05*x(2)^2;
