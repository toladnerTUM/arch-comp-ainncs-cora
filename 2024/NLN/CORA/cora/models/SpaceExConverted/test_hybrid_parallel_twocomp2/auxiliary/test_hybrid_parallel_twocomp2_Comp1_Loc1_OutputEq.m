function [dx]=test_hybrid_parallel_twocomp2_Comp1_Loc1_OutputEq(x,u)

dx(1,1) = 0.05*x(2) + 0.05*x(1)^2;
