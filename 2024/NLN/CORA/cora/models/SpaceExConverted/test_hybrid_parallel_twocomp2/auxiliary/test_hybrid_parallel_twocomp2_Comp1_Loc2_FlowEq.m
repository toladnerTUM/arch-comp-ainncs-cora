function [dx]=test_hybrid_parallel_twocomp2_Comp1_Loc2_FlowEq(x,u)

dx(1,1) = u(2) - x(2)^3;

dx(2,1) = u(1) - cos(x(1));
