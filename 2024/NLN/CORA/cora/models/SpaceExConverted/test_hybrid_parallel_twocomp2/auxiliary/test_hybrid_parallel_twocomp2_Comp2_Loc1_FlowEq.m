function [dx]=test_hybrid_parallel_twocomp2_Comp2_Loc1_FlowEq(x,u)

dx(1,1) = x(2) - x(3)^2;

dx(2,1) = cos(x(1)) - u(1);

dx(3,1) = x(2)^2;
