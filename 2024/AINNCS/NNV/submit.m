% install and add paths

cd code/nnv;

run install.m;

matlabshared.supportpkg.setSupportPackageRoot('/usr/local/MATLAB/R2022b');
addpath(genpath('/usr/local/MATLAB'));

% check if on codeocean/docker, error out if not
try
    if ~is_codeocean() % test codeocean detection
        'ERROR: run_codeocean.m should only be executed on the codeocean platform'
        return;
    end
catch
    'ERROR: path likely not set, run install.m'
end

% default output path for path_results, ensure logs subdirectory there
if ~exist('/results/logs/','dir')
    mkdir('/results/logs/')
end

cd /code/nnv/examples/Submission/ARCH-COMP2024/benchmarks/

run_all % run all arch-comp results 
