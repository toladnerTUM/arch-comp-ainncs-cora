function run_all()

    % Reproduce AINNCS ARCH-COMP 2024 results
    % author: Diego Manzanas
    % date: June 20, 2024
    
    % Supress warnings
    % warning ('off','all');
    
    % Turn off figure display
    set(0,'DefaultFigureVisible','off');

    % Ideas for next year:
      % Try UNebraska methods for sigmoiod and tanh activations
      % A "smarter" partitioning scheme

%% Begin benchmark evaluation

    % ACC 
    cd ACC;
    disp("Verifying ACC...")
    try
        acc = reach(); % verified (~ 15 seconds)
        acc_res = 'verified';
    catch ME
        warning('ACC failed');
        warning(ME.message);
        acc = '-';
        acc_res = 'unknown';
    end
    cd ..;
    
    % Just in case, shut Down Current Parallel Pool
    poolobj = gcp('nocreate');
    delete(poolobj);

    % Benchmark-9 (Tora)
    cd Tora_Heterogeneous;
    disp("Verifying tora with relutanh...")
    try
        tora_relutanh = reachTora_reluTanh(); % verified w/ input partition (~ 38 mins)
        tora_relutanh_res = 'verified';
    catch ME
        warning('Tora with ReLU-Tanh failed');
        warning(ME.message);
        tora_relutanh = '-';
        tora_relutanh_res = 'unknown';
    end

    % Just in case, shut Down Current Parallel Pool
    poolobj = gcp('nocreate');
    delete(poolobj);
    
    disp("Verifying tora with sigmoid...")
    try
        tora_sigmoid = reachTora_sigmoid();   % verified w/ input partition (~ 1 hour)
        tora_sigmoid_res = 'verified';
    catch ME
        warning('Tora with sigmoid failed');
        warning(ME.message);
        tora_sigmoid_res = 'unknown';
        tora_sigmoid = '-';
    end
        
    cd ..;
    cd Benchmark9-Tora;
    disp("Verifying tora with relu")
    try
        tora_relu = reach(); % verified (~25 seconds)
        tora_relu_res = 'verified';
    catch ME
        warning('Tora with relu failed');
        warning(ME.message);
        tora_relu_res = 'unknown';
        tora_relu = '-';
    end
    cd ..;

    % Benchmark-10 (Unicycle)
    unicycle = '-'; % overapprox -> unknown

    % VCAS
    cd VCAS;
    vcas = run_vcas(); % verified (~20 seconds)
    cd ..;

    % Double Pendulum 
    cd Double_Pendulum; % new specifications are unknown
    dp_more = '-';
    dp_less = '-';
    cd ..;

    % Airplane
    cd Airplane;
    disp("Verifying airplane...")
    airplane = reach(); % Falsified (reach sets ~ 7 seconds)
    cd ..;

    % Attitude Control
    attitude = '-'; % sigmoid controller -> overapprox -> unknown

    % Quad
    quad = '-'; % sigmoid controller -> overapprox -> unknown

    % Spacecraft Docking 
    spacecraft = '-'; % overapproximation -> unknown

    %%%%%%%%% New from 2024 %%%%%%%%%%%

    % NAV

    cd NAV/;

    disp("Verifying NAV point instance...")
    % navPoint = reach_point(); % unknown despite partitions
    navPoint = '-';

    % Just in case, shut Down Current Parallel Pool
    poolobj = gcp('nocreate');
    delete(poolobj);

    disp("Verifying NAV set instance...")
    try
        navSet = reach_set(); % less than 1 hour to verify, many hours to visualize
        navSet_res = 'verified';
    catch ME
        warning('NAV set failed');
        warning(ME.message);
        navSet_res = 'unknown';
        navSet = '-';
    end


    cd ..;

    % Cartpole

    cd CartPole;
    cartpole = '-';
    cd ..;

    % Just in case, shut Down Current Parallel Pool
    poolobj = gcp('nocreate');
    delete(poolobj);
    
    % Single Pendulum 
    cd Single_Pendulum;
    disp("Verifying single pendulum...")
    try
        sp = reach(); % verified in less than an hour, plotting in a long time
        sp_res = 'verified';
    catch ME
        warning('Single pendulum failed');
        warning(ME.message);
        sp_res = 'unknown';
        sp = '-';
    end
        
    cd ..;

%% Save results in CSV file for submission

    %% Save results in CSV file for submission

    % Create results variable
    resultsCSV = cell(24, 4); % column names + # of benchmarks (including instances)
    resultsCSV(1, :) = {'benchmark','instance','result','time'}; % columns names

    % ACC
    acc_csv = {'ACC', 'safety', acc_res, acc};
    resultsCSV(2,:) = acc_csv;

    % Airplane
    airplane_csv = {'Airplane', 'continuous', 'violated', airplane};
    resultsCSV(3,:) = airplane_csv;

    % Attitude Control
    attitude_csv = {'AttitudeControl','avoid','unknown',attitude};
    resultsCSV(4,:) = attitude_csv;

    % Cartpole
    cartpole_csv = {'Cartpole', 'reach', 'unknown', cartpole};
    resultsCSV(5,:) = cartpole_csv;

    % Spacecraft Docking 
    docking_csv = {'Docking', 'constraint', 'unknown', spacecraft};
    resultsCSV(6,:) = docking_csv;

    % Double Pendulum
    dp_csv(1,:) = {'DoublePendulum', 'more robust', 'unknown', dp_more};
    dp_csv(2,:) = {'DoublePendulum', 'less robust', 'unknown', dp_less};
    resultsCSV(7:8, :) = dp_csv;

    % NAV
    dp_csv(1,:) = {'NAV', 'standard', 'unknown', navPoint};
    dp_csv(2,:) = {'NAV', 'robust', navSet_res, navSet};
    resultsCSV(9:10, :) = dp_csv;

    % Quad
    quad_csv = {'QUAD', 'reach', 'unknown', quad};
    resultsCSV(11,:) = quad_csv;

    % Single Pendulum 
    sp_csv = {'SinglePendulum', 'reach', sp_res, sp};
    resultsCSV(12,:) = sp_csv; 

    % Benchmark-9 (Tora)
    tora_csv(1,:) = {'TORA', 'remain',         tora_relu_res, tora_relu};
    tora_csv(2,:) = {'TORA', 'reach-tanh',     tora_relutanh_res, tora_relutanh};
    tora_csv(3,:) = {'TORA', 'reach-sigmoid',  tora_sigmoid_res, tora_sigmoid};
    resultsCSV(13:15,:) = tora_csv;

    % Benchmark-10 (Unicycle)
    unicycle_csv = {'Unicycle', 'reach', 'unknown', unicycle};
    resultsCSV(16,:) = unicycle_csv;

    % VCAS
    vcas_m19 = {'VCAS', 'middle19', 'verified', vcas(1,1)};
    vcas_m22 = {'VCAS', 'middle22', 'verified', vcas(2,1)};
    vcas_m25 = {'VCAS', 'middle25', 'violated', vcas(3,1)};
    vcas_m28 = {'VCAS', 'middle28', 'violated', vcas(4,1)};
    vcas_w19 = {'VCAS', 'worst19',  'verified', vcas(1,2)};
    vcas_w22 = {'VCAS', 'worst22',  'violated', vcas(2,2)};
    vcas_w25 = {'VCAS', 'worst25',  'violated', vcas(3,2)};
    vcas_w28 = {'VCAS', 'worst28',  'violated', vcas(4,2)};
    resultsCSV(17:24,:) = [vcas_m19; vcas_m22; vcas_m25; vcas_m28; vcas_w19; vcas_w22; vcas_w25; vcas_w28];


    % Create and save csv files with results
    if is_codeocean
        writecell(resultsCSV, '/results/results.csv'); % ARCH repeatability specific
    else
        writecell(resultsCSV, 'results.csv');
    end


end
