#!/bin/bash

docker build . -t "dummy-submit-$1" --no-cache
docker run --mac-address 12:34:56:78:9a:bc --name "dummy-submit-container-$1" "dummy-submit-$1" /bin/bash -c "matlab  -c "/license.lic" -nodisplay -r "submit""
docker cp "dummy-submit-container-$1":/results/ .
docker rm --force "dummy-submit-container-$1"
docker image rm --force "dummy-submit-$1"

