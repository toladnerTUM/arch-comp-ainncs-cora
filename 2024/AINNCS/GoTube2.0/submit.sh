#!/bin/bash

docker build . -t "dummy-submit-$1" --no-cache
docker run --name "dummy-submit-container-$1" -e XLA_PYTHON_CLIENT_MEM_FRACTION=.50 "dummy-submit-$1"
docker cp "dummy-submit-container-$1":/app/results/. ./results/
docker rm --force "dummy-submit-container-$1"
docker image rm --force "dummy-submit-$1"
