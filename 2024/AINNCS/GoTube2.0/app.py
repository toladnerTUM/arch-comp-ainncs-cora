import base64
import requests
import time
from yaspin import yaspin
from dotenv import load_dotenv
import os
import csv

# Dictionary of benchmark names and parameters
BENCHMARKS = {
    "ACC": {
        "benchmark": "ACC",
        "instance": "",
        "time_step": 0.1,
        "time_horizon": 5,
        "mu": 2.4,
        "gamma": 0.05,
        "center": [100, 32.1, 0, 10.5, 30.1, 0],
        "boundaries": [10, 0.1, 0, 0.5, 0.1, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "AttitudeControl": {
        "benchmark": "Attitude-Control",
        "instance": "",
        "time_step": 0.1,
        "time_horizon": 3,
        "mu": 1.001,
        "gamma": 0.05,
        "center": [-0.445, -0.545, 0.655, -0.745, 0.855, -0.645],
        "boundaries": [0.005, 0.005, 0.005, 0.005, 0.005, 0.005],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "QUAD": {
        "benchmark": "QUAD",
        "instance": "",
        "time_step": 0.1,
        "time_horizon": 5,
        "mu": 1.001,
        "gamma": 0.05,
        "center": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "boundaries": [0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0, 0, 0, 0, 0, 0],
        "plot_axis1": 1,
        "plot_axis2": 3,
        "include_dimensions": [1, 3]
    },
    "Airplane": {
        "benchmark": "Airplane",
        "instance": "",
        'time_step': 0.1,
        'time_horizon': 2,
        'mu': 1.001,
        "gamma": 0.05,
        "center": [0, 0, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0, 0, 0],
        "boundaries": [0, 0, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0, 0, 0],
        "plot_axis1": 2,
        "plot_axis2": 7,
        "include_dimensions": [2, 7]
    },
    "Docking": {
        "benchmark": "Docking",
        "instance": "full_initial_set_trajectories_only",
        'time_step': 1,
        'time_horizon': 30,
        'mu': 1.2,
        "gamma": 0.05,
        "center": [88, 88, 0, 0],
        "boundaries": [18, 18, 0.28, 0.28],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "DockingSafety": {
        "benchmark": "Docking",
        "instance": "half_initial_set_with_safety_property",
        'time_step': 1,
        'time_horizon': 30,
        'mu': 1.5,
        "gamma": 0.2,
        "center": [88, 88, 0, 0],
        "boundaries": [12, 12, 0.14, 0.14],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "DoublePendulumLessRobust": {
        "benchmark": "Double_Pendulum",
        "instance": "less_robust_controller",
        'time_step': 0.05,
        'time_horizon': 20 * 0.05,
        'mu': 1.001,
        "gamma": 0.05,
        "center": [1.15, 1.15, 1.15, 1.15],
        "boundaries": [0.15, 0.15, 0.15, 0.15],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "DoublePendulumMoreRobust": {
        "benchmark": "Double_Pendulum",
        "instance": "more_robust_controller",
        'time_step': 0.02,
        'time_horizon': 20 * 0.02,
        'mu': 1.001,
        "gamma": 0.05,
        "center": [1.15, 1.15, 1.15, 1.15],
        "boundaries": [0.15, 0.15, 0.15, 0.15],
        "plot_axis1": 3,
        "plot_axis2": 4,
        "include_dimensions": []
    },
    "SinglePendulum": {
        "benchmark": "Single_Pendulum",
        "instance": "",
        'time_step': 0.05,
        'time_horizon': 1,
        'mu': 1.001,
        "gamma": 0.05,
        "center": [1.0875, 0.1],
        "boundaries": [0.0875, 0.1],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "Tora1": {
        "benchmark": "Benchmark9-Tora",
        "instance": "",
        'time_step': 1,
        'time_horizon': 20,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [0.65, -0.65, -0.35, 0.55],
        "boundaries": [0.05, 0.05, 0.05, 0.05],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "Tora2Sigmoid": {
        "benchmark": "Tora_Heterogeneous",
        "instance": "sigmoid_controller",
        'time_step': 0.5,
        'time_horizon': 5,
        'mu': 1.2,
        "gamma": 0.05,
        "center": [-0.76, -0.44, 0.525, -0.29],
        "boundaries": [0.01, 0.01, 0.015, 0.01],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "Tora2ReLUTanh": {
        "benchmark": "Tora_Heterogeneous",
        "instance": "relu_tanh_controller",
        'time_step': 0.5,
        'time_horizon': 5,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-0.76, -0.44, 0.525, -0.29],
        "boundaries": [0.01, 0.01, 0.015, 0.01],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "Unicycle": {
        "benchmark": "Benchmark9-Unicycle",
        "instance": "",
        'time_step': 0.2,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [9.525, -4.475, 2.105, 1.505],
        "boundaries": [0.025, 0.025, 0.005, 0.005],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "VCASMidAccel_19p5": {
        "benchmark": "VCAS",
        "instance": "mid_acceleration_h0_19p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -19.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASMidAccel_22p5": {
        "benchmark": "VCAS",
        "instance": "mid_acceleration_h0_22p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -22.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASMidAccel_25p5": {
        "benchmark": "VCAS",
        "instance": "mid_acceleration_h0_25p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -25.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASMidAccel_28p5": {
        "benchmark": "VCAS",
        "instance": "mid_acceleration_h0_28p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -28.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASWorstAccel_19p5": {
        "benchmark": "VCAS",
        "instance": "worst_acceleration_h0_19p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -19.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASWorstAccel_22p5": {
        "benchmark": "VCAS",
        "instance": "worst_acceleration_h0_22p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -22.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASWorstAccel_25p5": {
        "benchmark": "VCAS",
        "instance": "worst_acceleration_h0_25p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -25.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "VCASWorstAccel_28p5": {
        "benchmark": "VCAS",
        "instance": "worst_acceleration_h0_28p5",
        'time_step': 1,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [-131, -28.5, 25],
        "boundaries": [2, 0.01, 0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": [1, 2]
    },
    "CartPole": {
        "benchmark": "CartPole",
        "instance": "",
        'time_step': 0.02,
        'time_horizon': 10,
        'mu': 1.5,
        "gamma": 0.05,
        "center": [0.0, 0.0, 0.0, 0.0],
        "boundaries": [0.1, 0.05, 0.1, 0.05],
        "plot_axis1": 1,
        "plot_axis2": 3,
        "include_dimensions": [1, 3, 4]
    },
    "NAVLessRobust": {
        "benchmark": "NAV",
        "instance": "less_robust_controller",
        'time_step': 0.2,
        'time_horizon': 6,
        'mu': 1.001,
        "gamma": 0.05,
        "center": [3.0, 3.0, 0.0, 0.0],
        "boundaries": [0.1, 0.1, 0.0, 0.0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    },
    "NAVMoreRobust": {
        "benchmark": "NAV",
        "instance": "more_robust_controller",
        "time_step": 0.2,
        "time_horizon": 6,
        "mu": 1.001,
        "gamma": 0.05,
        "center": [3.0, 3.0, 0.0, 0.0],
        "boundaries": [0.1, 0.1, 0.0, 0.0],
        "plot_axis1": 1,
        "plot_axis2": 2,
        "include_dimensions": []
    }                   
}

def main():

    # create result.csv & write headers
    with open('results/results.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['benchmark', 'instance', 'result', 'time'])

    for benchmark_name, parameters in BENCHMARKS.items():

        # special treatment for VCAS benchmark
        if 'VCAS' in benchmark_name:
            benchmark_name, accel = benchmark_name.split('_')
            print(f'Running benchmark: {benchmark_name} h0 = -{accel}')
        else:
            print(f'Running benchmark: {parameters["benchmark"]}')
        response = api_call(benchmark_name, parameters)
        if response is not None:
            data = [parameters['benchmark'], parameters['instance'], response['result'],
                    response['time']]
            b64_image = response.get('plot', None)
            if b64_image is not None:
                # save the plot to the results dir
                with open(f'results/{parameters["benchmark"]}{"_" + parameters["instance"] if parameters["instance"] else ""}_plot.html', 'wb') as f:
                    f.write(base64.b64decode(b64_image))

            if data:
                with open('results/results.csv', 'a', newline='') as csvfile:
                    # Create a CSV writer object
                    writer = csv.writer(csvfile)
                    writer.writerow(data)
        
with yaspin():
    time.sleep(3)  # time consuming code
    
@yaspin()
def api_call(benchmark_name, parameters):
    base_url = os.getenv("BASE_API_URL")
    url = base_url + benchmark_name
    
    try:
        headers = {
            'Authorization': os.getenv("TOKEN")
        }
        
        # Make the API call
        response = requests.post(url, headers=headers, json=parameters, timeout=None)

        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            # Process the response data here
            print()
            print(f"API call successful for {benchmark_name}")

            # modify response data for printing
            received_data = response.json()
            for key in ['result', 'time']:
                print(f'{key}:', received_data[key])
            print()
            return response.json()
        else:
            print(f"Failed to fetch data for {benchmark_name}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while fetching data for {benchmark_name}: {e}")


        
if __name__ == "__main__":
    # Load environment variables from .env file
    load_dotenv()
    main()