#!/bin/bash

docker build . -t "dummy-submit-$1" --no-cache
docker run -e MLM_LICENSE_FILE=28000@mlm1.rbg.tum.de --name "dummy-submit-container-$1" "dummy-submit-$1" -batch run
docker cp "dummy-submit-container-$1":/home/matlab/Documents/MATLAB/results/ ./results
docker rm --force "dummy-submit-container-$1"
docker image rm --force "dummy-submit-$1"

