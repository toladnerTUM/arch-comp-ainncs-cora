--------------------------------------------------------
ARCH'24 AINNCS Category
Tool: CORA - A Tool for Continuous Reachability Analysis
Date: 29-May-2024 08:02:55
--------------------------------------------------------
 
Running 23 Benchmarks.. 
 
--------------------------------------------------------
BENCHMARK: Sherlock-Benchmark 10 (Unicycle Car Model)
Time to compute random simulations: 2.0176
Time to check violation in simulations: 0.054033
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
Done.
Time to compute reachable set: 5.7719
Time to check verification: 0.047673
Total Time: 7.8913
Result: VERIFIED
Plotting..
 
'01_Unicycle_reach' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Adaptive Cruise Controller (ACC)
Time to compute random simulations: 1.3939
Time to check violation in simulations: 0.018055
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
     .. dynamics dim 7
Done.
Time to compute reachable set: 3.2278
Time to check verification: 0.077031
Total Time: 4.7167
Result: VERIFIED
Plotting..
 
'02_ACC_safe-distance' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Sherlock-Benchmark-9 (TORA)
Time to compute random simulations: 0.51544
Time to check violation in simulations: 0.032608
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
  .. compute symbolic third-order derivatives
     .. dynamic index 1,1
     .. dynamic index 1,2
     .. dynamic index 1,3
     .. dynamic index 1,4
     .. dynamic index 1,5
     .. dynamic index 1,6
     .. dynamic index 2,1
     .. dynamic index 2,2
     .. dynamic index 2,3
     .. dynamic index 2,4
     .. dynamic index 2,5
     .. dynamic index 2,6
     .. dynamic index 3,1
     .. dynamic index 3,2
     .. dynamic index 3,3
     .. dynamic index 3,4
     .. dynamic index 3,5
     .. dynamic index 3,6
     .. dynamic index 4,1
     .. dynamic index 4,2
     .. dynamic index 4,3
     .. dynamic index 4,4
     .. dynamic index 4,5
     .. dynamic index 4,6
     .. dynamic index 5,1
     .. dynamic index 5,2
     .. dynamic index 5,3
     .. dynamic index 5,4
     .. dynamic index 5,5
     .. dynamic index 5,6
     .. dynamic index 1,1
     .. dynamic index 1,2
     .. dynamic index 1,3
     .. dynamic index 1,4
     .. dynamic index 1,5
     .. dynamic index 1,6
     .. dynamic index 2,1
     .. dynamic index 2,2
     .. dynamic index 2,3
     .. dynamic index 2,4
     .. dynamic index 2,5
     .. dynamic index 2,6
     .. dynamic index 3,1
     .. dynamic index 3,2
     .. dynamic index 3,3
     .. dynamic index 3,4
     .. dynamic index 3,5
     .. dynamic index 3,6
     .. dynamic index 4,1
     .. dynamic index 4,2
     .. dynamic index 4,3
     .. dynamic index 4,4
     .. dynamic index 4,5
     .. dynamic index 4,6
     .. dynamic index 5,1
     .. dynamic index 5,2
     .. dynamic index 5,3
     .. dynamic index 5,4
     .. dynamic index 5,5
     .. dynamic index 5,6
Done.
Time to compute reachable set: 13.4135
Time to check verification: 1.061
Total Time: 15.0226
Result: VERIFIED
Plotting..
 
'03_TORA_remain' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: TORA Heterogeneous (tanh)
Time to compute random simulations: 0.61861
Time to check violation in simulations: 0.035327
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
  .. compute symbolic third-order derivatives
     .. dynamic index 1,1
     .. dynamic index 1,2
     .. dynamic index 1,3
     .. dynamic index 1,4
     .. dynamic index 1,5
     .. dynamic index 1,6
     .. dynamic index 2,1
     .. dynamic index 2,2
     .. dynamic index 2,3
     .. dynamic index 2,4
     .. dynamic index 2,5
     .. dynamic index 2,6
     .. dynamic index 3,1
     .. dynamic index 3,2
     .. dynamic index 3,3
     .. dynamic index 3,4
     .. dynamic index 3,5
     .. dynamic index 3,6
     .. dynamic index 4,1
     .. dynamic index 4,2
     .. dynamic index 4,3
     .. dynamic index 4,4
     .. dynamic index 4,5
     .. dynamic index 4,6
     .. dynamic index 5,1
     .. dynamic index 5,2
     .. dynamic index 5,3
     .. dynamic index 5,4
     .. dynamic index 5,5
     .. dynamic index 5,6
     .. dynamic index 1,1
     .. dynamic index 1,2
     .. dynamic index 1,3
     .. dynamic index 1,4
     .. dynamic index 1,5
     .. dynamic index 1,6
     .. dynamic index 2,1
     .. dynamic index 2,2
     .. dynamic index 2,3
     .. dynamic index 2,4
     .. dynamic index 2,5
     .. dynamic index 2,6
     .. dynamic index 3,1
     .. dynamic index 3,2
     .. dynamic index 3,3
     .. dynamic index 3,4
     .. dynamic index 3,5
     .. dynamic index 3,6
     .. dynamic index 4,1
     .. dynamic index 4,2
     .. dynamic index 4,3
     .. dynamic index 4,4
     .. dynamic index 4,5
     .. dynamic index 4,6
     .. dynamic index 5,1
     .. dynamic index 5,2
     .. dynamic index 5,3
     .. dynamic index 5,4
     .. dynamic index 5,5
     .. dynamic index 5,6
Done.
Time to compute reachable set: 11.7154
Time to check verification: 0.048249
Total Time: 12.4176
Result: VERIFIED
Plotting..
 
'04_TORA_reach-tanh' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: TORA Heterogeneous (sigmoid)
Time to compute random simulations: 0.65926
Time to check violation in simulations: 0.034462
Time to compute reachable set: 8.2526
Time to check verification: 0.030748
Total Time: 8.9771
Result: VERIFIED
Plotting..
 
'05_TORA_reach-sigmoid' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Single Pendulum
Time to compute random simulations: 0.41487
Time to check violation in simulations: 0.016154
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
Done.
Time to compute reachable set: 3.1107
Time to check verification: 0.098558
Total Time: 3.6403
Result: VERIFIED
Plotting..
 
'06_Single Pendulum_reach' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Double Pendulum (less robust)
Time to compute random simulations: 0.55905
Time to check violation in simulations: 0.024856
Computing multivariate derivatives for dynamic 'doublePendulumControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
Done.

Abort analysis due to reachable set explosion!
  Step 1 at time t=0
The reachable sets until the current step are returned.

Time to compute reachable set: 5.1721
Reachability analysis stopped early at t=0.
Result: UNKNOWN
Plotting..
 
'07_Double Pendulum_less-robust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Double Pendulum (more robust)
Time to compute random simulations: 0.54489
Time to check violation in simulations: 0.018793
Time to compute reachable set: 3.7026
Time to check verification: 0.078745
Total Time: 4.345
Result: VIOLATED
Plotting..
 
'08_Double Pendulum_more-robust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Airplane
Time to compute random simulations: 0.53319
Time to check violation in simulations: 0.005111
Computing multivariate derivatives for dynamic 'airplaneControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
     .. dynamics dim 7
     .. dynamics dim 8
     .. dynamics dim 9
     .. dynamics dim 10
     .. dynamics dim 11
     .. dynamics dim 12
     .. dynamics dim 13
     .. dynamics dim 14
     .. dynamics dim 15
     .. dynamics dim 16
     .. dynamics dim 17
     .. dynamics dim 18
Done.
Time to compute reachable set: 10.0876
Time to check verification: 0.058865
Total Time: 10.6848
Result: VIOLATED
Plotting..
 
'09_Airplane_continuous' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -19.5
Time to compute random simulations: 0.063018
Time to check violation in simulations: 0.016247
Time to compute reachable set: 0.12597
Time to check verification: 0.005613
Total Time: 0.21085
Result: VERIFIED
Plotting..
 
'10_VCAS_middle-19.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -22.5
Time to compute random simulations: 0.040855
Time to check violation in simulations: 0.001928
Time to compute reachable set: 0.054869
Time to check verification: 0.000808
Total Time: 0.09846
Result: VERIFIED
Plotting..
 
'11_VCAS_middle-22.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -25.5
Time to compute random simulations: 0.035802
Time to check violation in simulations: 0.001611
Total Time: 0.037413
Result: VIOLATED
Plotting..
 
'12_VCAS_middle-25.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -28.5
Time to compute random simulations: 0.067259
Time to check violation in simulations: 0.000906
Total Time: 0.068165
Result: VIOLATED
Plotting..
 
'13_VCAS_middle-28.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -19.5
Time to compute random simulations: 0.051315
Time to check violation in simulations: 0.002732
Time to compute reachable set: 0.072152
Time to check verification: 0.001632
Total Time: 0.12783
Result: VERIFIED
Plotting..
 
'14_VCAS_worst-19.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -22.5
Time to compute random simulations: 0.035485
Time to check violation in simulations: 0.001128
Total Time: 0.036613
Result: VIOLATED
Plotting..
 
'15_VCAS_worst-22.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -25.5
Time to compute random simulations: 0.035516
Time to check violation in simulations: 0.001111
Total Time: 0.036627
Result: VIOLATED
Plotting..
 
'16_VCAS_worst-25.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -28.5
Time to compute random simulations: 0.035211
Time to check violation in simulations: 0.000351
Total Time: 0.035562
Result: VIOLATED
Plotting..
 
'17_VCAS_worst-28.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Attitude Control
Time to compute random simulations: 0.72645
Time to check violation in simulations: 0.014116
Computing multivariate derivatives for dynamic 'dynamics_attitudeControlControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
     .. dynamics dim 7
     .. dynamics dim 8
     .. dynamics dim 9
Done.
Time to compute reachable set: 5.7368
Time to check verification: 0.032901
Total Time: 6.5103
Result: VERIFIED
Plotting..
 
'18_Attitude Control_avoid' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Quadrotor (QUAD)
Refined layer 3 from order 1 to 2!
Time to compute random simulations: 1.1876
Time to check violation in simulations: 0.020763
Computing multivariate derivatives for dynamic 'dynamics_quadControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
     .. dynamics dim 7
     .. dynamics dim 8
     .. dynamics dim 9
     .. dynamics dim 10
     .. dynamics dim 11
     .. dynamics dim 12
     .. dynamics dim 13
     .. dynamics dim 14
     .. dynamics dim 15
Done.
Time to compute reachable set: 213.5609
Time to check verification: 0.11506
Total Time: 214.8843
Result: VERIFIED
Plotting..
 
'19_QUAD_reach' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Spacecraft Docking
Time to compute random simulations: 9.842
Time to check violation in simulations: 0.004253
Computing multivariate derivatives for dynamic 'dynamics_spacecraftDockingControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
  .. compute symbolic third-order derivatives
     .. dynamic index 1,1
     .. dynamic index 1,2
     .. dynamic index 1,3
     .. dynamic index 1,4
     .. dynamic index 1,5
     .. dynamic index 1,6
     .. dynamic index 1,7
     .. dynamic index 2,1
     .. dynamic index 2,2
     .. dynamic index 2,3
     .. dynamic index 2,4
     .. dynamic index 2,5
     .. dynamic index 2,6
     .. dynamic index 2,7
     .. dynamic index 3,1
     .. dynamic index 3,2
     .. dynamic index 3,3
     .. dynamic index 3,4
     .. dynamic index 3,5
     .. dynamic index 3,6
     .. dynamic index 3,7
     .. dynamic index 4,1
     .. dynamic index 4,2
     .. dynamic index 4,3
     .. dynamic index 4,4
     .. dynamic index 4,5
     .. dynamic index 4,6
     .. dynamic index 4,7
     .. dynamic index 5,1
     .. dynamic index 5,2
     .. dynamic index 5,3
     .. dynamic index 5,4
     .. dynamic index 5,5
     .. dynamic index 5,6
     .. dynamic index 5,7
     .. dynamic index 6,1
     .. dynamic index 6,2
     .. dynamic index 6,3
     .. dynamic index 6,4
     .. dynamic index 6,5
     .. dynamic index 6,6
     .. dynamic index 6,7
     .. dynamic index 1,1
     .. dynamic index 1,2
     .. dynamic index 1,3
     .. dynamic index 1,4
     .. dynamic index 1,5
     .. dynamic index 1,6
     .. dynamic index 1,7
     .. dynamic index 2,1
     .. dynamic index 2,2
     .. dynamic index 2,3
     .. dynamic index 2,4
     .. dynamic index 2,5
     .. dynamic index 2,6
     .. dynamic index 2,7
     .. dynamic index 3,1
     .. dynamic index 3,2
     .. dynamic index 3,3
     .. dynamic index 3,4
     .. dynamic index 3,5
     .. dynamic index 3,6
     .. dynamic index 3,7
     .. dynamic index 4,1
     .. dynamic index 4,2
     .. dynamic index 4,3
     .. dynamic index 4,4
     .. dynamic index 4,5
     .. dynamic index 4,6
     .. dynamic index 4,7
     .. dynamic index 5,1
     .. dynamic index 5,2
     .. dynamic index 5,3
     .. dynamic index 5,4
     .. dynamic index 5,5
     .. dynamic index 5,6
     .. dynamic index 5,7
     .. dynamic index 6,1
     .. dynamic index 6,2
     .. dynamic index 6,3
     .. dynamic index 6,4
     .. dynamic index 6,5
     .. dynamic index 6,6
     .. dynamic index 6,7
Done.
Time to compute reachable set: 46.4838
Time to check verification: 23.2351
Total Time: 79.5651
Result: UNKNOWN
Plotting..
 
'20_Docking_constraint' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: NavigationTask (NAV-point)
Time to compute random simulations: 0.92532
Time to check violation in simulations: 0.043035
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
     .. dynamics dim 6
Done.
Time to compute reachable set: 7.4617
Reachability analysis stopped early at t=2.08.
Result: UNKNOWN
Plotting..
 
'21_NAV_standard' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: NavigationTask (NAV-set)
Time to compute random simulations: 0.70026
Time to check violation in simulations: 0.041188
Time to compute reachable set: 11.9922
Time to check verification: 1.1875
Total Time: 13.9212
Result: VERIFIED
Plotting..
 
'22_NAV_robust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Cartpole
Time to compute random simulations: 8.3295
Time to check violation in simulations: 0.029845
Computing multivariate derivatives for dynamic 'nonlinearSysControlled':
  .. compute symbolic Jacobian
  .. compute symbolic Jacobian (output)
  .. compute symbolic Hessians
     .. dynamics dim 1
     .. dynamics dim 2
     .. dynamics dim 3
     .. dynamics dim 4
     .. dynamics dim 5
Done.
Time to compute reachable set: 17.5168
Time to check verification: 0.22232
Total Time: 26.0984
Result: VERIFIED
Plotting..
 
'23_CartPol*_reach' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
 
Completed!
