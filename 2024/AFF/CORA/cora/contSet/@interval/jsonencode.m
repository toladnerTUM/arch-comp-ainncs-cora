function res = jsonencode(I, varargin)

    res.inf = num2str(I.inf, '%.2f');
    res.sup = num2str(I.sup, '%.2f');
    res = jsonencode(res);

end
