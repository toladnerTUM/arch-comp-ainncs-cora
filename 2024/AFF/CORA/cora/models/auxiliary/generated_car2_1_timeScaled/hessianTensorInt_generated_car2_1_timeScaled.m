function Hf=hessianTensorInt_generated_car2_1_timeScaled(x,u,p)



 Hf{1} = interval(sparse(6,6),sparse(6,6));

Hf{1}(5,1) = -1/p(1);
Hf{1}(1,5) = -1/p(1);


 Hf{2} = interval(sparse(6,6),sparse(6,6));

Hf{2}(6,1) = -1/p(1);
Hf{2}(1,6) = -1/p(1);


 Hf{3} = interval(sparse(6,6),sparse(6,6));

Hf{3}(1,1) = -(2*cos(x(2)))/p(1);
Hf{3}(2,1) = (x(1)*sin(x(2)))/p(1) + (sin(x(2))*(x(1) - 16))/p(1);
Hf{3}(1,2) = (x(1)*sin(x(2)))/p(1) + (sin(x(2))*(x(1) - 16))/p(1);
Hf{3}(2,2) = (x(1)*cos(x(2))*(x(1) - 16))/p(1);


 Hf{4} = interval(sparse(6,6),sparse(6,6));

Hf{4}(1,1) = -(2*sin(x(2)))/p(1);
Hf{4}(2,1) = - (x(1)*cos(x(2)))/p(1) - (cos(x(2))*(x(1) - 16))/p(1);
Hf{4}(1,2) = - (x(1)*cos(x(2)))/p(1) - (cos(x(2))*(x(1) - 16))/p(1);
Hf{4}(2,2) = (x(1)*sin(x(2))*(x(1) - 16))/p(1);

end