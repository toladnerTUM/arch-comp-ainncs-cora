function [A,B]=out_jacobian_vanderPolparamEq(x,u,p)

A{1}=[1,0;...
0,1];

A{2}=[0,0;...
0,0];

A{3}=[0,0;...
0,0];

B{1}=[0;...
0];

B{2}=[0;...
0];

B{3}=[0;...
0];

