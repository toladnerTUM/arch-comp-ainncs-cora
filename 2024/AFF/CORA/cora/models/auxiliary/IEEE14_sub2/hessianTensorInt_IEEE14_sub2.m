function [Hf,Hg]=hessianTensorInt_IEEE14_sub2(x,y,u)



 outDyn = funOptimizeDyn(x,y,u);



 outCon = funOptimizeCon(x,y,u);



 Hf{1} = interval(sparse(40,40),sparse(40,40));



 Hf{2} = interval(sparse(40,40),sparse(40,40));



 Hf{3} = interval(sparse(40,40),sparse(40,40));

Hf{3}(1,1) = outDyn(1);
Hf{3}(7,1) = outDyn(2);
Hf{3}(15,1) = outDyn(3);
Hf{3}(1,7) = outDyn(4);
Hf{3}(15,7) = outDyn(5);
Hf{3}(1,15) = outDyn(6);
Hf{3}(7,15) = outDyn(7);
Hf{3}(15,15) = outDyn(8);


 Hf{4} = interval(sparse(40,40),sparse(40,40));

Hf{4}(2,2) = outDyn(9);
Hf{4}(8,2) = outDyn(10);
Hf{4}(16,2) = outDyn(11);
Hf{4}(2,8) = outDyn(12);
Hf{4}(16,8) = outDyn(13);
Hf{4}(2,16) = outDyn(14);
Hf{4}(8,16) = outDyn(15);
Hf{4}(16,16) = outDyn(16);


 Hf{5} = interval(sparse(40,40),sparse(40,40));



 Hf{6} = interval(sparse(40,40),sparse(40,40));



 Hg{1} = interval(sparse(40,40),sparse(40,40));

Hg{1}(1,1) = outCon(1);
Hg{1}(7,1) = outCon(2);
Hg{1}(15,1) = outCon(3);
Hg{1}(1,7) = outCon(4);
Hg{1}(15,7) = outCon(5);
Hg{1}(15,9) = outCon(6);
Hg{1}(17,9) = outCon(7);
Hg{1}(1,15) = outCon(8);
Hg{1}(7,15) = outCon(9);
Hg{1}(9,15) = outCon(10);
Hg{1}(15,15) = outCon(11);
Hg{1}(17,15) = outCon(12);
Hg{1}(25,15) = outCon(13);
Hg{1}(29,15) = outCon(14);
Hg{1}(9,17) = outCon(15);
Hg{1}(15,17) = outCon(16);
Hg{1}(17,17) = outCon(17);
Hg{1}(15,25) = outCon(18);
Hg{1}(29,25) = outCon(19);
Hg{1}(15,29) = outCon(20);
Hg{1}(25,29) = outCon(21);
Hg{1}(29,29) = outCon(22);


 Hg{2} = interval(sparse(40,40),sparse(40,40));

Hg{2}(2,2) = outCon(23);
Hg{2}(8,2) = outCon(24);
Hg{2}(16,2) = outCon(25);
Hg{2}(2,8) = outCon(26);
Hg{2}(16,8) = outCon(27);
Hg{2}(16,10) = outCon(28);
Hg{2}(18,10) = outCon(29);
Hg{2}(2,16) = outCon(30);
Hg{2}(8,16) = outCon(31);
Hg{2}(10,16) = outCon(32);
Hg{2}(16,16) = outCon(33);
Hg{2}(18,16) = outCon(34);
Hg{2}(10,18) = outCon(35);
Hg{2}(16,18) = outCon(36);
Hg{2}(18,18) = outCon(37);


 Hg{3} = interval(sparse(40,40),sparse(40,40));

Hg{3}(9,9) = outCon(38);
Hg{3}(10,9) = outCon(39);
Hg{3}(11,9) = outCon(40);
Hg{3}(15,9) = outCon(41);
Hg{3}(17,9) = outCon(42);
Hg{3}(18,9) = outCon(43);
Hg{3}(19,9) = outCon(44);
Hg{3}(25,9) = outCon(45);
Hg{3}(26,9) = outCon(46);
Hg{3}(29,9) = outCon(47);
Hg{3}(30,9) = outCon(48);
Hg{3}(9,10) = outCon(49);
Hg{3}(17,10) = outCon(50);
Hg{3}(18,10) = outCon(51);
Hg{3}(9,11) = outCon(52);
Hg{3}(17,11) = outCon(53);
Hg{3}(19,11) = outCon(54);
Hg{3}(9,15) = outCon(55);
Hg{3}(15,15) = outCon(56);
Hg{3}(17,15) = outCon(57);
Hg{3}(9,17) = outCon(58);
Hg{3}(10,17) = outCon(59);
Hg{3}(11,17) = outCon(60);
Hg{3}(15,17) = outCon(61);
Hg{3}(17,17) = outCon(62);
Hg{3}(18,17) = outCon(63);
Hg{3}(19,17) = outCon(64);
Hg{3}(25,17) = outCon(65);
Hg{3}(26,17) = outCon(66);
Hg{3}(29,17) = outCon(67);
Hg{3}(30,17) = outCon(68);
Hg{3}(9,18) = outCon(69);
Hg{3}(10,18) = outCon(70);
Hg{3}(17,18) = outCon(71);
Hg{3}(18,18) = outCon(72);
Hg{3}(9,19) = outCon(73);
Hg{3}(11,19) = outCon(74);
Hg{3}(17,19) = outCon(75);
Hg{3}(19,19) = outCon(76);
Hg{3}(9,25) = outCon(77);
Hg{3}(17,25) = outCon(78);
Hg{3}(29,25) = outCon(79);
Hg{3}(9,26) = outCon(80);
Hg{3}(17,26) = outCon(81);
Hg{3}(30,26) = outCon(82);
Hg{3}(9,29) = outCon(83);
Hg{3}(17,29) = outCon(84);
Hg{3}(25,29) = outCon(85);
Hg{3}(29,29) = outCon(86);
Hg{3}(9,30) = outCon(87);
Hg{3}(17,30) = outCon(88);
Hg{3}(26,30) = outCon(89);
Hg{3}(30,30) = outCon(90);


 Hg{4} = interval(sparse(40,40),sparse(40,40));

Hg{4}(10,9) = outCon(91);
Hg{4}(17,9) = outCon(92);
Hg{4}(18,9) = outCon(93);
Hg{4}(9,10) = outCon(94);
Hg{4}(11,10) = outCon(95);
Hg{4}(16,10) = outCon(96);
Hg{4}(17,10) = outCon(97);
Hg{4}(18,10) = outCon(98);
Hg{4}(19,10) = outCon(99);
Hg{4}(10,11) = outCon(100);
Hg{4}(18,11) = outCon(101);
Hg{4}(19,11) = outCon(102);
Hg{4}(10,16) = outCon(103);
Hg{4}(16,16) = outCon(104);
Hg{4}(18,16) = outCon(105);
Hg{4}(9,17) = outCon(106);
Hg{4}(10,17) = outCon(107);
Hg{4}(17,17) = outCon(108);
Hg{4}(18,17) = outCon(109);
Hg{4}(9,18) = outCon(110);
Hg{4}(10,18) = outCon(111);
Hg{4}(11,18) = outCon(112);
Hg{4}(16,18) = outCon(113);
Hg{4}(17,18) = outCon(114);
Hg{4}(18,18) = outCon(115);
Hg{4}(19,18) = outCon(116);
Hg{4}(10,19) = outCon(117);
Hg{4}(11,19) = outCon(118);
Hg{4}(18,19) = outCon(119);
Hg{4}(19,19) = outCon(120);


 Hg{5} = interval(sparse(40,40),sparse(40,40));

Hg{5}(11,9) = outCon(121);
Hg{5}(17,9) = outCon(122);
Hg{5}(19,9) = outCon(123);
Hg{5}(11,10) = outCon(124);
Hg{5}(18,10) = outCon(125);
Hg{5}(19,10) = outCon(126);
Hg{5}(9,11) = outCon(127);
Hg{5}(10,11) = outCon(128);
Hg{5}(11,11) = outCon(129);
Hg{5}(12,11) = outCon(130);
Hg{5}(14,11) = outCon(131);
Hg{5}(17,11) = outCon(132);
Hg{5}(18,11) = outCon(133);
Hg{5}(19,11) = outCon(134);
Hg{5}(20,11) = outCon(135);
Hg{5}(22,11) = outCon(136);
Hg{5}(11,12) = outCon(137);
Hg{5}(19,12) = outCon(138);
Hg{5}(20,12) = outCon(139);
Hg{5}(11,14) = outCon(140);
Hg{5}(19,14) = outCon(141);
Hg{5}(22,14) = outCon(142);
Hg{5}(9,17) = outCon(143);
Hg{5}(11,17) = outCon(144);
Hg{5}(17,17) = outCon(145);
Hg{5}(19,17) = outCon(146);
Hg{5}(10,18) = outCon(147);
Hg{5}(11,18) = outCon(148);
Hg{5}(18,18) = outCon(149);
Hg{5}(19,18) = outCon(150);
Hg{5}(9,19) = outCon(151);
Hg{5}(10,19) = outCon(152);
Hg{5}(11,19) = outCon(153);
Hg{5}(12,19) = outCon(154);
Hg{5}(14,19) = outCon(155);
Hg{5}(17,19) = outCon(156);
Hg{5}(18,19) = outCon(157);
Hg{5}(19,19) = outCon(158);
Hg{5}(20,19) = outCon(159);
Hg{5}(22,19) = outCon(160);
Hg{5}(11,20) = outCon(161);
Hg{5}(12,20) = outCon(162);
Hg{5}(19,20) = outCon(163);
Hg{5}(20,20) = outCon(164);
Hg{5}(11,22) = outCon(165);
Hg{5}(14,22) = outCon(166);
Hg{5}(19,22) = outCon(167);
Hg{5}(22,22) = outCon(168);


 Hg{6} = interval(sparse(40,40),sparse(40,40));

Hg{6}(12,11) = outCon(169);
Hg{6}(19,11) = outCon(170);
Hg{6}(20,11) = outCon(171);
Hg{6}(11,12) = outCon(172);
Hg{6}(12,12) = outCon(173);
Hg{6}(13,12) = outCon(174);
Hg{6}(19,12) = outCon(175);
Hg{6}(20,12) = outCon(176);
Hg{6}(21,12) = outCon(177);
Hg{6}(12,13) = outCon(178);
Hg{6}(20,13) = outCon(179);
Hg{6}(21,13) = outCon(180);
Hg{6}(11,19) = outCon(181);
Hg{6}(12,19) = outCon(182);
Hg{6}(19,19) = outCon(183);
Hg{6}(20,19) = outCon(184);
Hg{6}(11,20) = outCon(185);
Hg{6}(12,20) = outCon(186);
Hg{6}(13,20) = outCon(187);
Hg{6}(19,20) = outCon(188);
Hg{6}(20,20) = outCon(189);
Hg{6}(21,20) = outCon(190);
Hg{6}(12,21) = outCon(191);
Hg{6}(13,21) = outCon(192);
Hg{6}(20,21) = outCon(193);
Hg{6}(21,21) = outCon(194);


 Hg{7} = interval(sparse(40,40),sparse(40,40));

Hg{7}(13,12) = outCon(195);
Hg{7}(20,12) = outCon(196);
Hg{7}(21,12) = outCon(197);
Hg{7}(12,13) = outCon(198);
Hg{7}(13,13) = outCon(199);
Hg{7}(20,13) = outCon(200);
Hg{7}(21,13) = outCon(201);
Hg{7}(27,13) = outCon(202);
Hg{7}(31,13) = outCon(203);
Hg{7}(12,20) = outCon(204);
Hg{7}(13,20) = outCon(205);
Hg{7}(20,20) = outCon(206);
Hg{7}(21,20) = outCon(207);
Hg{7}(12,21) = outCon(208);
Hg{7}(13,21) = outCon(209);
Hg{7}(20,21) = outCon(210);
Hg{7}(21,21) = outCon(211);
Hg{7}(27,21) = outCon(212);
Hg{7}(31,21) = outCon(213);
Hg{7}(13,27) = outCon(214);
Hg{7}(21,27) = outCon(215);
Hg{7}(31,27) = outCon(216);
Hg{7}(13,31) = outCon(217);
Hg{7}(21,31) = outCon(218);
Hg{7}(27,31) = outCon(219);
Hg{7}(31,31) = outCon(220);


 Hg{8} = interval(sparse(40,40),sparse(40,40));

Hg{8}(14,11) = outCon(221);
Hg{8}(19,11) = outCon(222);
Hg{8}(22,11) = outCon(223);
Hg{8}(11,14) = outCon(224);
Hg{8}(14,14) = outCon(225);
Hg{8}(19,14) = outCon(226);
Hg{8}(22,14) = outCon(227);
Hg{8}(28,14) = outCon(228);
Hg{8}(32,14) = outCon(229);
Hg{8}(11,19) = outCon(230);
Hg{8}(14,19) = outCon(231);
Hg{8}(19,19) = outCon(232);
Hg{8}(22,19) = outCon(233);
Hg{8}(11,22) = outCon(234);
Hg{8}(14,22) = outCon(235);
Hg{8}(19,22) = outCon(236);
Hg{8}(22,22) = outCon(237);
Hg{8}(28,22) = outCon(238);
Hg{8}(32,22) = outCon(239);
Hg{8}(14,28) = outCon(240);
Hg{8}(22,28) = outCon(241);
Hg{8}(32,28) = outCon(242);
Hg{8}(14,32) = outCon(243);
Hg{8}(22,32) = outCon(244);
Hg{8}(28,32) = outCon(245);
Hg{8}(32,32) = outCon(246);


 Hg{9} = interval(sparse(40,40),sparse(40,40));

Hg{9}(1,1) = outCon(247);
Hg{9}(7,1) = outCon(248);
Hg{9}(15,1) = outCon(249);
Hg{9}(1,7) = outCon(250);
Hg{9}(15,7) = outCon(251);
Hg{9}(15,9) = outCon(252);
Hg{9}(17,9) = outCon(253);
Hg{9}(1,15) = outCon(254);
Hg{9}(7,15) = outCon(255);
Hg{9}(9,15) = outCon(256);
Hg{9}(15,15) = outCon(257);
Hg{9}(17,15) = outCon(258);
Hg{9}(25,15) = outCon(259);
Hg{9}(29,15) = outCon(260);
Hg{9}(9,17) = outCon(261);
Hg{9}(15,17) = outCon(262);
Hg{9}(17,17) = outCon(263);
Hg{9}(15,25) = outCon(264);
Hg{9}(29,25) = outCon(265);
Hg{9}(15,29) = outCon(266);
Hg{9}(25,29) = outCon(267);
Hg{9}(29,29) = outCon(268);


 Hg{10} = interval(sparse(40,40),sparse(40,40));

Hg{10}(2,2) = outCon(269);
Hg{10}(8,2) = outCon(270);
Hg{10}(16,2) = outCon(271);
Hg{10}(2,8) = outCon(272);
Hg{10}(16,8) = outCon(273);
Hg{10}(16,10) = outCon(274);
Hg{10}(18,10) = outCon(275);
Hg{10}(2,16) = outCon(276);
Hg{10}(8,16) = outCon(277);
Hg{10}(10,16) = outCon(278);
Hg{10}(16,16) = outCon(279);
Hg{10}(18,16) = outCon(280);
Hg{10}(10,18) = outCon(281);
Hg{10}(16,18) = outCon(282);
Hg{10}(18,18) = outCon(283);


 Hg{11} = interval(sparse(40,40),sparse(40,40));

Hg{11}(9,9) = outCon(284);
Hg{11}(10,9) = outCon(285);
Hg{11}(11,9) = outCon(286);
Hg{11}(15,9) = outCon(287);
Hg{11}(17,9) = outCon(288);
Hg{11}(18,9) = outCon(289);
Hg{11}(19,9) = outCon(290);
Hg{11}(25,9) = outCon(291);
Hg{11}(26,9) = outCon(292);
Hg{11}(29,9) = outCon(293);
Hg{11}(30,9) = outCon(294);
Hg{11}(9,10) = outCon(295);
Hg{11}(17,10) = outCon(296);
Hg{11}(18,10) = outCon(297);
Hg{11}(9,11) = outCon(298);
Hg{11}(17,11) = outCon(299);
Hg{11}(19,11) = outCon(300);
Hg{11}(9,15) = outCon(301);
Hg{11}(15,15) = outCon(302);
Hg{11}(17,15) = outCon(303);
Hg{11}(9,17) = outCon(304);
Hg{11}(10,17) = outCon(305);
Hg{11}(11,17) = outCon(306);
Hg{11}(15,17) = outCon(307);
Hg{11}(17,17) = outCon(308);
Hg{11}(18,17) = outCon(309);
Hg{11}(19,17) = outCon(310);
Hg{11}(25,17) = outCon(311);
Hg{11}(26,17) = outCon(312);
Hg{11}(29,17) = outCon(313);
Hg{11}(30,17) = outCon(314);
Hg{11}(9,18) = outCon(315);
Hg{11}(10,18) = outCon(316);
Hg{11}(17,18) = outCon(317);
Hg{11}(18,18) = outCon(318);
Hg{11}(9,19) = outCon(319);
Hg{11}(11,19) = outCon(320);
Hg{11}(17,19) = outCon(321);
Hg{11}(19,19) = outCon(322);
Hg{11}(9,25) = outCon(323);
Hg{11}(17,25) = outCon(324);
Hg{11}(29,25) = outCon(325);
Hg{11}(9,26) = outCon(326);
Hg{11}(17,26) = outCon(327);
Hg{11}(30,26) = outCon(328);
Hg{11}(9,29) = outCon(329);
Hg{11}(17,29) = outCon(330);
Hg{11}(25,29) = outCon(331);
Hg{11}(29,29) = outCon(332);
Hg{11}(9,30) = outCon(333);
Hg{11}(17,30) = outCon(334);
Hg{11}(26,30) = outCon(335);
Hg{11}(30,30) = outCon(336);


 Hg{12} = interval(sparse(40,40),sparse(40,40));

Hg{12}(10,9) = outCon(337);
Hg{12}(17,9) = outCon(338);
Hg{12}(18,9) = outCon(339);
Hg{12}(9,10) = outCon(340);
Hg{12}(10,10) = outCon(341);
Hg{12}(11,10) = outCon(342);
Hg{12}(16,10) = outCon(343);
Hg{12}(17,10) = outCon(344);
Hg{12}(18,10) = outCon(345);
Hg{12}(19,10) = outCon(346);
Hg{12}(10,11) = outCon(347);
Hg{12}(18,11) = outCon(348);
Hg{12}(19,11) = outCon(349);
Hg{12}(10,16) = outCon(350);
Hg{12}(16,16) = outCon(351);
Hg{12}(18,16) = outCon(352);
Hg{12}(9,17) = outCon(353);
Hg{12}(10,17) = outCon(354);
Hg{12}(17,17) = outCon(355);
Hg{12}(18,17) = outCon(356);
Hg{12}(9,18) = outCon(357);
Hg{12}(10,18) = outCon(358);
Hg{12}(11,18) = outCon(359);
Hg{12}(16,18) = outCon(360);
Hg{12}(17,18) = outCon(361);
Hg{12}(18,18) = outCon(362);
Hg{12}(19,18) = outCon(363);
Hg{12}(10,19) = outCon(364);
Hg{12}(11,19) = outCon(365);
Hg{12}(18,19) = outCon(366);
Hg{12}(19,19) = outCon(367);


 Hg{13} = interval(sparse(40,40),sparse(40,40));

Hg{13}(11,9) = outCon(368);
Hg{13}(17,9) = outCon(369);
Hg{13}(19,9) = outCon(370);
Hg{13}(11,10) = outCon(371);
Hg{13}(18,10) = outCon(372);
Hg{13}(19,10) = outCon(373);
Hg{13}(9,11) = outCon(374);
Hg{13}(10,11) = outCon(375);
Hg{13}(11,11) = outCon(376);
Hg{13}(12,11) = outCon(377);
Hg{13}(14,11) = outCon(378);
Hg{13}(17,11) = outCon(379);
Hg{13}(18,11) = outCon(380);
Hg{13}(19,11) = outCon(381);
Hg{13}(20,11) = outCon(382);
Hg{13}(22,11) = outCon(383);
Hg{13}(11,12) = outCon(384);
Hg{13}(19,12) = outCon(385);
Hg{13}(20,12) = outCon(386);
Hg{13}(11,14) = outCon(387);
Hg{13}(19,14) = outCon(388);
Hg{13}(22,14) = outCon(389);
Hg{13}(9,17) = outCon(390);
Hg{13}(11,17) = outCon(391);
Hg{13}(17,17) = outCon(392);
Hg{13}(19,17) = outCon(393);
Hg{13}(10,18) = outCon(394);
Hg{13}(11,18) = outCon(395);
Hg{13}(18,18) = outCon(396);
Hg{13}(19,18) = outCon(397);
Hg{13}(9,19) = outCon(398);
Hg{13}(10,19) = outCon(399);
Hg{13}(11,19) = outCon(400);
Hg{13}(12,19) = outCon(401);
Hg{13}(14,19) = outCon(402);
Hg{13}(17,19) = outCon(403);
Hg{13}(18,19) = outCon(404);
Hg{13}(19,19) = outCon(405);
Hg{13}(20,19) = outCon(406);
Hg{13}(22,19) = outCon(407);
Hg{13}(11,20) = outCon(408);
Hg{13}(12,20) = outCon(409);
Hg{13}(19,20) = outCon(410);
Hg{13}(20,20) = outCon(411);
Hg{13}(11,22) = outCon(412);
Hg{13}(14,22) = outCon(413);
Hg{13}(19,22) = outCon(414);
Hg{13}(22,22) = outCon(415);


 Hg{14} = interval(sparse(40,40),sparse(40,40));

Hg{14}(12,11) = outCon(416);
Hg{14}(19,11) = outCon(417);
Hg{14}(20,11) = outCon(418);
Hg{14}(11,12) = outCon(419);
Hg{14}(12,12) = outCon(420);
Hg{14}(13,12) = outCon(421);
Hg{14}(19,12) = outCon(422);
Hg{14}(20,12) = outCon(423);
Hg{14}(21,12) = outCon(424);
Hg{14}(12,13) = outCon(425);
Hg{14}(20,13) = outCon(426);
Hg{14}(21,13) = outCon(427);
Hg{14}(11,19) = outCon(428);
Hg{14}(12,19) = outCon(429);
Hg{14}(19,19) = outCon(430);
Hg{14}(20,19) = outCon(431);
Hg{14}(11,20) = outCon(432);
Hg{14}(12,20) = outCon(433);
Hg{14}(13,20) = outCon(434);
Hg{14}(19,20) = outCon(435);
Hg{14}(20,20) = outCon(436);
Hg{14}(21,20) = outCon(437);
Hg{14}(12,21) = outCon(438);
Hg{14}(13,21) = outCon(439);
Hg{14}(20,21) = outCon(440);
Hg{14}(21,21) = outCon(441);


 Hg{15} = interval(sparse(40,40),sparse(40,40));

Hg{15}(13,12) = outCon(442);
Hg{15}(20,12) = outCon(443);
Hg{15}(21,12) = outCon(444);
Hg{15}(12,13) = outCon(445);
Hg{15}(13,13) = outCon(446);
Hg{15}(20,13) = outCon(447);
Hg{15}(21,13) = outCon(448);
Hg{15}(27,13) = outCon(449);
Hg{15}(31,13) = outCon(450);
Hg{15}(12,20) = outCon(451);
Hg{15}(13,20) = outCon(452);
Hg{15}(20,20) = outCon(453);
Hg{15}(21,20) = outCon(454);
Hg{15}(12,21) = outCon(455);
Hg{15}(13,21) = outCon(456);
Hg{15}(20,21) = outCon(457);
Hg{15}(21,21) = outCon(458);
Hg{15}(27,21) = outCon(459);
Hg{15}(31,21) = outCon(460);
Hg{15}(13,27) = outCon(461);
Hg{15}(21,27) = outCon(462);
Hg{15}(31,27) = outCon(463);
Hg{15}(13,31) = outCon(464);
Hg{15}(21,31) = outCon(465);
Hg{15}(27,31) = outCon(466);
Hg{15}(31,31) = outCon(467);


 Hg{16} = interval(sparse(40,40),sparse(40,40));

Hg{16}(14,11) = outCon(468);
Hg{16}(19,11) = outCon(469);
Hg{16}(22,11) = outCon(470);
Hg{16}(11,14) = outCon(471);
Hg{16}(14,14) = outCon(472);
Hg{16}(19,14) = outCon(473);
Hg{16}(22,14) = outCon(474);
Hg{16}(28,14) = outCon(475);
Hg{16}(32,14) = outCon(476);
Hg{16}(11,19) = outCon(477);
Hg{16}(14,19) = outCon(478);
Hg{16}(19,19) = outCon(479);
Hg{16}(22,19) = outCon(480);
Hg{16}(11,22) = outCon(481);
Hg{16}(14,22) = outCon(482);
Hg{16}(19,22) = outCon(483);
Hg{16}(22,22) = outCon(484);
Hg{16}(28,22) = outCon(485);
Hg{16}(32,22) = outCon(486);
Hg{16}(14,28) = outCon(487);
Hg{16}(22,28) = outCon(488);
Hg{16}(32,28) = outCon(489);
Hg{16}(14,32) = outCon(490);
Hg{16}(22,32) = outCon(491);
Hg{16}(28,32) = outCon(492);
Hg{16}(32,32) = outCon(493);

end

function outDyn = funOptimizeDyn(in1,in2,in3)
%funOptimizeDyn
%    outDyn = funOptimizeDyn(IN1,IN2,IN3)

%    This function was generated by the Symbolic Math Toolbox version 9.1.
%    23-Feb-2024 17:19:33

xL1R = in1(1,:);
xL2R = in1(2,:);
yL1R = in2(1,:);
yL2R = in2(2,:);
yL9R = in2(9,:);
yL10R = in2(10,:);
t2 = -yL9R;
t3 = -yL10R;
t4 = t2+xL1R;
t5 = t3+xL2R;
t6 = cos(t4);
t7 = cos(t5);
t8 = sin(t4);
t9 = sin(t5);
t10 = t6.*pi.*(3.03e+2./4.0);
t11 = t7.*pi.*(3.27e+2./4.0);
t12 = t8.*yL1R.*pi.*(3.03e+2./4.0);
t13 = t9.*yL2R.*pi.*(3.27e+2./4.0);
t14 = -t10;
t15 = -t11;
t16 = -t12;
t17 = -t13;
outDyn = [t12;t14;t16;t14;t10;t16;t10;t12;t13;t15;t17;t15;t11;t17;t11;t13];

end


function outCon = funOptimizeCon(in1,in2,in3)
%funOptimizeCon
%    outCon = funOptimizeCon(IN1,IN2,IN3)

%    This function was generated by the Symbolic Math Toolbox version 9.1.
%    23-Feb-2024 17:19:34

uL3R = in3(3,:);
uL4R = in3(4,:);
uL5R = in3(5,:);
uL6R = in3(6,:);
uL7R = in3(7,:);
uL8R = in3(8,:);
uL9R = in3(9,:);
uL10R = in3(10,:);
xL1R = in1(1,:);
xL2R = in1(2,:);
yL1R = in2(1,:);
yL2R = in2(2,:);
yL3R = in2(3,:);
yL4R = in2(4,:);
yL5R = in2(5,:);
yL6R = in2(6,:);
yL7R = in2(7,:);
yL8R = in2(8,:);
yL9R = in2(9,:);
yL10R = in2(10,:);
yL11R = in2(11,:);
yL12R = in2(12,:);
yL13R = in2(13,:);
yL14R = in2(14,:);
yL15R = in2(15,:);
yL16R = in2(16,:);
t2 = -yL9R;
t3 = -yL10R;
t4 = -yL11R;
t5 = -yL12R;
t6 = -yL13R;
t7 = -yL14R;
t8 = -yL15R;
t9 = -yL16R;
t10 = pi./2.0;
t11 = t2+xL1R;
t12 = t3+xL2R;
t17 = t5+t10+yL10R;
t18 = t3+t10+yL12R;
t19 = t5+t10+yL11R;
t20 = t4+t10+yL12R;
t21 = t6+t10+yL11R;
t22 = t4+t10+yL13R;
t23 = t6+t10+yL12R;
t24 = t5+t10+yL13R;
t103 = t9+uL10R+2.027338849872577;
t104 = t8+yL14R+1.974518772970189;
t105 = t7+yL15R+1.974518772970189;
t106 = t9+yL13R+2.010252330516564;
t107 = t6+yL16R+2.010252330516564;
t108 = t7+yL13R+1.930837364399918;
t109 = t6+yL14R+1.930837364399918;
t110 = t4+uL7R+1.889157179148782;
t111 = t8+uL9R+2.016303955717304;
t112 = t4+uL8R+1.877799954880152;
t113 = t2+uL7R+1.803842837224271;
t114 = t4+yL9R+1.944216073043588;
t115 = t2+yL11R+1.944216073043588;
t13 = cos(t11);
t14 = cos(t12);
t15 = sin(t11);
t16 = sin(t12);
t25 = cos(t17);
t26 = cos(t18);
t27 = cos(t19);
t28 = cos(t20);
t29 = cos(t21);
t30 = cos(t22);
t31 = cos(t23);
t32 = cos(t24);
t33 = sin(t17);
t34 = sin(t18);
t35 = sin(t19);
t36 = sin(t20);
t37 = sin(t21);
t38 = sin(t22);
t39 = sin(t23);
t40 = sin(t24);
t116 = sin(t110);
t117 = cos(t111);
t118 = cos(t112);
t119 = sin(t111);
t120 = sin(t112);
t121 = cos(t113);
t122 = cos(t114);
t123 = cos(t115);
t124 = sin(t113);
t125 = sin(t114);
t126 = sin(t115);
t127 = cos(t103);
t128 = sin(t103);
t129 = cos(t104);
t130 = cos(t105);
t131 = sin(t104);
t132 = sin(t105);
t133 = cos(t106);
t134 = cos(t107);
t135 = sin(t106);
t136 = sin(t107);
t137 = cos(t108);
t138 = cos(t109);
t139 = sin(t108);
t140 = sin(t109);
t141 = cos(t110);
t41 = t13.*(1.01e+2./2.0e+1);
t42 = t14.*(1.09e+2./2.0e+1);
t43 = t15.*(1.01e+2./2.0e+1);
t44 = t16.*(1.09e+2./2.0e+1);
t53 = t13.*yL1R.*(-1.01e+2./2.0e+1);
t54 = t14.*yL2R.*(-1.09e+2./2.0e+1);
t55 = t15.*yL1R.*(-1.01e+2./2.0e+1);
t56 = t16.*yL2R.*(-1.09e+2./2.0e+1);
t57 = t25.*6.187908032926483;
t58 = t26.*6.187908032926483;
t59 = t33.*6.187908032926483;
t60 = t34.*6.187908032926483;
t69 = t31.*9.09008271975275;
t70 = t32.*9.09008271975275;
t71 = t39.*9.09008271975275;
t72 = t40.*9.09008271975275;
t73 = t25.*yL4R.*(-6.187908032926483);
t74 = t26.*yL4R.*(-6.187908032926483);
t75 = t33.*yL4R.*(-6.187908032926483);
t76 = t34.*yL4R.*(-6.187908032926483);
t91 = t31.*yL4R.*(-9.09008271975275);
t92 = t32.*yL4R.*(-9.09008271975275);
t93 = t31.*yL5R.*(-9.09008271975275);
t94 = t32.*yL5R.*(-9.09008271975275);
t95 = t39.*yL4R.*(-9.09008271975275);
t96 = t40.*yL4R.*(-9.09008271975275);
t97 = t39.*yL5R.*(-9.09008271975275);
t98 = t40.*yL5R.*(-9.09008271975275);
t142 = t29.*1.8554995578159;
t143 = t30.*1.8554995578159;
t144 = t37.*1.8554995578159;
t145 = t38.*1.8554995578159;
t146 = t27.*4.889512660317341;
t147 = t28.*4.889512660317341;
t148 = t35.*4.889512660317341;
t149 = t36.*4.889512660317341;
t170 = t27.*yL3R.*(-4.889512660317341);
t171 = t28.*yL3R.*(-4.889512660317341);
t172 = t27.*yL4R.*(-4.889512660317341);
t173 = t28.*yL4R.*(-4.889512660317341);
t174 = t35.*yL3R.*(-4.889512660317341);
t175 = t36.*yL3R.*(-4.889512660317341);
t176 = t35.*yL4R.*(-4.889512660317341);
t177 = t36.*yL4R.*(-4.889512660317341);
t186 = t29.*yL3R.*(-1.8554995578159);
t187 = t30.*yL3R.*(-1.8554995578159);
t188 = t29.*yL5R.*(-1.8554995578159);
t189 = t30.*yL5R.*(-1.8554995578159);
t190 = t37.*yL3R.*(-1.8554995578159);
t191 = t38.*yL3R.*(-1.8554995578159);
t192 = t37.*yL5R.*(-1.8554995578159);
t193 = t38.*yL5R.*(-1.8554995578159);
t202 = t122.*5.498427854825518;
t203 = t123.*5.498427854825518;
t204 = t137.*1.107553096330212e+1;
t205 = t138.*1.107553096330212e+1;
t206 = t125.*5.498427854825518;
t207 = t126.*5.498427854825518;
t208 = t139.*1.107553096330212e+1;
t209 = t140.*1.107553096330212e+1;
t210 = t118.*2.26369831989984e+1;
t211 = t120.*2.26369831989984e+1;
t212 = t127.*2.579110623055469;
t213 = t121.*4.963868339591833;
t216 = t128.*2.579110623055469;
t217 = t124.*4.963868339591833;
t220 = t141.*5.386511817626898;
t225 = t116.*5.386511817626898;
t236 = t117.*4.536913203821156;
t241 = t133.*3.347079069531278;
t242 = t134.*3.347079069531278;
t243 = t119.*4.536913203821156;
t244 = t129.*4.787863941009193;
t245 = t130.*4.787863941009193;
t246 = t135.*3.347079069531278;
t247 = t136.*3.347079069531278;
t248 = t131.*4.787863941009193;
t249 = t132.*4.787863941009193;
t265 = t118.*uL4R.*(-2.26369831989984e+1);
t271 = t118.*yL3R.*(-2.26369831989984e+1);
t277 = t120.*uL4R.*(-2.26369831989984e+1);
t282 = t120.*yL3R.*(-2.26369831989984e+1);
t288 = t122.*yL3R.*(-5.498427854825518);
t289 = t123.*yL3R.*(-5.498427854825518);
t291 = t137.*yL5R.*(-1.107553096330212e+1);
t292 = t138.*yL5R.*(-1.107553096330212e+1);
t293 = t137.*yL6R.*(-1.107553096330212e+1);
t294 = t138.*yL6R.*(-1.107553096330212e+1);
t296 = t125.*yL3R.*(-5.498427854825518);
t297 = t126.*yL3R.*(-5.498427854825518);
t300 = t139.*yL5R.*(-1.107553096330212e+1);
t301 = t140.*yL5R.*(-1.107553096330212e+1);
t302 = t139.*yL6R.*(-1.107553096330212e+1);
t303 = t140.*yL6R.*(-1.107553096330212e+1);
t316 = t117.*uL5R.*(-4.536913203821156);
t317 = t117.*yL7R.*(-4.536913203821156);
t321 = t133.*yL5R.*(-3.347079069531278);
t322 = t134.*yL5R.*(-3.347079069531278);
t323 = t133.*yL8R.*(-3.347079069531278);
t324 = t134.*yL8R.*(-3.347079069531278);
t325 = t119.*uL5R.*(-4.536913203821156);
t326 = t119.*yL7R.*(-4.536913203821156);
t327 = t129.*yL6R.*(-4.787863941009193);
t328 = t130.*yL6R.*(-4.787863941009193);
t329 = t129.*yL7R.*(-4.787863941009193);
t330 = t130.*yL7R.*(-4.787863941009193);
t331 = t135.*yL5R.*(-3.347079069531278);
t332 = t136.*yL5R.*(-3.347079069531278);
t333 = t135.*yL8R.*(-3.347079069531278);
t334 = t136.*yL8R.*(-3.347079069531278);
t335 = t131.*yL6R.*(-4.787863941009193);
t336 = t132.*yL6R.*(-4.787863941009193);
t337 = t131.*yL7R.*(-4.787863941009193);
t338 = t132.*yL7R.*(-4.787863941009193);
t339 = t127.*uL6R.*(-2.579110623055469);
t340 = t121.*uL3R.*(-4.963868339591833);
t341 = t127.*yL8R.*(-2.579110623055469);
t342 = t128.*uL6R.*(-2.579110623055469);
t343 = t124.*uL3R.*(-4.963868339591833);
t344 = t128.*yL8R.*(-2.579110623055469);
t346 = t141.*uL3R.*(-5.386511817626898);
t347 = t141.*yL3R.*(-5.386511817626898);
t354 = t116.*uL3R.*(-5.386511817626898);
t355 = t116.*yL3R.*(-5.386511817626898);
t45 = -t41;
t46 = -t42;
t47 = -t43;
t48 = -t44;
t49 = t41.*yL1R;
t50 = t42.*yL2R;
t51 = t43.*yL1R;
t52 = t44.*yL2R;
t61 = -t57;
t62 = -t58;
t63 = -t59;
t64 = -t60;
t65 = t57.*yL4R;
t66 = t58.*yL4R;
t67 = t59.*yL4R;
t68 = t60.*yL4R;
t77 = -t71;
t78 = -t72;
t79 = t69.*yL4R;
t80 = t70.*yL4R;
t81 = t69.*yL5R;
t82 = t70.*yL5R;
t83 = t71.*yL4R;
t84 = t72.*yL4R;
t85 = t71.*yL5R;
t86 = t72.*yL5R;
t99 = t91.*yL5R;
t100 = t92.*yL5R;
t101 = t95.*yL5R;
t102 = t96.*yL5R;
t150 = t146.*yL3R;
t151 = t147.*yL3R;
t152 = t146.*yL4R;
t153 = t147.*yL4R;
t154 = t148.*yL3R;
t155 = t149.*yL3R;
t156 = t148.*yL4R;
t157 = t149.*yL4R;
t158 = -t144;
t159 = -t145;
t160 = -t148;
t161 = -t149;
t162 = t142.*yL3R;
t163 = t143.*yL3R;
t164 = t142.*yL5R;
t165 = t143.*yL5R;
t166 = t144.*yL3R;
t167 = t145.*yL3R;
t168 = t144.*yL5R;
t169 = t145.*yL5R;
t194 = t186.*yL5R;
t195 = t187.*yL5R;
t196 = t190.*yL5R;
t197 = t191.*yL5R;
t198 = t170.*yL4R;
t199 = t171.*yL4R;
t200 = t174.*yL4R;
t201 = t175.*yL4R;
t214 = -t202;
t215 = -t203;
t218 = -t206;
t219 = -t207;
t221 = t210.*uL4R;
t222 = -t208;
t223 = -t209;
t224 = t210.*yL3R;
t226 = t211.*uL4R;
t227 = t211.*yL3R;
t228 = t202.*yL3R;
t229 = t203.*yL3R;
t230 = t204.*yL5R;
t231 = t205.*yL5R;
t232 = t204.*yL6R;
t233 = t205.*yL6R;
t234 = t206.*yL3R;
t235 = t207.*yL3R;
t237 = t208.*yL5R;
t238 = t209.*yL5R;
t239 = t208.*yL6R;
t240 = t209.*yL6R;
t250 = -t211;
t251 = -t213;
t254 = -t216;
t255 = -t217;
t256 = t236.*uL5R;
t257 = t236.*yL7R;
t260 = t241.*yL5R;
t261 = t242.*yL5R;
t262 = t241.*yL8R;
t263 = t242.*yL8R;
t264 = t243.*uL5R;
t266 = t243.*yL7R;
t267 = t244.*yL6R;
t268 = t245.*yL6R;
t269 = t244.*yL7R;
t270 = t245.*yL7R;
t272 = -t225;
t273 = t246.*yL5R;
t274 = t247.*yL5R;
t275 = t246.*yL8R;
t276 = t247.*yL8R;
t278 = t248.*yL6R;
t279 = t249.*yL6R;
t280 = t248.*yL7R;
t281 = t249.*yL7R;
t283 = t212.*uL6R;
t284 = t213.*uL3R;
t285 = t212.*yL8R;
t286 = t216.*uL6R;
t287 = t217.*uL3R;
t290 = t216.*yL8R;
t295 = t220.*uL3R;
t298 = t220.*yL3R;
t304 = t225.*uL3R;
t305 = t225.*yL3R;
t307 = -t243;
t308 = -t246;
t309 = -t247;
t310 = -t248;
t311 = -t249;
t313 = t291.*yL6R;
t314 = t292.*yL6R;
t318 = t300.*yL6R;
t319 = t301.*yL6R;
t351 = t265.*yL3R;
t358 = t277.*yL3R;
t362 = t342.*yL8R;
t363 = t346.*yL3R;
t364 = t354.*yL3R;
t365 = t316.*yL7R;
t366 = t321.*yL8R;
t367 = t322.*yL8R;
t368 = t325.*yL7R;
t369 = t327.*yL7R;
t370 = t328.*yL7R;
t371 = t331.*yL8R;
t372 = t332.*yL8R;
t373 = t335.*yL7R;
t374 = t336.*yL7R;
t375 = t339.*yL8R;
t87 = t79.*yL5R;
t88 = t80.*yL5R;
t89 = t83.*yL5R;
t90 = t84.*yL5R;
t178 = t162.*yL5R;
t179 = t163.*yL5R;
t180 = t166.*yL5R;
t181 = t167.*yL5R;
t182 = t150.*yL4R;
t183 = t151.*yL4R;
t184 = t154.*yL4R;
t185 = t155.*yL4R;
t252 = t230.*yL6R;
t253 = t231.*yL6R;
t258 = t237.*yL6R;
t259 = t238.*yL6R;
t299 = t221.*yL3R;
t306 = t226.*yL3R;
t312 = t286.*yL8R;
t315 = t295.*yL3R;
t320 = t304.*yL3R;
t345 = t256.*yL7R;
t348 = t260.*yL8R;
t349 = t261.*yL8R;
t350 = t264.*yL7R;
t352 = t267.*yL7R;
t353 = t268.*yL7R;
t356 = t273.*yL8R;
t357 = t274.*yL8R;
t359 = t278.*yL7R;
t360 = t279.*yL7R;
t361 = t283.*yL8R;
t376 = t57+t82+t150;
t377 = t59+t86+t154;
t378 = t230+t270;
t379 = t237+t281;
t380 = t260+t283;
t381 = t273+t286;
t382 = t256+t267;
t383 = t264+t278;
t384 = t83+t166+t240+t276;
t385 = t79+t162+t233+t263;
t386 = t153+t165+t202+t221+t295;
t387 = t157+t169+t206+t226+t304;
mt1 = [t51;t45;t55;t45;t41;t207;t219;t55;t41;t207;t51+t289+t340;t229;t217;t284;t219;t229;t289;t217;t255;t284;t255;t340;t52;t46;t56;t46;t42;t60;t64;t56;t42;t60;t52+t74;t66;t64;t66;t74;2.102597904407234e+1;t147;t143;t218;t387;t177;t193;t220;t210;t354;t277;t147;t155;t175;t143;t167;t191;t218;t288;t228;t387;t155;t167;t228;t195+t199+t288+t351+t363;t183;t179;t305;t227;t315;t299;t177;t175;t183;t199;t193;t191;t179;t195;t220;t305;t355;t210;t227;t282;t354;t315;t355;t363;t277;t299;t282;t351;t146;t176;t156;t146;t70;t63;t174;t377;t98;t70;t84;t96;t63;t73;t65;t176;t174;t198;t182;t156;t377;t84;t65;t182;t73+t100+t198;t88;t98;t96;t88;t100;t142;t192;t168;t69];
mt2 = [t97;t85;t142;t69;1.065211007893471e+1;t205;t242;t190;t95;t384;t303;t334;t205;t238;t301;t242;t274;t332;t192;t190;t194;t178;t97;t95;t99;t87;t168;t85;t384;t238;t274;t178;t87;t99+t194+t314+t367;t253;t349;t303;t301;t253;t314;t334;t332;t349;t367;t204;t302;t239;t204;1.156586861229566e+1;t245;t300;t379;t338;t245;t279;t336;t302;t300;t313;t252;t239;t379;t279;t252;t313+t370;t353;t338;t336;t353];
mt3 = [t370;t244;t337;t280;t244;7.671826633755321;t335;t383;t236;t325;t337;t335;t369;t352;t280;t383;t352;t365+t369;t266;t345;t236;t266;t326;t325;t345;t326;t365;t241;t333;t275;t241;5.121999289652515;t331;t381;t212;t342;t333;t331;t366;t348;t275;t381;t348;t366+t375;t290;t361;t212;t290;t344;t342;t361;t344;t375;t49;t43;t53;t43;t47;t203;t215;t53;t47;t203;t49+t235+t287;t297;t213];
mt4 = [t343;t215;t297;t235;t213;t251;t343;t251;t287;t50;t44;t54;t44;t48;t58;t62;t54;t48;t58;t50+t68;t76;t62;t76;t68;7.730834241521559e+1;t161;t159;t214;t386;t173;t189;t272;t250;t346;t265;t161;t151;t171;t159;t163;t187;t214;t234;t296;t386;t151;t163;t296;t181+t185+t234+t306+t320;t201;t197;t298;t224;t364;t358;t173;t171;t201;t185;t189;t187;t197;t181;t272;t298;t347;t250;t224;t271;t346;t364;t347;t320;t265;t358;t271;t306;t160;t172;t152;t160;3.909801189652931e+1;t78;t61;t170;t376;t94;t78;t80;t92;t61;t67;t75;t172;t170;t184;t200;t152;t376;t80];
mt5 = [t75;t200;t67+t90+t184;t102;t94;t92;t102;t90;t158;t188;t164;t77;t93;t81;t158;t77;4.818501275053576e+1;t223;t309;t186;t91;t385;t294;t324;t223;t231;t292;t309;t261;t322;t188;t186;t180;t196;t93;t91;t89;t101;t164;t81;t385;t231;t261;t196;t101;t89+t180+t259+t357;t319;t372;t294;t292;t319;t259;t324;t322;t372;t357;t222;t293;t232;t222;2.953667575304287e+1;t311;t291;t378;t330;t311;t268;t328;t293];
mt6 = [t291;t258;t318;t232;t378;t268;t318;t258+t360;t374;t330;t328;t374;t360;t310;t329;t269;t310;1.699403618740193e+1;t327;t382;t307;t316;t329;t327;t359;t373;t269;t382;t373;t350+t359;t257;t368;t307;t257;t317;t316;t368;t317;t350;t308;t323;t262;t308;1.068802786407191e+1;t321;t380;t254;t339;t323;t321;t356;t371;t262;t380;t371;t312+t356;t285;t362;t254;t285;t341;t339;t362;t341;t312];
outCon = [mt1;mt2;mt3;mt4;mt5;mt6];

end
