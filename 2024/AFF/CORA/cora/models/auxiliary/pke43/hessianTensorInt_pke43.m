function Hf=hessianTensorInt_pke43(x,u)



 Hf{1} = interval(sparse(4,4),sparse(4,4));

Hf{1}(3,1) = -1/1000;
Hf{1}(4,1) = 1;
Hf{1}(1,3) = -1/1000;
Hf{1}(1,4) = 1;


 Hf{2} = interval(sparse(4,4),sparse(4,4));



 Hf{3} = interval(sparse(4,4),sparse(4,4));


end