function [Tf,ind] = thirdOrderTensorInt_prodDes(x,u)



 Tf{1,1} = interval(sparse(4,4),sparse(4,4));

Tf{1,1}(1,1) = (6*x(1)*x(2))/(x(1) + 1)^4 - (6*x(2))/(x(1) + 1)^3;
Tf{1,1}(2,1) = 2/(x(1) + 1)^2 - (2*x(1))/(x(1) + 1)^3;
Tf{1,1}(1,2) = 2/(x(1) + 1)^2 - (2*x(1))/(x(1) + 1)^3;


 Tf{1,2} = interval(sparse(4,4),sparse(4,4));

Tf{1,2}(1,1) = 2/(x(1) + 1)^2 - (2*x(1))/(x(1) + 1)^3;


 Tf{1,3} = interval(sparse(4,4),sparse(4,4));



 Tf{1,4} = interval(sparse(4,4),sparse(4,4));



 Tf{2,1} = interval(sparse(4,4),sparse(4,4));

Tf{2,1}(1,1) = (6*x(2))/(x(1) + 1)^3 - (6*x(1)*x(2))/(x(1) + 1)^4;
Tf{2,1}(2,1) = (2*x(1))/(x(1) + 1)^3 - 2/(x(1) + 1)^2;
Tf{2,1}(1,2) = (2*x(1))/(x(1) + 1)^3 - 2/(x(1) + 1)^2;


 Tf{2,2} = interval(sparse(4,4),sparse(4,4));

Tf{2,2}(1,1) = (2*x(1))/(x(1) + 1)^3 - 2/(x(1) + 1)^2;


 Tf{2,3} = interval(sparse(4,4),sparse(4,4));



 Tf{2,4} = interval(sparse(4,4),sparse(4,4));



 Tf{3,1} = interval(sparse(4,4),sparse(4,4));



 Tf{3,2} = interval(sparse(4,4),sparse(4,4));



 Tf{3,3} = interval(sparse(4,4),sparse(4,4));



 Tf{3,4} = interval(sparse(4,4),sparse(4,4));



 Tg{1,1} = interval(sparse(4,4),sparse(4,4));



 Tg{1,2} = interval(sparse(4,4),sparse(4,4));



 Tg{1,3} = interval(sparse(4,4),sparse(4,4));



 Tg{1,4} = interval(sparse(4,4),sparse(4,4));



 Tg{2,1} = interval(sparse(4,4),sparse(4,4));



 Tg{2,2} = interval(sparse(4,4),sparse(4,4));



 Tg{2,3} = interval(sparse(4,4),sparse(4,4));



 Tg{2,4} = interval(sparse(4,4),sparse(4,4));



 Tg{3,1} = interval(sparse(4,4),sparse(4,4));



 Tg{3,2} = interval(sparse(4,4),sparse(4,4));



 Tg{3,3} = interval(sparse(4,4),sparse(4,4));



 Tg{3,4} = interval(sparse(4,4),sparse(4,4));


 ind = cell(3,1);
 ind{1} = [1;2];


 ind{2} = [1;2];


 ind{3} = [];

end

