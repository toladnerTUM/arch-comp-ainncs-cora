function Hf=hessianTensorInt_laubLoomis2_ARCH23(x,u)



 Hf{1} = interval(sparse(8,8),sparse(8,8));



 Hf{2} = interval(sparse(8,8),sparse(8,8));



 Hf{3} = interval(sparse(8,8),sparse(8,8));

Hf{3}(3,2) = -4/5;
Hf{3}(2,3) = -4/5;


 Hf{4} = interval(sparse(8,8),sparse(8,8));

Hf{4}(4,3) = -13/10;
Hf{4}(3,4) = -13/10;


 Hf{5} = interval(sparse(8,8),sparse(8,8));

Hf{5}(5,4) = -1;
Hf{5}(4,5) = -1;


 Hf{6} = interval(sparse(8,8),sparse(8,8));



 Hf{7} = interval(sparse(8,8),sparse(8,8));

Hf{7}(7,2) = -3/2;
Hf{7}(2,7) = -3/2;

end