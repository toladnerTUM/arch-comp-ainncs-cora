function Hf=hessianTensorInt_trailer_simple(x,u)



 Hf{1} = interval(sparse(5,5),sparse(5,5));

Hf{1}(3,3) = -5*cos(x(3));


 Hf{2} = interval(sparse(5,5),sparse(5,5));

Hf{2}(3,3) = -5*sin(x(3));


 Hf{3} = interval(sparse(5,5),sparse(5,5));

Hf{3}(4,4) = 10*tan(x(4))*(tan(x(4))^2 + 1);


 Hf{4} = interval(sparse(5,5),sparse(5,5));

Hf{4}(2,2) = -6*x(2);

end