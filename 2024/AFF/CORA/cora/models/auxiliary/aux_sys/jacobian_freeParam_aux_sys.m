function [A,B]=jacobian_freeParam_aux_sys(x,u,p)

A=[-p(1)*p(3),p(1)*p(3);...
p(1)*p(4),- p(1)*p(4) - p(2)*p(4)];

B=[p(3);...
0];

