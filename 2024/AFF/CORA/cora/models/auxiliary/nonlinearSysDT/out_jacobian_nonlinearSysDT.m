function [A,B]=out_jacobian_nonlinearSysDT(x,u)

A=[1,1,0,0;...
0,-1/2,1/2,0];

B=[0,0,1,0,0,0,0,1,0;...
0,0,0,0,0,0,0,0,1];

