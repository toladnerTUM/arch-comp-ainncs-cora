function Hf=hessianTensor_infiniteBus_polynomialized(x,u)



 Hf{1} = sparse(4,4);

Hf{1}(3,2) = -1;
Hf{1}(2,3) = -1;


 Hf{2} = sparse(4,4);

Hf{2}(3,1) = 1;
Hf{2}(1,3) = 1;


 Hf{3} = sparse(4,4);


end