function Hf=hessianTensorInt_modular_system_odes(x,u)



 Hf{1} = interval(sparse(9,9),sparse(9,9));

Hf{1}(3,1) = -10001/5000;
Hf{1}(4,1) = 1/1000;
Hf{1}(1,3) = -10001/5000;
Hf{1}(1,4) = 1/1000;


 Hf{2} = interval(sparse(9,9),sparse(9,9));



 Hf{3} = interval(sparse(9,9),sparse(9,9));

Hf{3}(5,3) = -2/2215;
Hf{3}(3,5) = -2/2215;
Hf{3}(6,5) = 2/2215;
Hf{3}(5,6) = 2/2215;


 Hf{4} = interval(sparse(9,9),sparse(9,9));



 Hf{5} = interval(sparse(9,9),sparse(9,9));



 Hf{6} = interval(sparse(9,9),sparse(9,9));

Hf{6}(5,3) = 2/2215;
Hf{6}(3,5) = 2/2215;
Hf{6}(6,5) = -2/2215;
Hf{6}(5,6) = -2/2215;


 Hf{7} = interval(sparse(9,9),sparse(9,9));

Hf{7}(8,7) = -5/167607;
Hf{7}(7,8) = -5/167607;


 Hf{8} = interval(sparse(9,9),sparse(9,9));


end