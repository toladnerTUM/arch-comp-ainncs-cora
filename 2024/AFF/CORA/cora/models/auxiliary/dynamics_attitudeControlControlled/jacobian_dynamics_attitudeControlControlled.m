function [A,B]=jacobian_dynamics_attitudeControlControlled(x,u)

A=[0,x(3)/4,x(2)/4,0,0,0,1/4,0,0;...
-(3*x(3))/2,0,-(3*x(1))/2,0,0,0,0,1/2,0;...
2*x(2),2*x(1),0,0,0,0,0,0,1;...
x(4)^2/2 + x(5)^2/2 + x(6)^2/2 + 1/2,x(4)^2/2 - x(6)/2 + x(5)^2/2 + x(6)^2/2,x(5)/2 + x(4)^2/2 + x(5)^2/2 + x(6)^2/2,x(1)*x(4) + x(2)*x(4) + x(3)*x(4),(x(3)*(2*x(5) + 1))/2 + x(1)*x(5) + x(2)*x(5),(x(2)*(2*x(6) - 1))/2 + x(1)*x(6) + x(3)*x(6),0,0,0;...
x(6)/2 + x(4)^2/2 + x(5)^2/2 + x(6)^2/2,x(4)^2/2 + x(5)^2/2 + x(6)^2/2 + 1/2,x(4)^2/2 - x(4)/2 + x(5)^2/2 + x(6)^2/2,(x(3)*(2*x(4) - 1))/2 + x(1)*x(4) + x(2)*x(4),x(1)*x(5) + x(2)*x(5) + x(3)*x(5),(x(1)*(2*x(6) + 1))/2 + x(2)*x(6) + x(3)*x(6),0,0,0;...
x(4)^2/2 - x(5)/2 + x(5)^2/2 + x(6)^2/2,x(4)/2 + x(4)^2/2 + x(5)^2/2 + x(6)^2/2,x(4)^2/2 + x(5)^2/2 + x(6)^2/2 + 1/2,(x(2)*(2*x(4) + 1))/2 + x(1)*x(4) + x(3)*x(4),(x(1)*(2*x(5) - 1))/2 + x(2)*x(5) + x(3)*x(5),x(1)*x(6) + x(2)*x(6) + x(3)*x(6),0,0,0;...
0,0,0,0,0,0,0,0,0;...
0,0,0,0,0,0,0,0,0;...
0,0,0,0,0,0,0,0,0];

B=[0;...
0;...
0;...
0;...
0;...
0;...
0;...
0;...
0];

