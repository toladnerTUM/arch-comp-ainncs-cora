function HA = test_hybrid_parallel_twocomp1(~)


%% Generated on 22-May-2023 18:19:02

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont1_1):
%  state x := [x1; x2]
%  input u := [y2; y3]
%  output y := [y1]

% Component 2 (system.cont2_1):
%  state x := [x3; x4; x5]
%  input u := [y1]
%  output y := [y2; y3]

%-----------------------Component system.cont1_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x1' = -x2 + u1 &&
%   x2' = x1 + u2
dynA = ...
[0,-1;1,0];
dynB = ...
[1,0;0,1];
dync = ...
[0;0];

%% output equation:
%   y1 == 0.05*(x1+x2)
dynC = ...
[0.050000000000000002775557561562891,0.050000000000000002775557561562891];
dynD = ...
[0,0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x1 + x2 <= 0 &&
%   y1 == 0.05*(x1+x2)
A = ...
[1,1];
b = ...
[0];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1' := x1 + 3 && x2' := x2 + 3
resetA = ...
[1,0;0,1];
resetc = ...
[3;3];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 + x2 >= 0
c = [0.5;0.5];
d = 0;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2);

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x1' = -x2 + u2 &&
%   x2' = -x1 + u1
dynA = ...
[0,-1;-1,0];
dynB = ...
[0,1;1,0];
dync = ...
[0;0];

%% output equation:
%   y1 == 0.05*(x1-x2)
dynC = ...
[0.050000000000000002775557561562891,-0.050000000000000002775557561562891];
dynD = ...
[0,0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x1 + x2 >= 0 &&
%   y1 == 0.05*(x1-x2)
A = ...
[-1,-1];
b = ...
[0];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1' := x1 - 3  && x2' := x2 + 3
resetA = ...
[1,0;0,1];
resetc = ...
[-3;3];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 + x2 <= 0
c = [-0.5;-0.5];
d = 0;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 1);

loc(2) = location('loc2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% input names: y2, y3
iBinds{1} = [[2,1];[2,2]];

%-----------------------Component system.cont2_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x3' = x4-x5 && x4' = x3 - u3 && x5' = x4
dynA = ...
[0,1,-1;1,0,0;0,1,0];
dynB = ...
[0;-1;0];
dync = ...
[0;0;0];

%% output equation:
%   y2 == 0.05*x5 && y3 == 0.05*(x3 + x4)
dynC = ...
[0,0,0.050000000000000002775557561562891;...
0.050000000000000002775557561562891,0.050000000000000002775557561562891,0];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x3 + x4 + x5 <= 1 &&
%   y2 == 0.05*x5 &&
%   y3 == 0.05*(x3 + x4)
A = ...
[1,1,1];
b = ...
[1];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x3' := x3 + 1 && x4' := x4 + 1 && x5' := x5 + 1
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[1;1;1];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x3 + x4 + x5 > 1
c = [0.33333333333333331482961625624739;0.33333333333333331482961625624739;...
0.33333333333333331482961625624739];
d = 0.33333;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2);

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x3' = -x4+x5 && x4' = x3 && x5' = x4 + u3
dynA = ...
[0,-1,1;1,0,0;0,1,0];
dynB = ...
[0;0;1];
dync = ...
[0;0;0];

%% output equation:
%   y2 == 0.05*x3 && y3 == 0.05*(x4 - x5)
dynC = ...
[0.050000000000000002775557561562891,0,0;0,...
0.050000000000000002775557561562891,-0.050000000000000002775557561562891];
dynD = ...
[0;0];
dynk = ...
[0;0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x3 + x4 + x5 >= 1 &&
%   y2 == 0.05*x3 &&
%   y3 == 0.05*(x4 - x5)
A = ...
[-1,-1,-1];
b = ...
[-1];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x3' := x3 - 1 && x4' := x4 - 1 && x5' := x5 - 1
resetA = ...
[1,0,0;0,1,0;0,0,1];
resetc = ...
[-1;-1;-1];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x3 + x4 + x5 < 1
c = [-0.33333333333333331482961625624739;...
-0.33333333333333331482961625624739;-0.33333333333333331482961625624739];
d = -0.33333;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 1);

loc(2) = location('loc2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% input names: y1
iBinds{2} = [[1,1]];

HA = parallelHybridAutomaton(comp,iBinds);

end