function [dx]=vanDerPolSys_St1_FlowEq(x,u)

dx(1,1) = x(2);

dx(2,1) = u(1) - x(1) - x(2)*(x(1)^2 - 1);
