function HA = test_hybrid_flat_twoloc4(~)


%% Generated on 27-May-2024 13:30:40

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont_1):
%  state x := [x1; x2]
%  input u := [u1]

%------------------------Component system.cont_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x1' = -2*sin(x1) + u1 &&
%   x2' = x1 - x2
dynamics = nonlinearSys(@test_hybrid_flat_twoloc4_Loc1_FlowEq,2,1); 
%% invariant equation:
%   
inv = emptySet(2);

trans = transition();
%% reset equation:
%   x1' := x1 - 2 && x2' := x2 - 2
resetA = ...
[1,0;0,1];
resetc = ...
[-2;-2];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1^2 + x2^2 <= 5
vars = sym('x',[2,1]);
syms x1 x2;
eq = x1^2 + x2^2 - 5;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 2);

%% reset equation:
%   x1' := x1 + 2 && x2' := x2 + 2
resetA = ...
[1,0;0,1];
resetc = ...
[2;2];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1^2 + x2^2 >= 5
vars = sym('x',[2,1]);
syms x1 x2;
eq = 5 - x2^2 - x1^2;
compOp = '<=';

guard = levelSet(eq,vars,compOp);

trans(2) = transition(guard, reset, 2);

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x1' = -2*cos(x1) + u1 &&
%   x2' = x1 + x2
dynamics = nonlinearSys(@test_hybrid_flat_twoloc4_Loc2_FlowEq,2,1); 
%% invariant equation:
%   
inv = fullspace(2);

trans = transition();
loc(2) = location('loc2', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end