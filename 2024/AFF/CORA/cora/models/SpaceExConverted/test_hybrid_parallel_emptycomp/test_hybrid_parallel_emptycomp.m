function HA = test_hybrid_parallel_emptycomp(~)


%% Generated on 27-May-2024 13:30:45

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.comp1_1):
%  state x := [x1; x2]
%  input u := [uDummy]

% Component 2 (system.comp2_1):
%  state x := [];
%  input u := [uDummy]

%-----------------------Component system.comp1_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x1' = 0 && x2' = 1
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x2 <= 0
P_A = ...
[0,1];
P_b = ...
[-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := x1 + 1 && x2' := x2 + 5
resetA = ...
[1,0;0,1];
resetc = ...
[1;5];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 >= 0
c = [0;-1];
d = 0;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2, 'switch12');

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x1' = 0 && x2' = -1
dynA = ...
[0,0;0,0];
dynB = ...
[0;0];
dync = ...
[0;-1];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   x2 >= 0
P_A = ...
[0,-1];
P_b = ...
[-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := x1 + 1 && x2' := x2 - 5
resetA = ...
[1,0;0,1];
resetc = ...
[1;-5];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x2 <= 0
c = [0;-1];
d = 0;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 1, 'switch21');

loc(2) = location('loc2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% only dummy input
iBinds{1} = [0 1];

%-----------------------Component system.comp2_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   
dynA = ...
zeros([0,0]);
dynB = ...
zeros([0,1]);
dync = ...
zeros([0,1]);
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = fullspace(0);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
zeros([0,0]);
resetc = ...
zeros([0,1]);
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(0);

trans(1) = transition(guard, reset, 2, 'switch12');

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   
dynA = ...
zeros([0,0]);
dynB = ...
zeros([0,1]);
dync = ...
zeros([0,1]);
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   
inv = fullspace(0);

trans = transition();
%% reset equation:
%   no reset equation given
resetA = ...
zeros([0,0]);
resetc = ...
zeros([0,1]);
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   no guard set given -> instant transition
guard = fullspace(0);

trans(1) = transition(guard, reset, 1, 'switch21');

loc(2) = location('loc2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% only dummy input
iBinds{2} = [0 1];

HA = parallelHybridAutomaton(comp,iBinds);

end