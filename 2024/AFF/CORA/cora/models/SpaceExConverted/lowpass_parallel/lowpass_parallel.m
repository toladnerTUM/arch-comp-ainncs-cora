function HA = lowpass_parallel(~)


%% Generated on 24-Jul-2023 23:55:51

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.filter1):
%  state x := [x1; x2]
%  input u := [u1]
%  output y := [y1]

% Component 2 (system.filter2):
%  state x := [x3; x4]
%  input u := [y1]

%-----------------------Component system.filter1---------------------------

clear loc

%---------------------------Location loc1_11-------------------------------

%% flow equation:
%   x1' = -10.21587*x1 + 20.13684457 & x2' = -200.2418*x2 + u1_1 - 400.4898478
dynA = ...
[-10.215870000000000672457645123359,0;0,-200.24180000000001200533006340265];
dynB = ...
[0;1];
dync = ...
[20.136844570000000942400220083073;-400.48984780000000682775862514973];

%% output equation:
%   x1 > -1.097840 & x1 < 1.097840 & x2 > -2.698928 & x2 < 2.698928 & y1 == x1
dynC = ...
[1,0];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x1 > -1.097840 & x1 < 1.097840 & x2 > -2.698928 & x2 < 2.698928 & y1 == x1
A = ...
[-1,0;1,0;0,-1;0,1];
b = ...
[1.0978399999999999270272610374377;1.0978399999999999270272610374377;...
2.6989279999999999937188022158807;2.6989279999999999937188022158807];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1 := x1 + 1.071580 & x2 := x2 - 1.301798
resetA = ...
[1,0;0,1];
resetc = ...
[1.0715799999999999769784153613728;-1.3017980000000000107007736005471];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 > -1.107840 & x1 < -1.087840 & x2 > -2.698928 & x2 < 2.698928
A = ...
[-1,0;0,-1;0,1;1,0];
b = ...
[1.0978399999999999270272610374377;2.6989279999999999937188022158807;...
2.6989279999999999937188022158807;-1.0878399999999999181454768404365];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 2);

%% reset equation:
%   x1 := x1 - 1.071580 & x2 := x2 + 1.301798
resetA = ...
[1,0;0,1];
resetc = ...
[-1.0715799999999999769784153613728;1.3017980000000000107007736005471];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 > 1.087840 & x1 < 1.107840 & x2 > -2.698928 & x2 < 2.698928
A = ...
[1,0;0,-1;0,1;-1,0];
b = ...
[1.0978399999999999270272610374377;2.6989279999999999937188022158807;...
2.6989279999999999937188022158807;-1.0878399999999999181454768404365];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 3);

loc(1) = location('loc1_11', inv, trans, dynamics);



%---------------------------Location loc1_21-------------------------------

%% flow equation:
%   x1' = -14.58328*x1 + 33.95001456 & x2' = -205.3664*x2 + u1_1 - 632.6889324
dynA = ...
[-14.58328000000000024272139853565,0;0,-205.3663999999999987267074175179];
dynB = ...
[0;1];
dync = ...
[33.950014559999999619321897625923;-632.68893239999999877909431234002];

%% output equation:
%   x1 > -1.629461 & x1 < 0.3718705 & x2 > -2.653889 & x2 < 2.705073 & y1 == x2
dynC = ...
[0,1];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x1 > -1.629461 & x1 < 0.3718705 & x2 > -2.653889 & x2 < 2.705073 & y1 == x2
A = ...
[-1,0;1,0;0,-1;0,1];
b = ...
[1.629461000000000048260062612826;0.37187049999999999272404238581657;...
2.6538889999999999425028818222927;2.7050730000000000607940364716342];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1 := x1 & x2 := x2
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 > 0.3618705 & x1 < 0.3818705 & x2 > -2.653899 & x2 < 2.705073
A = ...
[1,0;0,-1;0,1;-1,0];
b = ...
[0.37187049999999999272404238581657;2.6538889999999999425028818222927;...
2.7050730000000000607940364716342;-0.36187049999999998384225818881532];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 1);

loc(2) = location('loc1_21', inv, trans, dynamics);



%---------------------------Location loc1_31-------------------------------

%% flow equation:
%   x1' = -14.58328*x1 + 7.87473456 & x2' = -205.3664*x2 + u1_1 - 146.7527324
dynA = ...
[-14.58328000000000024272139853565,0;0,-205.3663999999999987267074175179];
dynB = ...
[0;1];
dync = ...
[7.8747345600000002718843461479992;-146.75273240000001351290848106146];

%% output equation:
%   x1 > -0.3718705 & x1 < 1.629461 & x2 > -2.705073 & x2 < 2.653899 & y1 == x2
dynC = ...
[0,1];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x1 > -0.3718705 & x1 < 1.629461 & x2 > -2.705073 & x2 < 2.653899 & y1 == x2
A = ...
[-1,0;1,0;0,-1;0,1];
b = ...
[0.37187049999999999272404238581657;1.629461000000000048260062612826;...
2.7050730000000000607940364716342;2.6538990000000000080149220593739];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1 := x1 & x2 := x2
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 > -0.3818705 & x1 < -0.3618705 & x2 > -2.705073 & x2 < 2.653899
A = ...
[-1,0;0,-1;0,1;1,0];
b = ...
[0.37187049999999999272404238581657;2.7050730000000000607940364716342;...
2.6538990000000000080149220593739;-0.36187049999999998384225818881532];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 1);

loc(3) = location('loc1_31', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% input names: u1
iBinds{1} = [[0,1]];

%-----------------------Component system.filter2---------------------------

clear loc

%---------------------------Location loc2_11-------------------------------

%% flow equation:
%   x3' = -12.65885*x3 - 2.92283193 & x4' = -47.25145*x4 + u2_1 + 8.0628348
dynA = ...
[-12.658849999999999269562067638617,0;0,-47.251449999999998397015588125214];
dynB = ...
[0;1];
dync = ...
[-2.9228319300000000779959918872919;8.0628347999999991913000485510565];

%% output equation:
%   x3 > -1.350712 & x3 < 0.9670362 & x4 > -2.504938 & x4 < 1.562432 & y2 == x4
dynC = ...
[0,1];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x3 > -1.350712 & x3 < 0.9670362 & x4 > -2.504938 & x4 < 1.562432 & y2 == x4
A = ...
[-1,0;1,0;0,-1;0,1];
b = ...
[1.3507119999999999127027194845141;0.96703620000000001244444547410239;...
2.504938000000000108968833956169;1.5624320000000000430162572229165];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x3 := x3 & x4 := x4
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x3 > 0.9570362 & x3 < 0.9770362 & x4 > -2.504938 & x4 < 1.562432
A = ...
[1,0;0,-1;0,1;-1,0];
b = ...
[0.96703620000000001244444547410239;2.504938000000000108968833956169;...
1.5624320000000000430162572229165;-0.95703620000000000356266127710114];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 3);

loc(1) = location('loc2_11', inv, trans, dynamics);



%---------------------------Location loc2_21-------------------------------

%% flow equation:
%   x3' = -12.65885*x3 - 43.04013293 & x4' = -47.25145*x4 +u2_1  + 118.7291948
dynA = ...
[-12.658849999999999269562067638617,0;0,-47.251449999999998397015588125214];
dynB = ...
[0;1];
dync = ...
[-43.040132929999998623316059820354;118.72919480000000191921571968123];

%% output equation:
%   x3 > -0.9670362 & x3 < 1.350712 & x4 > 1.562432 & x4 < 2.504938 & y2 == x4
dynC = ...
[0,1];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x3 > -0.9670362 & x3 < 1.350712 & x4 > 1.562432 & x4 < 2.504938 & y2 == x4
A = ...
[-1,0;1,0;0,-1;0,1];
b = ...
[0.96703620000000001244444547410239;1.3507119999999999127027194845141;...
-1.5624320000000000430162572229165;2.504938000000000108968833956169];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x3 := x3 & x4 := x4
resetA = ...
[1,0;0,1];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x3 > -0.9770362 & x3 < -0.9570362 & x4 > -1.562432 & x4 < 2.504938
A = ...
[-1,0;0,-1;0,1;1,0];
b = ...
[0.96703620000000001244444547410239;-1.5624320000000000430162572229165;...
2.504938000000000108968833956169;-0.95703620000000000356266127710114];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 3);

loc(2) = location('loc2_21', inv, trans, dynamics);



%---------------------------Location loc2_31-------------------------------

%% flow equation:
%   x3' = -10.07836*x3 - 26.0284288 & x4' = -40.16005*x4 + u2_1 + 79.7753009
dynA = ...
[-10.07835999999999998522071109619,0;0,-40.160049999999998249222699087113];
dynB = ...
[0;1];
dync = ...
[-26.028428800000000364889274351299;79.77530090000000484451447846368];

%% output equation:
%   x3 > -2.251641 & x3 < 2.251641 & x4 > -2.580417 & x4 < 2.580417 & y2 == x4
dynC = ...
[0,1];
dynD = ...
[0];
dynk = ...
[0];
dynamics = linearSys(dynA, dynB, dync, dynC, dynD, dynk);

%% invariant equation:
%   x3 > -2.251641 & x3 < 2.251641 & x4 > -2.580417 & x4 < 2.580417 & y2 == x4
A = ...
[-1,0;1,0;0,-1;0,1];
b = ...
[2.2516409999999997815223196084844;2.2516409999999997815223196084844;...
2.5804170000000001827800133469282;2.5804170000000001827800133469282];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x3 := x3 + 2.089598 & x4 := x4 - 1.797423
resetA = ...
[1,0;0,1];
resetc = ...
[2.089598000000000066478378357715;-1.7974229999999999929372052065446];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x3 > -2.261641 & x3 < -2.241641 & x4 > -2.580417 & x4 < 2.580417
A = ...
[-1,0;0,-1;0,1;1,0];
b = ...
[2.2516409999999997815223196084844;2.5804170000000001827800133469282;...
2.5804170000000001827800133469282;-2.2416409999999999946851403365145];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(1) = transition(guard, reset, 1);

%% reset equation:
%   x3 := x3 - 2.089598 & x4 := x4 + 1.797423
resetA = ...
[1,0;0,1];
resetc = ...
[-2.089598000000000066478378357715;1.7974229999999999929372052065446];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x3 > 2.241641 & x3 < 2.261641 & x4 > -2.580417 & x4 < 2.580417
A = ...
[1,0;0,-1;0,1;-1,0];
b = ...
[2.2516409999999997815223196084844;2.5804170000000001827800133469282;...
2.5804170000000001827800133469282;-2.2416409999999999946851403365145];
polyOpt = struct('A', A, 'b', b);
guard = mptPolytope(polyOpt);

trans(2) = transition(guard, reset, 2);

loc(3) = location('loc2_31', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% input names: y1
iBinds{2} = [[1,1]];

HA = parallelHybridAutomaton(comp,iBinds);

end