function HA = test_hybrid_parallel_twocomp2(~)


%% Generated on 22-May-2023 18:19:08

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont1_1):
%  state x := [x1; x2]
%  input u := [y2; y3]
%  output y := [y1]

% Component 2 (system.cont2_1):
%  state x := [x3; x4; x5]
%  input u := [y1]
%  output y := [y2; y3]

%-----------------------Component system.cont1_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x1' = -sin(x2) + u1 &&
%   x2' = x1^2 + u2
%% output equation:
%   y1 == 0.05*(x1^2+x2)
dynamics = nonlinearSys(@test_hybrid_parallel_twocomp2_Comp1_Loc1_FlowEq,2,2,...
    @test_hybrid_parallel_twocomp2_Comp1_Loc1_OutputEq,1); 

%% invariant equation:
%   x1 + x2^2 <= 0 &&
%   y1 == 0.05*(x1^2+x2)
vars = sym('x',[2,1]);
syms x1 x2;
eq = x1 + x2^2;
compOp = '<=';

inv = levelSet(eq,vars,compOp);

trans = transition();
%% reset equation:
%   x1' := x1^0.5 + 3 && x2' := x2^2 + 3
reset = struct('f', @test_hybrid_parallel_twocomp2_Comp1_Loc1_Trans1_ResetEq);

%% guard equation:
%   x1 + x2^2 >= 0
vars = sym('x',[2,1]);
syms x1 x2;
eq = x1 + x2^2;
compOp = '==';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 2);

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x1' = -x2^3 + u2 &&
%   x2' = -cos(x1) + u1
%% output equation:
%   y1 == 0.05*(x1-x2^2)
dynamics = nonlinearSys(@test_hybrid_parallel_twocomp2_Comp1_Loc2_FlowEq,2,2,...
    @test_hybrid_parallel_twocomp2_Comp1_Loc2_OutputEq,1); 

%% invariant equation:
%   x1 + x2^2 >= 0 &&
%   y1 == 0.05*(x1-x2^2)
vars = sym('x',[2,1]);
syms x1 x2;
eq = - x1 - x2^2;
compOp = '<=';

inv = levelSet(eq,vars,compOp);

trans = transition();
%% reset equation:
%   x1' := x1^2 - 3  && x2' := x2^3 + 3
reset = struct('f', @test_hybrid_parallel_twocomp2_Comp1_Loc2_Trans1_ResetEq);

%% guard equation:
%   x1 + x2^2 <= 0
vars = sym('x',[2,1]);
syms x1 x2;
eq = - x1 - x2^2;
compOp = '==';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 1);

loc(2) = location('loc2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(1) = hybridAutomaton(loc);

% input names: y2, y3
iBinds{1} = [[2,1];[2,2]];

%-----------------------Component system.cont2_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x3' = x4-x5^2 &&
%   x4' = cos(x3) - u3 &&
%   x5' = x4^2
%% output equation:
%   y2 == 0.05*sin(x5) && y3 == 0.05*(x3 + cos(x4))
dynamics = nonlinearSys(@test_hybrid_parallel_twocomp2_Comp2_Loc1_FlowEq,3,1,...
    @test_hybrid_parallel_twocomp2_Comp2_Loc1_OutputEq,2); 

%% invariant equation:
%   x3^2 + x4 + x5 <= 1 &&
%   y2 == 0.05*sin(x5) &&
%   y3 == 0.05*(x3 + cos(x4))
vars = sym('x',[3,1]);
syms x1 x2 x3;
eq = x2 + x3 + x1^2 - 1;
compOp = '<=';

inv = levelSet(eq,vars,compOp);

trans = transition();
%% reset equation:
%   x3' := x3^2 + 1 &&
%   x4' := x4 + 1 &&
%   x5' := x5 + 1
reset = struct('f', @test_hybrid_parallel_twocomp2_Comp2_Loc1_Trans1_ResetEq);

%% guard equation:
%   x3^2 + x4 + x5 > 1
vars = sym('x',[3,1]);
syms x1 x2 x3;
eq = x2 + x3 + x1^2 - 1;
compOp = '==';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 2);

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x3' = -x4^2+x5 &&
%   x4' = sin(x3) &&
%   x5' = cos(x4) + u3
%% output equation:
%   y2 == 0.05*sin(x3) && y3 == 0.05*(x4 - cos(x5))
dynamics = nonlinearSys(@test_hybrid_parallel_twocomp2_Comp2_Loc2_FlowEq,3,1,...
    @test_hybrid_parallel_twocomp2_Comp2_Loc2_OutputEq,2); 

%% invariant equation:
%   x3^2 + x4 + x5 >= 1 &&
%   y2 == 0.05*sin(x3) &&
%   y3 == 0.05*(x4 - cos(x5))
vars = sym('x',[3,1]);
syms x1 x2 x3;
eq = 1 - x3 - x1^2 - x2;
compOp = '<=';

inv = levelSet(eq,vars,compOp);

trans = transition();
%% reset equation:
%   x3' := x3^2 - 1 &&
%   x4' := x4 - 1 &&
%   x5' := x5 - 1
reset = struct('f', @test_hybrid_parallel_twocomp2_Comp2_Loc2_Trans1_ResetEq);

%% guard equation:
%   x3^2 + x4 + x5 < 1
vars = sym('x',[3,1]);
syms x1 x2 x3;
eq = 1 - x3 - x1^2 - x2;
compOp = '==';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 1);

loc(2) = location('loc2', inv, trans, dynamics);



%% composition: hybrid automaton and input binds
comp(2) = hybridAutomaton(loc);

% input names: y1
iBinds{2} = [[1,1]];

HA = parallelHybridAutomaton(comp,iBinds);

end