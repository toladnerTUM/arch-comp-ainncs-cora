function [dx]=test_hybrid_parallel_twocomp2_Comp2_Loc2_OutputEq(x,u)

dx(1,1) = 0.05*sin(x(1));
dx(2,1) = 0.05*x(2) - 0.05*cos(x(3));
