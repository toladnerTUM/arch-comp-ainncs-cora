function [dx]=test_hybrid_parallel_twocomp2_Comp2_Loc1_OutputEq(x,u)

dx(1,1) = 0.05*sin(x(3));
dx(2,1) = 0.05*x(1) + 0.05*cos(x(2));
