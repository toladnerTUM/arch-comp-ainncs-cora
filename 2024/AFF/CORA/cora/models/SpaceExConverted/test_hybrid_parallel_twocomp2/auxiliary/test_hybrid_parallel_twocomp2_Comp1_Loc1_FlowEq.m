function [dx]=test_hybrid_parallel_twocomp2_Comp1_Loc1_FlowEq(x,u)

dx(1,1) = u(1) - sin(x(2));

dx(2,1) = u(2) + x(1)^2;
