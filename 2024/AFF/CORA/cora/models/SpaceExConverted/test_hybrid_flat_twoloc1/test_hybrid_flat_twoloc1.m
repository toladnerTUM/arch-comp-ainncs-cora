function HA = test_hybrid_flat_twoloc1(~)


%% Generated on 22-May-2023 18:18:29

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (system.cont_1):
%  state x := [x1; x2]
%  input u := [u1]

%------------------------Component system.cont_1---------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x1' = -2*sin(x1) + u1 &&
%   x2' = x1 - x2
dynamics = nonlinearSys(@test_hybrid_flat_twoloc1_Loc1_FlowEq,2,1); 
%% invariant equation:
%   x1 >= 0
A = ...
[-1,0];
b = ...
[0];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1' := 5 && x2' := x2
resetA = ...
[0,0;0,1];
resetc = ...
[5;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 < 0
c = [-1;0];
d = 0;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 2);

loc(1) = location('loc1', inv, trans, dynamics);



%-----------------------------Location loc2--------------------------------

%% flow equation:
%   x1' = -2*cos(x1) + u1 &&
%   x2' = x1 + x2
dynamics = nonlinearSys(@test_hybrid_flat_twoloc1_Loc2_FlowEq,2,1); 
%% invariant equation:
%   x1 <= 0
A = ...
[1,0];
b = ...
[0];
polyOpt = struct('A', A, 'b', b);
inv = mptPolytope(polyOpt);

trans = transition();
%% reset equation:
%   x1' := -5 && x2' := x2
resetA = ...
[0,0;0,1];
resetc = ...
[-5;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 > 0
c = [1;0];
d = 0;

guard = conHyperplane(c,d);

trans(1) = transition(guard, reset, 1);

loc(2) = location('loc2', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end