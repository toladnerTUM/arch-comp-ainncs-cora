function HA = hybrid_bball(~)


%% Generated on 26-Jul-2023 20:53:12

%---------------Automaton created from Component 'model'-------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (model):
%  state x := [x1; x2]
%  input u := [uDummy]

%----------------------------Component model-------------------------------

clear loc

%-----------------------------Location loc1--------------------------------

%% flow equation:
%   x1' == x2
%    & x2' == -981/100
dynA = ...
[0,1;0,0];
dynB = ...
[0;0];
dync = ...
[0;-9.8100000000000004973799150320701];
dynamics = linearSys(dynA, dynB, dync);

%% invariant equation:
%   -x1 <= 0
P_A = ...
[-1,0];
P_b = ...
[-0];

inv = polytope(P_A,P_b);

trans = transition();
%% reset equation:
%   x1' := 0
%    & x2' := -(3*x2)/4
resetA = ...
[0,0;0,-0.75];
resetc = ...
[0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1 == 0 & 
%   x2 <= 0
c = [1;0];
d = 0;C = ...
[0,1;-1,0];
D = [-0;-0];

guard = conHyperplane(c,d,C,D);

trans(1) = transition(guard, reset, 1);

loc(1) = location('loc1', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end