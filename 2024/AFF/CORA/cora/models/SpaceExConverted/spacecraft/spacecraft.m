function HA = spacecraft(~)


%% Generated on 17-Jun-2023 15:09:51

%---------------Automaton created from Component 'model'-------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component 1 (model):
%  state x := [x1; x2; x3; x4]
%  input u := [uDummy]

%----------------------------Component model-------------------------------

clear loc

%------------------------------Location S1---------------------------------

%% flow equation:
%   x1' == x3
%    & x2' == x4
%    & x3' == (926948889703905*x2)/4611686018427387904 - (8306548220084521*x1)/144115188075855872 - (3265054335568193*x3)/1125899906842624 + (5049673395201759*x4)/576460752303423488 - (1434960000000000000*x1 + 60503653440000000000000000)/((x1 + 42164000)^2 - x2^2)^(3/2) + 7099798111223007/8796093022208
%    & x4' == - (1604866734412731*x1)/9223372036854775808 - (299459006578007*x2)/4503599627370496 - (1259760864732321*x3)/144115188075855872 - (6536980713807791*x4)/2251799813685248 - (1434960000000000000*x2)/((x1 + 42164000)^2 - x2^2)^(3/2)
dynamics = nonlinearSys(@spacecraft_Loc1_FlowEq,4,1); 
%% invariant equation:
%   10000 - x2^2 - x1^2 <= 0
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = 10000 - x2^2 - x1^2;
compOp = '<=';

inv = levelSet(eq,vars,compOp);

trans = transition();
%% reset equation:
%   x1' := x1
%    & x2' := x2
%    & x3' := x3
%    & x4' := x4
resetA = ...
[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
resetc = ...
[0;0;0;0];
reset = struct('A', resetA, 'c', resetc);

%% guard equation:
%   x1^2 + x2^2 - 10000 == 0
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = x1^2 + x2^2 - 10000;
compOp = '==';

guard = levelSet(eq,vars,compOp);

trans(1) = transition(guard, reset, 2);

loc(1) = location('S1', inv, trans, dynamics);



%------------------------------Location S2---------------------------------

%% flow equation:
%   x1' == x3
%    & x2' == x4
%    & x3' == (2420212822470693*x2)/9223372036854775808 - (20267551401769*x1)/35184372088832 - (2706379030028195*x3)/140737488355328 + (5044369956280567*x4)/576460752303423488 - (1434960000000000000*x1 + 60503653440000000000000000)/((x1 + 42164000)^2 - x2^2)^(3/2) + 7099798111223007/8796093022208
%    & x4' == - (2420212822470693*x1)/9223372036854775808 - (5187974344175791*x2)/9007199254740992 - (5044369956280567*x3)/576460752303423488 - (1353189303907865*x4)/70368744177664 - (1434960000000000000*x2)/((x1 + 42164000)^2 - x2^2)^(3/2)
dynamics = nonlinearSys(@spacecraft_Loc2_FlowEq,4,1); 
%% invariant equation:
%   x1^2 + x2^2 - 10000 < 0
vars = sym('x',[4,1]);
syms x1 x2 x3 x4;
eq = x1^2 + x2^2 - 10000;
compOp = '<=';

inv = levelSet(eq,vars,compOp);

trans = transition();
loc(2) = location('S2', inv, trans, dynamics);



HA = hybridAutomaton(loc);

end