function sys = test_linear1(~)


%% Generated on 27-May-2024

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component (system.cont_1):
%  state x := [x1; x2]
%  input u := [uDummy]

%------------------------Component system.cont_1---------------------------

%-----------------------------State always---------------------------------

%% equation:
%   x1' = 2*x1 + x2 &&
%   x2' = 4*x1 - 3*x2
dynA = ...
[2,1;4,-3];
dynB = ...
[0;0];
dync = ...
[0;0];
sys = linearSys(dynA, dynB, dync);


end