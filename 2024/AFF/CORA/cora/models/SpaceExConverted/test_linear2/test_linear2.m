function sys = test_linear2(~)


%% Generated on 27-May-2024

%---------------Automaton created from Component 'system'------------------

%% Interface Specification:
%   This section clarifies the meaning of state, input & output dimensions
%   by showing their mapping to SpaceEx variable names. 

% Component (system.cont_1):
%  state x := [x1; x2]
%  input u := [u1]

%------------------------Component system.cont_1---------------------------

%-----------------------------State always---------------------------------

%% equation:
%   x1' = 2*x1 + x2 +0.5*u1 &&
%   x2' = 4*x1 - 3*x2 + -0.25*u1
dynA = ...
[2,1;4,-3];
dynB = ...
[0.5;-0.25];
dync = ...
[0;0];
sys = linearSys(dynA, dynB, dync);


end