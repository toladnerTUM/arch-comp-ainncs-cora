function R = guardIntersect_linProg(obj,R,guard,options)
% guardIntersect_linProg - compute enclosure of guard intersections with
%                          linear programming
%
% Syntax:  
%    R = guardIntersect_linProg(obj,R,guard,options)
%
% Inputs:
%    obj - object of class location
%    R - list of intersections between the reachable set and the guard
%    guard - guard set
%    options - struct containing the algorithm settings
%
% Outputs:
%    R - set enclosing the guard intersection
% 
% Author:       Niklas Kochdumper
% Written:      11-May-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % convert all sets to constrained zonotopes
    R = convertSets(R,options);
    
    % compute the intersections with the guard set
    for i = 1:length(R)
       R{i} = guard & R{i}; 
    end

    % guard is hyperplane -> transform space to be orthogonal to hyperplane
    if isa(guard,'conHyperplane')
        B = gramSchmidt(guard.h.c);
        for i = 1:length(R)
            temp = B'*R{i};
            R{i} = project(temp,2:size(B,1));
        end
    end
    
    % compute union of all guard intersections
    if length(R) == 1
        R = zonotope(R{1});
    else
        temp = or(R{1},R{2:end},'linProg',options.guardOrder);
        R = zonotope(temp.Z);
    end

    % guard is hyperlane -> transform back to original space
    if isa(guard,'conHyperplane') 
        temp = zonotope(guard.h.d/norm(guard.h.c));
        R = B*cartProd(temp,R);
    end
end


% Auxiliary Functions -----------------------------------------------------

function R_ = convertSets(R,options)
% convert all reachable sets to constrained zonotopes
    
    R_ = cell(length(R),1);

    % loop over all relevant reachable sets
    for i = 1:length(R)
        
        if isa(R{i},'polyZonotope')
           R{i} = zonotope(R{i}); 
        end

        temp = reduce(R{i},'redistribute',options.guardOrder);
        R_{i} = conZonotope(temp);  
    end
end