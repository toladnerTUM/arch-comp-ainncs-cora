Repeatability package for tool C2E2 for ARCH2020 category Continuous and Hybrid Systems with Linear Continuous Dynamics.
The Repeatability package can be run by running command
```bash
./measure_all
```
with root permission.
The script will build the docker image using the Dockerfile and run all tests. 
The result will be store in file `result.csv`.
