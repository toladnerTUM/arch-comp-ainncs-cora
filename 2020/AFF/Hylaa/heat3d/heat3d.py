'''
3D Heat Equation Based on Tran's model
'''

import math
import sys
import time

import numpy as np
from scipy.sparse import dia_matrix

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil
from hylaa import lpinstance

def define_ha(samples_per_side, safe):
    '''make the hybrid automaton and init set'''
    
    ha = HybridAutomaton('')

    mode = ha.new_mode('mode')

    # parameters
    diffusity_const = 0.01
    heat_exchange_const = 0.5

    print(f"Making 3d Heat Plate ODEs with {samples_per_side} samples per side...")
    a_matrix = heat3d_dia(samples_per_side, diffusity_const, heat_exchange_const)
    print("Finished Making ODEs")

    mode.set_dynamics(a_matrix)

    error = ha.new_mode('error')
    dims = a_matrix.shape[0]

    center_x = int(math.floor(samples_per_side/2.0))
    center_y = int(math.floor(samples_per_side/2.0))
    center_z = int(math.floor(samples_per_side/2.0))

    center_dim = center_z * samples_per_side * samples_per_side + center_y * samples_per_side + center_x

    threshold = 0.5

    if samples_per_side == 5:
        # 0.10369885
        threshold = 0.1036
        
    elif samples_per_side == 10:
        # 0.02966356
        threshold = 0.0296

    if safe:
        threshold += 1e-4
    
    # x_center >= 0.9
    mat = np.zeros((1, dims), dtype=float)
    mat[0, center_dim] = -1
    rhs = np.array([-threshold], dtype=float) # 0.5 = safe
    trans1 = ha.new_transition(mode, error)
    trans1.set_guard(mat, rhs)

    ##################3
    # make init
    init_vec = make_init(a_matrix, samples_per_side, 1.0)

    # init temp is [0.9, 1.1]
    center = init_vec.copy()
    generator = init_vec * 0.1
    
    init_lpi = lputil.from_zonotope(center, [generator], mode)
    init_list = [StateSet(init_lpi, mode)]

    return ha, init_list

def heat3d_dia(samples, diffusity_const, heat_exchange_const):
    'fast dia_matrix construction for heat3d dynamics'

    samples_sq = samples**2
    dims = samples**3
    step = 1.0 / (samples + 1)

    a = diffusity_const * 1.0 / step**2
    d = -2.0 * (a + a + a)

    data = np.zeros((7, dims))
    offsets = np.array([-samples_sq, -samples, -1, 0, 1, samples, samples_sq], dtype=float)

    # element with z = -1
    data[0, :-samples_sq] = a

    # element with y = -1
    for s in range(samples):
        start = s * samples_sq
        end = (s + 1) * (samples_sq) - samples
        data[1, start:end] = a

    # element with x = -1
    for s in range(samples_sq):
        start = s * samples
        end = (s + 1) * (samples) - 1
        data[2, start:end] = a

    #### diagonal element ####
    data[3, :] = d     # (prefill)

    # adjust when z = 0 or z = samples-1
    data[3, :samples_sq] += a
    data[3, -samples_sq:] += a

    # adjust when y = 0 or y = samples-1
    for z in range(samples):
        z_offset = z * samples_sq

        data[3, z_offset:z_offset + samples] += a
        data[3, z_offset + samples_sq - samples:z_offset + samples_sq] += a

    # adjust when x = 0 (and add diffusion term when x = samples-1)
    for z in range(samples):
        for y in range(samples):
            offset = z * samples_sq + y * samples

            data[3, offset] += a

            data[3, offset + samples - 1] += a / (1+heat_exchange_const * step)

    #### end diagnal element ####
    # element with x = +1
    for s in range(samples_sq):
        start = 1 + s * samples
        end = (s + 1) * samples
        data[4, start:end] = a

    # element with y = +1
    for s in range(samples):
        start = s * samples_sq + samples
        end = (s + 1) * (samples_sq)
        data[5, start:end] = a

    # element with z = +1
    data[6, samples_sq:] = a

    rv = dia_matrix((data, offsets), shape=(dims, dims))
    assert np.may_share_memory(rv.data, data)     # make sure we didn't copy memory

    return rv

def make_init(a_matrix, samples, init_temp):
    '''returns an initial state vector'''

    vec = np.zeros((a_matrix.shape[0], 1), dtype=float)

    assert samples == 5 or (samples >= 10 and samples % 10 == 0), "init region isn't evenly divided by discretization"

    # maximum z point for initial region is 0.2 for 5 samples and 0.1 otherwise
    max_z = samples // 10 + 1 if samples >= 10 else 2 * samples // 10 + 1

    for z in range(max_z):
        zoffset = z * samples * samples

        for y in range(2 * samples // 10 + 1):
            yoffset = y * samples

            for x in range(4 * samples // 10 + 1):
                index = x + yoffset + zoffset

                vec[index] = init_temp

    return vec

def make_settings(samples_per_side):
    'make the reachability settings object'

    lpinstance.StaticSettings.MAX_MEMORY_SWIGLPK_LEAK_GB = 20.0 

    # see hylaa.settings for a list of reachability settings
    settings = HylaaSettings(0.02, 40.0) # step size = 0.1, time bound 20.0
    settings.plot.plot_mode = PlotSettings.PLOT_NONE
    settings.stdout = HylaaSettings.STDOUT_VERBOSE
    settings.plot.filename = "heat3d.png"

    settings.plot.xdim_dir = None # x dimension will be time

    center_x = int(math.floor(samples_per_side/2.0))
    center_y = int(math.floor(samples_per_side/2.0))
    center_z = int(math.floor(samples_per_side/2.0))

    center_dim = center_z * samples_per_side * samples_per_side + center_y * samples_per_side + center_x
    settings.plot.ydim_dir = center_dim

    return settings

def run_hylaa():
    'Runs hylaa with the given settings, returning the HylaaResult object.'

    samples_per_side = 5
    safe = True

    if len(sys.argv) > 1:
        samples_per_side = int(sys.argv[1])
        print(f"Running with samples_per_side = {samples_per_side}")

        if len(sys.argv) > 2:
            assert sys.argv[2] == 'safe' or sys.argv[2] == 'unsafe'
            
            safe = sys.argv[2] == 'safe'
            print(f"Running with safe = {safe}")

    start = time.perf_counter()
    ha, init = define_ha(samples_per_side, safe)
    settings = make_settings(samples_per_side)

    result = Core(ha, settings).run(init)
    diff = time.perf_counter() - start

    assert not (safe and result.has_concrete_error), "error mode was reachable"

    n = '?'

    if samples_per_side == 5:
        n = '01'
    elif samples_per_side == 10:
        n = '02'

    with open("../results.csv", "a") as f:
        f.write(f"Hylaa,HEAT,{n},1,{diff},\n")

if __name__ == '__main__':
    # two args: dims_per_side, 'safe'/'unsafe'
    run_hylaa() 
