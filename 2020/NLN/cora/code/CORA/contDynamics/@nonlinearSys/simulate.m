function [t,x,ind] = simulate(obj,params,options)
% simulate - simulates the system within a location
%
% Syntax:  
%    [t,x,ind] = simulate(obj,params,options)
%
% Inputs:
%    obj - nonlinearSys object
%    params - struct containing the parameters for the simulation
%       .tStart: initial time
%       .tFinal: final time
%       .timeStep: time step size
%       .x0: initial point
%    options - ODE45 options (for hybrid systems)
%
%    tstart - start time
%    tfinal - final time
%    x0 - initial state 
%    options - contains, e.g. the events when a guard is hit
%
% Outputs:
%    t - time vector
%    x - state vector
%    ind - returns the event which has been detected
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      03-May-2007 
% Last update:  12-March-2008
%               08-May-2020 (MW, update interface)
% Last revision: ---

%------------- BEGIN CODE --------------

try
    if isfield(params,'timeStep')
        [t,x,~,~,ind] = ode45(getfcn(obj,params),...
            params.tStart:params.timeStep:params.tFinal,params.x0,options);
    else
        [t,x,~,~,ind] = ode45(getfcn(obj,params),...
            [params.tStart,params.tFinal],params.x0,options);
    end
catch
    if isfield(params,'timeStep')
        [t,x] = ode45(getfcn(obj,params),...
            params.tStart:params.timeStep:params.tFinal,params.x0,options);
    else
        [t,x] = ode45(getfcn(obj,params),...
            [params.tStart,params.tFinal],params.x0,options);
    end
    ind=[];
end

%------------- END OF CODE --------------