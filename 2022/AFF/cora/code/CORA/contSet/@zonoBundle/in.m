function res = in(zB,S,varargin)
% in - determines if a zonotope bundle contains a set or a point
%
% Syntax:  
%    res = in(zB,S)
%    res = in(zB,S,type)
%
% Inputs:
%    zB - zonoBundle object
%    S - contSet object or single point
%    type - type of containment check ('exact' or 'approx')
%
% Outputs:
%    res - true/false
%
% Example: 
%    I1 = interval([0;-1],[2;2]);
%    I2 = I1 + [2;0];
%    Z1 = zonotope([0 1 2 0;0 1 0 2]);
%    Z2 = zonotope([3 -0.5 3 0;-1 0.5 0 3]);
%    zB = zonoBundle({Z1,Z2});
%
%    in(zB,I1)
%    in(zB,I2)
%
%    figure; hold on;
%    plot(zB,[1,2],'b');
%    plot(I1,[1,2],'g');
%    
%    figure; hold on;
%    plot(zB,[1,2],'b');
%    plot(I2,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval/in, conZonotope/in
%
% Author:       Niklas Kochdumper
% Written:      19-Nov-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

res = true;

% parse input arguments
[type] = setDefaultValues({{'exact'}},varargin{:});

% check input arguments
inputArgsCheck({{zB,'att',{'zonoBundle'},{''}};
                {S,'att',{'contSet','numeric'},{''}};
                {type,'str',{'exact','approx'}}});
    
% point or point cloud in zonotope bundle containment
if isnumeric(S)
    
    for i = 1:size(S,2)
        res = in(conZonotope(zB),S(:,i));
        if ~res
            return;
        end
    end
    
% capsule/ellipsoid in zonotope bundle containment
elseif isa(S,'capsule') || isa(S,'ellipsoid')
    
    poly = mptPolytope(zB);
    res = in(poly,S); 

else
    
    % use the fast but over-approximative or the exact but possibly
    % slow containment check
    if strcmp(type,'exact')

        if isa(S,'taylm') || isa(S,'polyZonotope')
            throw(CORAerror('CORA:noExactAlg',S,"'taylm' or 'polyZonotope'"));
        elseif isa(S,'interval')
            res = in(zB,vertices(S));
        else
            poly = mptPolytope(zB);
            res = in(poly,S); 
        end
        
    else
        
        if isa(S,'taylm') || isa(S,'polyZonotope')
            poly = mptPolytope(zB);
            res = in(poly,S); 
        else
            cZ = conZonotope(zB);
            res = in(cZ,S,type);
        end
    end
end

%------------- END OF CODE --------------