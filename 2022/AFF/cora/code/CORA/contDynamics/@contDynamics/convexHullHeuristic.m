function [res,points] = convexHullHeuristic(obj, R, params, options)
    % convexHullHeuristic - simulates a system using different heuristics
%
% Syntax:
%    res = convexHullHeuristic(obj, R, params, options)
%    [res,points] = convexHullHeuristic(obj, R, params, options)
%
% Inputs:
%    obj - contDynamics object
%    R - object of class reachSet storing the computed reachable set
%    params - struct containing the parameter that define the 
%             reachability problem
%    options - struct containing settings for the random simulation
%
%       .points:    number of random initial points (positive integer)
%       .vertSamp:  flag that specifies if random initial points, inputs,
%                   and parameters are sampled from the vertices of the 
%                   corresponding sets (0 or 1)
%       .strechFac: stretching factor for enlarging the reachable sets 
%                   during execution of the algorithm (scalar > 1).
%       .start:     option to choose startSet, "large" chooses the first n
%                   vertices, "veryLarge" chooses all vertices, "small" 
%                   samples random start points like simulateRRT()
%       .heuristic: option to choose heuristic, "furthestFromAverage",
%                   "furthestFromAverage2" (compute convex hull first),
%                   "extremeDistances", "extremeDistancesP" 
%                   (Max Min According to Dimensions (Per Vertex))
%
% Outputs:
%    res - object of class simResult storing time and states of the
%          simulated trajectories
%    points - final points of the simulation
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Fabian Frank
% Written:      02-August-2021
% Last update:  02-August-2021
% Last revision:---

%------------- BEGIN CODE --------------

% new options preprocessing

%requires own optionCheck
options = validateOptions(obj,'simulateRRT',params,options);


% set simulation options
stepsizeOptions = odeset('MaxStep',0.2*(options.tFinal-options.tStart));
% generate overall options
opt = odeset(stepsizeOptions);


% obtain set of uncertain inputs 
if isfield(options,'uTransVec')
    U = options.uTransVec(:,1) + options.U;
else
    U = options.uTrans + options.U;
end

% possible extreme inputs
V_input = vertices(U);
V_input_mat = V_input;
nrOfExtrInputs = length(V_input_mat(1,:));

%%%%new
%V_input = vertices(options.Ur);
%length(V_input);
%additi = zeros(length(options.R0.Z(:,1))-length(V_input(:,1)),length(V_input(1,:)));

%V_input_matO = cat(1,V_input,additi);
%if isfield(options,'uTransVec')
%    V_input_mat = options.uTransVec(:,1) + V_input_matO;
%else
%    V_input_mat = options.uTrans + V_input_matO;
%end
%nrOfExtrInputs = length(V_input_mat(1,:));


% initialize simulation results
%x = cell(length(R.timePoint.set)*options.points,1);
%t = cell(length(R.timePoint.set)*options.points,1);
x = cell(1,1);
t = cell(1,1);

cnt = 1;

%select n vertices of R0
if(options.start == "large")
V_startObject = vertices(options.R0);
if(length(V_startObject) > options.points)
    V_startObject = V_startObject(:,1:options.points);
end
end

%select all vertices of R0
if(options.start == "veryLarge")
V_startObject = vertices(options.R0);
end

if(options.start == "small")
% init obtained states from the RRT
if options.vertSamp
   X = randPoint(options.R0,options.points,'extreme'); 
else
   X = randPoint(options.R0,options.points,'normal');
end
   V_startObject = X;
end

% loop over all time steps
for iStep = 1:length(R.timePoint.set)
%for iStep = 1:14
    
    iStep
    
    % update time
    options.tStart = infimum(R.timeInterval.time{iStep});
    options.tFinal = supremum(R.timeInterval.time{iStep});
    V_startObjectN = [];
    % loop over all trajectories
    for iSample = 1:length(V_startObject) 
    
        %nearest neighbor and selected state
        options.x0 = V_startObject(:,iSample);
        %options.x0 = nearestNeighbor(V_startObject(:,iSample),V_startObject,normMatrix);

        
        % update set of uncertain inputs when tracking
       if isfield(options,'uTransVec')
          U = options.uTransVec(:,iStep) + options.U;
          V_input = vertices(U);
          V_input_mat = V_input;
        end
        
        %if isfield(options,'uTransVec')
        %    V_input_mat = options.uTransVec(:,iStep) + V_input_matO;
        %end

        %simulate model to find out best input
        for iInput = 1:nrOfExtrInputs
            %set input
            options.u = V_input_mat(:,iInput);
            %simulate
            [t_traj{iInput},x_traj{iInput}] = simulate(obj,options,opt);   
            x_next(:,iInput) = x_traj{iInput}(end,:);  
            x{cnt} = x_traj{iInput};
            t{cnt} = t_traj{iInput};
            cnt = cnt + 1;
        end
        
        V_startObjectN = cat(2,V_startObjectN,x_next);
        %arr = ones(nrOfExtrInputs) * V_indices(iSample);
        %V_indicesN = cat(2,V_indicesN,arr);
        

    end

    %compute average value of the trajectories and take the n which are
    %furthest apart
    if(options.heuristic == "furthestFromAverage2")
    V_startObjectN = mptPolytope.enclosePoints(V_startObjectN);
    V_startObjectN = vertices(V_startObjectN);

    M = mean(V_startObjectN,2);
    
    dist = V_startObjectN - M;
    [~, I] = sortAccDistance(dist);     
    V_startObjectN = V_startObjectN(:,I);
    if(options.points < length(V_startObjectN))
        V_startObjectN = V_startObjectN(:,1:options.points); 
    end
    V_startObject = V_startObjectN;
    end
    
    if(options.heuristic == "furthestFromAverage")
    M = mean(V_startObjectN,2);
    
    dist = V_startObjectN - M;
    
    [~, I] = sortAccDistance(dist);
    V_startObjectN = V_startObjectN(:,I);
    if(options.points < length(V_startObjectN))
        V_startObjectN = V_startObjectN(:,1:options.points); 
    end
    V_startObject = V_startObjectN;    
    end
    
    %take max min value according to each dimension
    if(options.heuristic == "extremeDistances")
    D = sortAccDistance2(V_startObjectN);
    V_startObject = D;
    
    %%%reduces to options.points points
    %msize = length(V_startObject);
    %if(msize > options.points)
    %idx = randperm(msize);
    %V_startObject = V_startObject(:,idx(1:option));
    %end
    end
    
    %take max min value according to each dimension independently for the
    %vertices of the initial set
    if(options.heuristic == "extremeDistancesP")
    V_startObject = safeOneForeach(V_startObjectN,iStep);
    end
    
    % update X
end

% construct object storing the simulation results
res = simResult(x,t);
end

% Auxiliary Functions -----------------------------------------------------
function [A, ind] = sortAccDistance(arr)
A = zeros(length(arr),1);
for i = 1:length(arr)
   h = arr(:,i);
   h = h.^2;
   %h = abs(h);
   x = sum(h);
   A(i) = x;
    
end
%[A,ind] = sort(A);
[A,ind] = sort(A,'descend');
end

function D = sortAccDistance2(arr)
%array = arr
[~,ind] = sort(arr,2);
ord = ind(1,:);
A = arr(:,ord);
k = length(arr(1,:));
dim = length(arr(:,1));
number = 1;


D = cat(2,A(:,1:number),A(:,k-number+1:k));
[~,ind] = sort(arr,2);

for i = 2:dim
    ord = ind(i,:);
    A = arr(:,ord);
    D = cat(2,D,A(:,k-number+1:k));
    D = cat(2,D,A(:,1:number));
end
%[A,ind] = sort(A,'descend');
end

function s = safeOneForeach(arr,i)
dim = length(arr(:,1));
dim2 = dim;
if i == 1
    k = 2^dim2;
else
    k = 2^dim2 * 2 * dim;
end
l = length(arr(1,:)) / k;
s = sortAccDistance2(arr(:,1:k));
for corner = 1:(l-1)
    arr(:,corner*k+1:(corner+1)*(k));
    z = sortAccDistance2(arr(:,corner*k+1:(corner+1)*k));
    s = cat(2,s,z);
end
end

      












%------------- END OF CODE --------------