function val = norm(E,varargin)
% norm - compute the maximum Euclidean norm of an ellipsoid
%
% Syntax:  
%    val = norm(E)
%    val = norm(E,type)
%
% Inputs:
%    E    - ellipsoid object 
%    type - (optional) norm type (default: 2)
%
% Outputs:
%    val - value of the maximum norm
%
% Example: 
%    E = ellipsoid.generateRandom('Dimension',2);
%    val = norm(ellipsoid(E.Q,zeros(size(E.q))));
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      20-November-2019
% Last update:  31-July-2020
%               04-July-2022 (VG: class array case)
% Last revision:---

%------------- BEGIN CODE --------------
type = setDefaultValues({{2}},varargin{:});
if ~any(type==[1,2,inf]) && ~strcmp(type,'fro')
    throw(CORAerror('CORA:wrongValue','second',"1, 2, Inf, or 'fro'"));
end

% check input arguments
inputArgsCheck({{E,'att',{'ellipsoid'},{'scalar'}}});


if type ~= 2
    throw(CORAerror('CORA:noExactAlg',E,type,...
        'Only implemented for Euclidean norm'));
end

if ~all(E.q==0)
    throw(CORAerror('CORA:noExactAlg',E,type,...
        'Not yet implemented for non-zero center'));
end

% transform into eigenspace
lmax = max(eig(E.Q));
val = sqrt(lmax);

%------------- END OF CODE --------------