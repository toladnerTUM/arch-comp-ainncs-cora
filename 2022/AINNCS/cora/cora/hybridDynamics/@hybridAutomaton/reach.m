function [R,res] = reach(HA,params,options,varargin)
% reach - computes the reachable set of a hybrid automaton
%
% Syntax:  
%    R = reach(HA,params,options)
%    [R,res] = reach(HA,params,options,spec)
%    [R,res] = reach(HA,params,options,spec,spec_location)
%
% Inputs:
%    HA - hybridAutomaton object
%    params - parameter defining the reachability problem
%    options - options for the computation of the reachable set
%    spec - object of class specification 
%    spec_location - cell array which maps which specifications are
%                    relevant in which locations, e.g.,
%                       spec_location = {[1,2], [2]}
%                    indicates that specifications 1 and 2 are relevant to
%                    location 1, and specification 2 is relevant to location 2
%
% Outputs:
%    R - reachSet object storing the reachable set
%    res - true/false whether specifications are satisfied
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: location/reach

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      07-May-2007 
% Last update:  16-August-2007
%               20-August-2013
%               30-October-2015
%               22-August-2016
%               19-December-2019 (NK, restructured the algorithm)
%               13-October-2021 (MP, implemented location-specific
%                                specifications)
% Last revision: ---

%------------- BEGIN CODE --------------

    res = true;
    
    % options preprocessing
    options = validateOptions(HA,mfilename,params,options);

    % compute derivatives for each location
    compDerivatives(HA,options);
    
    % check specifications
    spec = [];
    spec_locations = {};
    if nargin >= 4
       spec = varargin{1};
       if nargin >= 5
           spec_locations = varargin{2};
       end
    end
    options = check_flatHA_specification(options,HA,spec,spec_locations);

    % initialize reachable set
    R = [];

    % initialize queue for reachable set computation (during the
    % computation, multiple branches of reachable set can emerge, requiring
    % to compute the successor reachable sets for all branches one after
    % the other; the queue handles this process)
    list{1}.set = options.R0;
    list{1}.loc = options.startLoc;
    list{1}.time = interval(options.tStart);
    list{1}.parent = 0;

    % display information on command window
    if options.verbose
        disp(newline + "Start analysis...");
    end

    % loop until the queue is empty or a specification is violated
    while ~isempty(list) && res

        % get location, initial set, start time, and parent branch for
        % reachable set computation of first element in the queue
        locID = list{1}.loc;
        R0 = list{1}.set;
        tStart = list{1}.time;
        parent = list{1}.parent;

        % get input and specification for the current location
        options.U = options.Uloc{locID};
        options.specification = options.specificationLoc{locID};
        % get timeStep for the current location (unless adaptive)
        if ~strcmp(options.linAlg,'adaptive')
            options.timeStep = options.timeStepLoc{locID};
        end

        % check if current location has an immediate transition (this is
        % the case if any guard set is defined as guard = [])
        immediateTransition = cellfun(@(x) isnumeric(x.guard) && isempty(x.guard),...
            HA.location{locID}.transition,'UniformOutput',true);

        if any(immediateTransition)
            % execute reset function of immediate transition: overwrite
            % start set and location (time and parent do not change)
            list{1}.set = reset(HA.location{locID}.transition{immediateTransition},...
                            R0,options.U);
            list{1}.loc = HA.location{locID}.transition{immediateTransition}.target;

            % notify user that an immediate transition has occurred
            if options.verbose
                disp("  transition: location " + locID + ...
                    " -> location " + list{1}.loc + "..." + ...
                    " (time: " + string(tStart) + ")");
            end
            continue

        else
            if options.verbose
                fprintf("Compute reachable set in location " + locID + ...
                    "..." + " (time: " + string(tStart) + " to ");
            end

            % compute the reachable set within a location until either the
            % final time is reached or the reachable set hits a guard set
            % and the computation proceeds in another location
            [Rtemp,Rjump,res] = reach(HA.location{locID},R0,tStart,options);

            if options.verbose
                fprintf(string(Rtemp(1).timePoint.time{end}) + ")\n");
            end
        end

        % remove current element from the queue
        list = list(2:end);

        % add the new branches of reachable sets to the queue
        for i=1:length(Rjump)
            Rjump{i}.parent = Rjump{i}.parent + length(R);
        end
        list = [list; Rjump];

        % display transitions on command window
        if options.verbose && isscalar(list)
            % multiple successor locations currently not supported...
            disp("  transition: location " + locID + " -> location " + ...
                string(list{1}.loc) + "... (time: " + string(list{1}.time) + ")");
        end

        % store the computed reachable set
        for i=1:size(Rtemp,1)
            temp = reachSet(Rtemp.timePoint,Rtemp.timeInterval,...
                            Rtemp.parent,locID);
            R = add(R,temp,parent);
        end
    end

    if options.verbose
        disp("...time horizon reached, analysis finished." + newline);
    end
end



% Auxiliary Function ------------------------------------------------------

function options = check_flatHA_specification(options,HA,spec,spec_locations)
% rewrites specifications in the correct format

    numLoc = length(HA.location);

    % initialize specifications with empty cells
    if isempty(spec)

        specLoc = cell(numLoc,1);

    % adjust specification for each location  
    elseif ~iscell(spec)   

        % check if time information is provided
        for i = 1:length(spec)
            if ~isempty(spec(i).time)
                throw(CORAerror('CORA:notSupported',...
                        'Timed specifications are not yet supported for hybrid automata!')); 
            end
        end
        
        specLoc = cell(numLoc,1);
        % if no information to the concerned locations are given,
        % specifications are assumed to be for all locations
        
        if isempty(spec_locations)
            for i = 1:numLoc
                specLoc{i} = spec;
            end
            
        % assign the correct specifications to each location    
        else
            for i = 1:numLoc
                specLoc{i} = spec(spec_locations{i});
            end
        end

    % copy specification for each location
    else

        % check if time information is provided
        for i = 1:length(spec)
            for j = 1:length(spec{i})
                if ~isempty(spec{i}(j).time)
                    throw(CORAerror('CORA:notSupported',...
                        'Timed specifications are not yet supported for hybrid automata!')); 
                end
            end
        end
        
        % copy specifications
        if all(size(spec) ~= [numLoc,1])
            error('Input argument "spec" has the wrong format!');
        else
            specLoc = spec;
        end
    end

    options.specificationLoc = specLoc;

end

%------------- END OF CODE --------------