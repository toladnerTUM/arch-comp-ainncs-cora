function C = mtimes(arg1,arg2)
% mtimes - Overloaded '*' operator for the multiplication of a matrix with 
%    a capsule
%
% Syntax:  
%    C = mtimes(arg1,arg2)
%
% Inputs:
%    arg1 - numerical matrix or capsule object
%    arg2 - numerical matrix or capsule object
%
% Outputs:
%    C - capsule after multiplication with a matrix
%
% Example: 
%    C = capsule([1; 1], [0; 1], 0.5);
%    M = [0 1; 1 0];
%    C_ = M*C;
%    plot(C); hold on;
%    plot(C_);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plus

% Author:       Matthias Althoff
% Written:      04-March-2019
% Last update:  05-May-2020 (MW, standardized error message)
% Last revision:---

%------------- BEGIN CODE --------------

%Find a capsule object
%Is arg1 a capsule?
if isa(arg1,'capsule')
    %initialize resulting capsule
    C = arg1;
    %initialize other summand
    matrix = arg2;
%Is factor2 a capsule?    
elseif isa(arg2,'capsule')
    %initialize resulting capsule
    C = arg2;
    %initialize other summand
    matrix = arg1;  
end

%numeric matrix
if isnumeric(matrix) && size(matrix,2) == dim(C)
    % new center
    C.c = matrix*C.c;
    % new generator
    C.g = matrix*C.g;
    % new axes of ellipsoid of transformed ball
    newAxes = eig(matrix*matrix');
    C.r = C.r*max(newAxes);
    
% wrong matrix size    
elseif size(matrix,2) ~= dim(C)
    throw(CORAerror('CORA:dimensionMismatch','obj1',matrix,...
        'size1',size(matrix),'obj2',C,'dim2',dim(C)));

%something else?
else
    % throw error for given arguments
    throw(CORAerror('CORA:noops',arg1,arg2));
end

%------------- END OF CODE --------------