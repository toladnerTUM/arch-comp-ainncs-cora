function res = in(I,S,varargin)
% in - determines if an interval contains a set or a point
%
% Syntax:  
%    res = in(I,S)
%
% Inputs:
%    I - interval object
%    S - contSet object or single point
%
% Outputs:
%    res - true/false
%
% Example: 
%    I1 = interval([-1;-2],[2;3]);
%    I2 = interval([0;0],[1;1]);
%
%    in(I1,I2)
%    in(I1,[1;2])
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/in

% Author:       Niklas Kochdumper
% Written:      16-May-2018
% Last update:  02-Sep-2019
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
[TOL] = setDefaultValues({{1e-12}},varargin{:});

% check input arguments
% inputArgsCheck({{I,'att',{'interval'},{''}};
%                 {S,'att',{'numeric','contSet'},{''}}});

% init result
res = false;

% point in interval containment
if isnumeric(S)
    
    % account for numerical jitter
    obj2lb = S + TOL;
    obj2ub = S - TOL;
    
    if all([all(I.inf <= obj2lb), all(I.sup >= obj2ub)])
        res = true;
    end

% interval in interval containment
elseif isa(S,'interval')
    
    % check for dimension mismatch
    if dim(I) ~= dim(S)
        throw(CORAerror('CORA:dimensionMismatch','obj1',I,...
            'size1',dim(I),'obj2',S,'size2',dim(S)));
    end

    if all(I.sup >= S.sup) && all(I.inf <= S.inf)
        res = true;
    end

% other set in interval containment
else

    res = in(mptPolytope(I),S);

end

%------------- END OF CODE --------------