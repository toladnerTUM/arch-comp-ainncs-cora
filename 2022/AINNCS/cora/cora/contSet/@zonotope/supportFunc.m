function [val,x,fac] = supportFunc(Z,dir,varargin)
% supportFunc - calculates the upper or lower bound of a zonotope along a
%    certain direction
%
% Syntax:  
%    val = supportFunc(Z,dir)
%    val = supportFunc(Z,dir,type)
%
% Inputs:
%    Z - zonotope object
%    dir - direction for which the bounds are calculated (vector)
%    type - upper or lower bound ('lower' or 'upper')
%
% Outputs:
%    val - bound of the zonotope in the specified direction
%    x - support vector
%    fac - factor values that correspond to the upper bound
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: conZonotope/supportFunc

% Author:       Niklas Kochdumper
% Written:      19-November-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
    
% parse input arguments
if size(dir,1) == 1 && size(dir,2) >= 1
    % transpose dir
    dir = dir';
end
[type] = setDefaultValues({{'upper'}},varargin{:});

% check input arguments
inputArgsCheck({{Z,'att',{'zonotope'},{'nonempty'}};
                {dir,'att',{'numeric'},{'vector'}};
                {type,'str',{'lower','upper'}}});

% project zonotope onto the direction
obj_ = dir'*Z;

% get object properties
c = center(obj_);
G = generators(obj_);

% upper or lower bound
if strcmp(type,'lower')
    
   val = c - sum(abs(G));
   fac = -sign(G)';
    
else
    
   val = c + sum(abs(G));
   fac = sign(G)';
   
end

% compute support vector
x = center(Z) + generators(Z)*fac;

%------------- END OF CODE --------------