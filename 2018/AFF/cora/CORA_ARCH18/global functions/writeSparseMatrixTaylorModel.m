function writeSparseMatrixTaylorModel(M,var,fid,x,u,xInt,uInt,r,rInt)
% writeSparseMatrixTaylorModel - write a sparse matrix in such a way that
%                                the inputs are taylor models and the
%                                output is a interval matrix
%
% Syntax:  
%    writeSparseMatrixTaylorModel(M,var,fid,x,u)
%
% Inputs:
%    M - symbolic matrix
%    var - name of the matrix that is written
%    fid - identifier of the file to which the matrix is written
%    x - symbolic variables for the states
%    u - symbolic variables for the inputs
%    xInt - symbolic variables for the state intervals
%    uInt - symbolic variables for the input intervals
%
% Outputs:
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Niklas Kochdumper
% Written:      15-July-2017
% Last update:  20-July-2017
%               24-January-2018
% Last revision:---

%------------- BEGIN CODE --------------


% define symbolic variables for interval inputs
for i=1:length(x)
    command=['xInt(',num2str(i),',1)=sym(''xIntL',num2str(i),'R'');'];
    eval(command);
end

for i=1:length(u)
    command=['uInt(',num2str(i),',1)=sym(''uIntL',num2str(i),'R'');'];
    eval(command);
end

%write each row
[row,col] = find(M~=0);

for i=1:length(row)
    iRow = row(i);
    iCol = col(i);
    temp = M(iRow,iCol);
    if ~isempty(symvar(temp))
        str1=bracketSubs(char(vpa(temp)));
        Mtemp = M(iRow,iCol);
        if ~isempty(r) && ~isempty(rInt)
            Mtemp = subs(Mtemp,r,rInt);
        end
        Mtemp = subs(Mtemp,[x;u],[xInt;uInt]);
        str2=bracketSubs(char(Mtemp));
        str=[var,'(',num2str(iRow),',',num2str(iCol),') = intersect(interval(',str1,'),',str2,');'];
    else
        str=[var,'(',num2str(iRow),',',num2str(iCol),') = interval(',char(temp),');']; 
    end
    %write in file
    fprintf(fid, '%s\n', str);
end

%------------- END OF CODE --------------