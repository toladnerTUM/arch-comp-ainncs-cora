function romhandle = createROMfile(fom,method,Ur,A,N,Ws,P)
% ROM - writes the file of the ROM, consisting of two parts:
%    1. the reduced linear part: Ar = Ur^T A Ur
%    2. the reduced nonlinear part: Nr(xr): Rr -> Rr
%    additional simplification: unused terms in nonlinear part are omitted
%
% Syntax:  
%    createROMfile(fom,Ur,A,N)
%
% Inputs:
%    fom - nonlinearSys object (full-order model = FOM, in Rn)
%    method - order reduction method ('POD','DEIM','Gappy')
%    Ur - projection matrix on subspace Rr
%    A - rxr matrix containing linear terms
%    N - n -> n nonlinear function
%    Ws - projection matrix of nonlinear terms on subspace Rs
%    P - projection matrix for nonlinear function
%
% Outputs:
%    romhandle - function handle of ROM in cora/models/Cora
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       01-December-2020
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

% dimensions
[n,r] = size(Ur);

% original state
xsym = sym('x',[n,1]);

% reduced state
xrsym = sym('xr',[r,1]);

% path and filename for new models-file
path = [coraroot filesep 'models' filesep 'Cora'];
romfilename = [fom.name '_ROM'];

% header syntax and comment
funcStr = ['function dxr = ' romfilename '(xr,u)'];
header = ['% automatically generated file for ROM of ''' fom.name ''' system' newline ...
    '% - reduction method:   ' method newline ...
    '% - original dimension: ' num2str(n) newline ...
    '% - reduced dimension:  ' num2str(r)];

% linear part (same for all approaches) -----------------------------------
Ar = Ur' * A * Ur;

% linear part by matrix-vector product
ArStr = num2str(Ar,16);
temp = '';
for i=1:r-1
	temp = [temp ArStr(i,:) ';...' newline];
end
temp = [temp ArStr(r,:) '];' newline];
ArStr = ['Ar = [' newline temp];
% -------------------------------------------------------------------------


% nonlinear part
nonlinpartname = 'nonlinpart';

% standard POD approach ---------------------------------------------------
if strcmp(method,'POD')

    % preliminary information: which dimensions are needed for N?
    requiredforN = ~all(strcmp(string(jacobian(N,sym('x',[n,1]))),"0"),1);
    neglectedDimsStr = '';
    temp = string(find(~requiredforN));
    for i=1:length(temp)-1
        neglectedDimsStr = [neglectedDimsStr char(temp(i)) '/'];
    end
    neglectedDimsStr = [neglectedDimsStr char(temp(end))];

    % new nonlinearity in three steps:

    % 1. projection to Rn
    % note: only evaluate those dimensions which are needed for N
    Urxr = string(Ur * xrsym);
    UrxrStr = ['Urxr = zeros(' num2str(n) ',1);' newline];
    for i=1:n
        if requiredforN(i)
            UrxrStr = [UrxrStr 'Urxr(' num2str(i) ') = ' char(Urxr(i)) ';' newline]; 
        else
            UrxrStr = [UrxrStr 'Urxr(' num2str(i) ') = 0;' newline]; 
        end
    end
    % replace xr1 with xr(1) etc.
    for j=1:r
        old = ['xr' num2str(j)];
        new = ['xr(' num2str(j) ')'];
        UrxrStr = strrep(UrxrStr,old,new);
    end
    infoStr = ['(Note: The dimensions ' neglectedDimsStr ...
        ' of Urxr are disregarded because they are not used further.)'];

    % 2. evaluation of nonlinear functions
    Urxrsym = sym('Urxr',[n,1]);
    NUrxr = N;
    for i=1:n
        NUrxr = subs(NUrxr,xsym(i),Urxrsym(i));
    end
    NUrxrStr = ['NUrxr = zeros(' num2str(n) ',1);' newline];
    for i=1:n
        NUrxrStr = [NUrxrStr 'NUrxr(' num2str(i) ') = ' char(NUrxr(i)) ';' newline]; 
        % replace Urxr1 with Urxr(1) etc.
        for j=1:n
            old = ['Urxr' num2str(j)];
            new = ['Urxr(' num2str(j) ')'];
            NUrxrStr = strrep(NUrxrStr,old,new);
        end
    end

    % 3. projection on Rr by matrix-vector product
    UrTStr = num2str(Ur',16);
    temp = '';
    for i=1:r-1
        temp = [temp UrTStr(i,:) ';...' newline];
    end
    temp = [temp UrTStr(r,:) '];' newline];
    UrTStr = ['UrT = [' newline temp];
    UrNUrxrStr = [nonlinpartname ' = UrT * NUrxr;' newline];

    nonlinStr = ['% 1. Mapping of xr to Rn:' newline ...
        '% ' infoStr newline UrxrStr newline ...
        '% 2. Evaluation of original nonlinear functions:' newline NUrxrStr newline ...
        '% 3. Mapping back to Rr:' newline UrTStr UrNUrxrStr];

    
% DEIM approach -----------------------------------------------------------
elseif strcmp(method,'DEIM') || strcmp(method,'Q-DEIM') || strcmp(method,'Gappy')
    
    % precompute linear mapping
    linmap = Ur' * Ws * (P' * Ws)^(-1);
    linmapStr = num2str(linmap,16);
    temp = '';
    for i=1:r-1
        temp = [temp linmapStr(i,:) ';...' newline];
    end
    temp = [temp linmapStr(r,:) '];' newline];
    linmapStr = ['linmap = [' newline temp];
    
    % preliminary information: which dimensions are needed for N?
    PTN = P'*N;
    requiredforN = ~all(strcmp(string(jacobian(PTN,sym('x',[n,1]))),"0"),1);
    neglectedDimsStr = '';
    temp = string(find(~requiredforN));
    for i=1:length(temp)-1
        neglectedDimsStr = [neglectedDimsStr char(temp(i)) '/'];
    end
    neglectedDimsStr = [neglectedDimsStr char(temp(end))];
    
    % projection of xr to Rn
    % note: evaluate only xr needed for selected nonlinear functions
    Urxr = string(Ur * xrsym);
    UrxrStr = ['Urxr = zeros(' num2str(n) ',1);' newline];
    for i=1:n
        if requiredforN(i)
            UrxrStr = [UrxrStr 'Urxr(' num2str(i) ') = ' char(Urxr(i)) ';' newline]; 
        else
            UrxrStr = [UrxrStr 'Urxr(' num2str(i) ') = 0;' newline]; 
        end
    end
    % replace xr1 with xr(1) etc.
    for j=1:r
        old = ['xr' num2str(j)];
        new = ['xr(' num2str(j) ')'];
        UrxrStr = strrep(UrxrStr,old,new);
    end
    infoStr = ['(Note: The dimensions ' neglectedDimsStr ...
        ' of Urxr are removed because of the projection matrix P' ...
        ' or disregarded because they are not used further.)'];
    
    % 2. evaluation of nonlinear functions
    Urxrsym = sym('Urxr',[n,1]);
    NUrxr = PTN;
    for i=1:n
        NUrxr = subs(NUrxr,xsym(i),Urxrsym(i));
    end
    PTNUrxrStr = ['PTNUrxr = zeros(' num2str(r) ',1);' newline];
    for i=1:r
        PTNUrxrStr = [PTNUrxrStr 'PTNUrxr(' num2str(i) ') = ' char(NUrxr(i)) ';' newline]; 
    end
    % replace Urxr1 with Urxr(1) etc.
    for i=1:n
        old = ['Urxr' num2str(i)];
        new = ['Urxr(' num2str(i) ')'];
        PTNUrxrStr = strrep(PTNUrxrStr,old,new);
    end
    
    nonlinpart = [nonlinpartname ' = linmap * PTNUrxr;' newline];
    
    nonlinStr = ['% 1. Linear mapping (Ur^T W (P^T W)^(-1)):' newline linmapStr newline ...
        '% 2. Mapping of xr to Rn:' newline ...
        '% ' infoStr newline UrxrStr newline ...
        '% 3. Evaluation of nonlinear function:' newline PTNUrxrStr newline ...
        '% Nonlinear part solely in Rr:' newline nonlinpart];
end
% -------------------------------------------------------------------------

diffeqStr = ['dxr = Ar * xr + ' nonlinpartname ';'];

% text for rom models-file
totalStr = [funcStr newline header newline newline ...
    '% Linear part:' newline ArStr newline ...
    '% Nonlinear part:' newline nonlinStr newline ...
    '% Reduced model in Rr:' newline diffeqStr newline newline 'end']; 

% open file - write - close
fid = fopen([path filesep romfilename '.m'],'w');
fwrite(fid, totalStr);
fclose(fid);

% return function handle
romhandle = str2func(romfilename);

end

