function S = and(S1,S2,varargin)
% and - overloads & operator, computes the intersection of a strip with
%       another set
%
% Syntax:  
%    S = and(S1,S2)
%
% Inputs:
%    S1 - strips object
%    S2 - strips-, or zonotope object
%
% Outputs:
%    S - resuting contSet object
%
% Example: 
%    zono = zonotope([0;0],[1 0 1;0 1 1]);
%    S = strips([1 1],1);
%
%    res = S & zono;
%
%    figure; hold on;
%    xlim([-5,5]); ylim([-5,5]);
%    plot(zono,[1,2],'g');   
%    plot(S,[1,2],'b');
%    plot(res,[1,2],'r');
%    xlim([-4,4]); ylim([-4,4]);    
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: mtimes, plus

% Author:        Niklas Kochdumper, Matthias Althoff
% Written:       23-November-2020
% Last update:   02-March-2021 (MA, interface to intersect strip changed)
% Last revision: ---

%------------- BEGIN CODE --------------

    % different set representations
    if isa(S2,'strip')
        
        S = strip([S1.C;S2.C],[S1.d;S2.d],[S1.y;S2.y]);
        
    elseif isa(S2,'zonotope')
        
        % convert to appropriate format
        d = num2cell(S1.d);
        y = num2cell(S1.y);
        
        S = intersectStrip(S2,S1.C,d,y,varargin{:});
        
    elseif isa(S2,'mptPolytope')
        
        S = mptPolytope(S1) & S2;
        
    end     
        
%------------- END OF CODE --------------