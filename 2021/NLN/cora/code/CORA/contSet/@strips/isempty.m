function res = isempty(S)
% isempty - returns true if a strips object is empty and false otherwise
%
% Syntax:  
%    res = isempty(S)
%
% Inputs:
%    S - strips object
%
% Outputs:
%    res - logical
%
% Example: 
%    S1 = strips();
%    isempty(S1); % true
%
%    C = [1 1; 1 -2];
%    d = [1; 0.5];
%    y = [1; 0];     
%    S2 = strips(C,d,y);
%    isempty(S2); % false
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Mark Wetzlinger
% Written:      21-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

res = isempty(S.C) && isempty(S.d);

%------------- END OF CODE --------------