function poly = mptPolytope(obj)
% mptPolytope - convert a strip to a mptPolytope
%
% Syntax:  
%    poly = mptPolytope(obj)
%
% Inputs:
%    obj - strip object
%
% Outputs:
%    poly - mptPolytope object
%
% Example: 
%    S = strips([1 1],1);
%    poly = mptPolytope(S);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: halfspace/mptPolytope

% Author:       Niklas Kochdumper
% Written:      21-November-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % construct constraints
    C = [obj.C; -obj.C];
    d = [obj.d + obj.y; obj.d - obj.y];

    % construct mptPolytope object
    poly = mptPolytope(C,d);

%------------- END OF CODE --------------