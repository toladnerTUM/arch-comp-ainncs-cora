function p = randPoint(obj,varargin)
% randPoint - generates a random point within a polytope
%
% Syntax:  
%    p = randPoint(obj)
%    p = randPoint(obj,N)
%    p = randPoint(obj,N,type)
%    p = randPoint(obj,'all','extreme')
%
% Inputs:
%    obj - mptPolytope object
%    N - number of random points
%    type - type of the random point ('extreme' or 'normal')
%
% Outputs:
%    p - random point in R^n
%
% Example: 
%    poly = mptPolytope.generateRandom(2);
%
%    points = randPoint(poly,100);
%
%    figure; hold on;
%    plot(poly,[1,2],'r');
%    plot(points(1,:),points(2,:),'.k','MarkerSize',10);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/randPoint

% Author:       Niklas Kochdumper
% Written:      30-October-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    N = 1;
    type = 'normal';
    if nargin > 1 && ~isempty(varargin{1})
       N = varargin{1}; 
    end
    if nargin > 2 && ~isempty(varargin{2})
       type = varargin{2}; 
    end
    
    % return all extreme points 
    if ischar(N) && strcmp(N,'all')
        p = vertices(obj); return;
    end
    
    % generate random points
    p = zeros(dim(obj),N);
    
    if strcmp(type,'normal')
        for i = 1:N
            p(:,i) = randPointNormal(obj);
        end
    elseif strcmp(type,'extreme')
        for i = 1:N
            p(:,i) = randPointExtreme(obj);
        end
    else
        [msg,id] = errWrongInput('type');
        error(id,msg);
    end
end


% Auxiliary Functions -----------------------------------------------------

function p = randPointNormal(P)
% generate random point within the polytope

    % draw n+1 random extreme points
    n = dim(P);
    points = zeros(n,n+1);
    
    for i = 1:size(points,2)
       points(:,i) = randPointExtreme(P); 
    end

    % interpolate betwenn the points
    delta = rand(n+1,1);
    delta = delta./sum(delta);
    
    p = points*delta;
end

function p = randPointExtreme(P)
% generate random point on the polytope boundary
        
    % check if vertex representation is available
    if P.P.hasVRep
        
       % select random vertex
       V = P.P.V;
       ind = randi([1,size(V,1)]);
       p = V(ind,:)';
       
    else
       
       % center polytope at origin
       c = center(P);
       P = P - c;
       
       % select random direction
       n = length(c);
       d = rand(n,1) - 0.5*ones(n,1);
       d = d./norm(d);
        
       % compute farest point in this direction that is still in polytope
       [~,x] = supportFunc(P,d);
       p = x + c;
    end
end

%------------- END OF CODE --------------