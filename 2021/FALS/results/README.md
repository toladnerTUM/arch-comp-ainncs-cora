# ARCH-COMP 2021 Category Report: Falsification with Validation of Results

Contact: Gidon Ernst <gidonernst@gmail.com>

This artifact documents the results from the falsification category
of the ARCH 2021 friendly competition.
See also the written report, which appeared in the proceedings of the
ARCH workshop 2021 (easychair/EPiC) at
https://www.easychair.org/publications/paper/F4kf

The results are permanently archived at:
https://zenodo.org/record/5651631

## Dependencies

-   Java   >= 1.8
-   Matlab >= 2017

## Files:

-   `arch2021-falsification-traces.zip`:
    experimental logs with simulation traces and other information;
    as provided by the participants, with some postprocessing

The formats and validation rules are explained here:
<https://gitlab.com/gernst/ARCH-COMP/-/blob/FALS/2021/FALS/Validation.md>

## Instructions to run re-validation:

    # approx 740 MB download
    wget "https://zenodo.org/record/5651631/files/arch2021-falsification-traces.zip?download=1"
    
    # approx   4 GB of data
    unzip "arch2021-falsification-traces.zip?download=1"

    # validate the results
    cd ../falstar
    ./falstar-config.sh # (once)
    ./falstar.sh validate-$TOOL.cfg

Running validation will *extend* the existing logfiles in the results folder.

## Disclaimer:

This effort is the first attempt to establish validation as part of
the falsification category, and the material documented here should
be taken with a grain of salt. Not all results reported by the participants
can be validated perfectly, due to a number of reasons (version mismatch,
inaccuracies in numeric simulation, data conversion, manual data collection
and formatting, and possibly bugs in the toolchain).
No guarantee is given that the numbers are fully "correct",
and no blame should be assigned for any discrepancy found.

*Acknowledgement*: A big thanks goes to the participants for making this effort possible.
