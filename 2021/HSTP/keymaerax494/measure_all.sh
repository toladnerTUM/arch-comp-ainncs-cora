#!/bin/bash

echo "
*******************************************************************************
KeYmaera X 4.9.4 repeatability evaluation.

The repeatability evaluation does not use Wolfram Engine due to an 
incompatibility of JLink with Ubuntu in Wolfram Engine 12.3 (earlier versions 
<=12.1 would be compatible, but are no longer available from Wolfram). The 
repeatability package therefore opts to run solely with Z3; as a consequence, 
Pegasus invariant generation is unavailable and proof automation, especially 
in the nonlinear HSTP sub-category, is severely limited.

The results reported in the paper are obtained on MacOS with Mathematica 12.3 
and full Pegasus invariant generation, including SOSTools in Matlab 2021a.
*******************************************************************************
"

docker build -t keymaerax494:1.0 .
docker create -it --name kyx494 keymaerax494:1.0 bash
docker start kyx494
docker exec -it kyx494 bash -c "mkdir -p /root/.keymaerax"
docker cp ./keymaerax.z3.conf kyx494:/root/.keymaerax/keymaerax.conf
docker exec -it -w /root/arch2021/ kyx494 java -da -jar keymaerax_494.jar -launch -setup

mkdir -p results

# end-of-year versions since 2017
z3versions=(z3,4.8.10,,18.04 z3,4.8.7,,16.04 Z3,4.8.4,.d6df51951f4c,16.04, z3,4.6.0,,16.04)

for i in "${z3versions[@]}"; do
    docker exec -it kyx494 bash -c "rm /root/.keymaerax/z3"
    IFS=',' read z3fp z3v z3m ubuntuv <<< "${i}"
    docker exec -it kyx494 bash -c "wget https://github.com/Z3Prover/z3/releases/download/$z3fp-$z3v/z3-$z3v$z3m-x64-ubuntu-$ubuntuv.zip; unzip z3-$z3v$z3m-x64-ubuntu-$ubuntuv.zip; cp ./z3-$z3v$z3m-x64-ubuntu-$ubuntuv/bin/z3 /root/.keymaerax"
    docker exec -it -w /root/arch2021 kyx494 bash "./runKeYmaeraX494Benchmarks"
    docker exec -w /root/arch2021 kyx494 bash -c "mkdir -p results; mv *.csv results"
    mkdir -p results/$z3v
    docker cp kyx494:/root/arch2021/results ./results/$z3v
done

docker rm -f kyx494
