#!/bin/bash

docker build . -t "dummy-submit-$1" --no-cache
docker run -t -w /root/mars --name "dummy-submit-container-$1" "dummy-submit-$1" python3.9 runEvaluations.py
docker cp "dummy-submit-container-$1":root/mars/results/ .
docker rm --force "dummy-submit-container-$1"
docker image rm --force "dummy-submit-$1"