classdef nnGNNGlobalPoolingLayer < nnGNNLayer
% nnGNNGlobalPoolingLayer - class for global pooling gnn layer
%
% Syntax:
%    obj = nnGNNGlobalPoolingLayer(type, name)
%
% Inputs:
%    type - type of global pooling, one of {'add', 'mean'}
%    name - name of the layer, defaults to type
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tianze Huang, Gerild Pjetri, Tobias Ladner
% Written:      22-December-2022
% Last update:  23-February-2023 (TL: clean up)
% Last revision:--- 

%------------- BEGIN CODE --------------

properties (Constant)
    is_refinable = false
end

properties
    type
end

methods
    % constructor
    function obj = nnGNNGlobalPoolingLayer(type, varargin)
        % parse input
        if nargin > 2
            throw(CORAerror('CORA:tooManyInputArgs', 2))
        end
        name = setDefaultValues({[]}, varargin);
        inputArgsCheck({{type, 'str', {'add', 'mean'}}})

        % call super class constructor
        obj@nnGNNLayer(name)
        obj.type = type;
    end

    function outputSize = getOutputSize(obj, inputSize, G)
        if nargin < 3
            nrNodes = 1;
        else
            nrNodes = obj.computeNrNodes(G);
        end  
        outputSize = [inputSize(1)/nrNodes, 1];
    end

    function [nin, nout] = getNumNeurons(obj)
        nin = [];
        nout = [];
    end
end

% evaluate ----------------------------------------------------------------

methods(Access = {?nnLayer, ?neuralNetwork})

    % numeric
    function r = evaluateNumeric(obj, input, evParams)
        % construct matrix
        inputDim = size(input, 1);
        graph = evParams.graph;
        if obj.type == "add"
            M = computeMatrixSumNodeWise(obj, graph, inputDim);
        elseif obj.type == "avg"
            M = computeMatrixSumNodeWise(obj, graph, inputDim);
            M = M * 1/computeNrNodes(obj, graph);
        end 

        % propagate
        r = M * input;
    end

    % sensitivity
    function S = evaluateSensitivity(obj, S, x, evParams)
        inputDim = size(x, 1);
        graph = evParams.graph;
        if obj.type == "add"
            M = computeMatrixSumNodeWise(obj, graph, inputDim);
        elseif obj.type == "avg"
            M = computeMatrixSumNodeWise(obj, graph, inputDim);
            M = M * 1/computeNrNodes(obj, graph);
        end 
        S = S * M;
    end

    % zonotope/polyZonotope
    function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        % construct matrix
        inputDim = size(c, 1);
        graph = evParams.graph;
        if obj.type == "add"
            M = computeMatrixSumNodeWise(obj, graph, inputDim);
        elseif obj.type == "avg"
            M = computeMatrixSumNodeWise(obj, graph, inputDim);
            M = M * 1/computeNrNodes(obj, graph);
        end 

        % propagate
        c = M * c;
        G = M * G;
        Grest = M * Grest; 
    end
end

% Auxiliary functions -----------------------------------------------------

methods(Access=protected)
    function M = computeMatrixSumNodeWise(obj, graph, inputDim)
        nrNodes = obj.computeNrNodes(graph);
        nrFeatures = inputDim/nrNodes;
        M = kron(ones(1, nrNodes),speye(nrFeatures));
    end
end

end

%------------- END OF CODE --------------