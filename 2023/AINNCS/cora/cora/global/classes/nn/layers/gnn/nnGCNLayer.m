classdef nnGCNLayer < nnGNNLayer
% nnGCNLayer - class for graph convolutional layer (gcn)
%
% Syntax:
%    obj = nnGCNLayer(W, b, name)
%
% Inputs:
%    W - weight matrix
%    b - bias matrix
%    name - name of the layers
%
% Outputs:
%    obj - generated GCNlayer
%
% References:
%    [1] Kipf, T. N., et al. (2016). Semi-supervised classification with 
%        graph convolutional networks. arXiv preprint arXiv:1609.02907.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork

% Author:       Gerild Pjetri, Tianze Huang, Tobias Ladner
% Written:      13-December-2022
% Last update:  23-February-2023
% Last revision:15-January-2023

%------------- BEGIN CODE --------------

properties (Constant)
    is_refinable = false
end

properties
   W , b
end

methods
    % constructor
    function obj = nnGCNLayer(W, varargin)
        % parse input
        if nargin < 1
            throw(CORAerror('CORA:notEnoughInputArgs', 1))
        elseif nargin > 3
            throw(CORAerror("CORA:tooManyInputArgs", 3))
        end
        [b, name] = setDefaultValues({0, []}, varargin);
        inputArgsCheck({ ...
            {W, 'att', 'numeric'}; ...
            {b, 'att', 'numeric'}; ...
        })

        % check dimensions
        if length(b) == 1
            b = b * ones(size(W, 1), 1);
        end
        if ~all(size(b, 1) == size(W, 1))
           throw(CORAerror('CORA:wrongInputInConstructor', ...
               'The dimensions of W and b should match.'));
        end
        if size(b, 2) ~= 1
           throw(CORAerror('CORA:wrongInputInConstructor', ...
               "Second input 'b' should be a column vector."));
        end        

        % call super class constructor
        obj@nnGNNLayer(name)
        obj.W = W;
        obj.b = b;
    end

    function outputSize = getOutputSize(obj, inputSize, G)
        if nargin < 3
            nrNodes = 1;
        else
            nrNodes = obj.computeNrNodes(G);
        end      
        nrOutFeatures = size(obj.W,1);
        outputSize = [nrNodes*nrOutFeatures, 1];
    end

    function [nin, nout] = getNumNeurons(obj)
        nin = size(obj.W, 2);
        nout = size(obj.W, 1);
    end
end

% evaluate ----------------------------------------------------------------

methods(Access = {?nnLayer, ?neuralNetwork})

    % numeric
    function r = evaluateNumeric(obj, input, evParams)        
        % message passing
        [MP, bv] = computeMessagePassing(obj, evParams.graph);
        r = MP * input + bv;
        
        % make it full again
        r = full(r);
    end

    % sensitivity
    function S = evaluateSensitivity(obj, S, x, evParams)
        [MP, ~] = computeMessagePassing(obj, evParams.graph);
        S = S * MP;
    end

    % zonotope/polyZonotope 
    function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        % message passing
        [MP, bv] = computeMessagePassing(obj, evParams.graph);
        c = MP * c + bv;
        G = MP * G;
        Grest = MP * Grest;
        
        % make it full again
        c = full(c);
        G = full(G);
        Grest = full(Grest);
    end
end

% Auxiliary functions -----------------------------------------------------

methods (Access=protected)

    function [MP, bv] = computeMessagePassing(obj, G)
        % compute vectorized message passing

        % init
        nrNodes = computeNrNodes(obj, G);
        nrOutFeatures = size(obj.W,1);

         % compute vectorized D^(-1/2)*A*D^(-1/2)
        A = G.adjacency;
        D = sqrtm(inv(diag(sum(A)))); % D^(-1/2)
        DAD = kron((D*A*D)', speye(nrOutFeatures));

        % compute vectorized W
        Wv = kron(speye(nrNodes), obj.W);

        % message passing
        MP = DAD * Wv;

        % bias
        bv = kron(ones(nrNodes,1),obj.b);
    end

end

end

%------------- END OF CODE --------------