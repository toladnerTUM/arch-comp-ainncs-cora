function [coeffs, d] = computeApproxPoly(obj, l, u, varargin)
% computeApproxPoly - computes an approximating polynomial and respective
%   error bound
%
% Syntax:
%    [coeffs, d] = computeApproxPoly(obj, l, u, order, poly_method)
%
% Inputs:
%    obj - nnActivationLayer
%    l,u - bounds to compute the polynomial within
%    order - polynomial order
%    poly_method -  how approximation polynomial is found in nonlinear
%        layers, e.g. 'regression', 'hand-crafted', ... 
%
% Outputs:
%    obj - generated object
%
% References:
%    [1] Kochdumper, Niklas, et al. "Open-and closed-loop neural network 
%       verification using polynomial zonotopes." 
%       arXiv preprint arXiv:2207.02715 (2022).
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralNetwork

% Author:       Tobias Ladner
% Written:      16-February-2023
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% parse input
if nargin < 3
    throw(CORAerror('CORA:notEnoughInputArgs', 3))
elseif nargin > 5
    throw(CORAerror('CORA:tooManyInputArgs', 5))
end
[order, poly_method] = setDefaultValues({1, 'regression'}, varargin);

validPolyMethods = {'regression', 'ridgeregression', ...
    'throw-catch', 'taylor', 'hand-crafted'};
inputArgsCheck({ ...
    {l, 'att', 'numeric', 'scalar'}, ...
    {u, 'att', 'numeric', 'scalar'}, ...
    {order, 'att', 'numeric', 'scalar'}, ...
    {poly_method, 'str', validPolyMethods} ...
})

% trivial case
if l == u
    coeffs = [0, obj.f(l)];
    d = 0;
    return
elseif l > u
    throw(CORAerror("CORA:wrongValue", "second/third", 'l <= u'))
end

% init
coeffs = [];
d = [];

% compute approximation polynomial

% use at least 10 points within [l, u] for regression
x = linspace(l, u, max(order+1, 10));
y = obj.f(x);

if strcmp(poly_method, "regression")
    % compute polynomial that best fits the activation function
    coeffs = nnHelper.leastSquarePolyFunc(x, y, order);

elseif strcmp(poly_method, "ridgeregression")
    coeffs = nnHelper.leastSquareRidgePolyFunc(x, y, order);

else
    % check custom computation in subclass
    [coeffs, d] = computeApproxPolyCustom(obj, l, u, order, poly_method);
end
   
% parse coeffs and d
if isempty(coeffs)
    throw(CORAerror('CORA:nnLayerNotSupported', obj, ...
        sprintf("'%s' for polynomial of order=%d", poly_method, order)))
elseif ~isempty(d)
    % approximation error already found
    return
end

[coeffs, d] = computeApproxError(obj, l, u, coeffs);

end

%------------- END OF CODE --------------