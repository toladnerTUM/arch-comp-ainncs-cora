classdef (Abstract) nnGNNLayer < nnLayer
% nnGNNLayer - abstract class for nn layers
%
% Syntax:
%    layer = nnGNNLayer(name)
%
% Inputs:
%    name - name of the layer, defaults to type
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: validateEvaluateParams, evParams.graph

% Author:       Gerild Pjetri, Tianze Huang, Tobias Ladner
% Written:      13-December-2022
% Last update:  23-February-2023 (TL: integrate in nnLayer)
% Last revision:15-January-2023

%------------- BEGIN CODE --------------

methods
    function obj = nnGNNLayer(varargin)
        obj@nnLayer(varargin{:})
        obj.inputSize = [];
    end
end

methods (Abstract)
    [nin, nout] = getNumNeurons(obj, graph)
    outputSize = getOutputSize(obj, inputSize, graph)
end

% Auxiliary functions -----------------------------------------------------

methods (Access=protected)
    function nrNodes = computeNrNodes(obj, graph)
        nrNodes = height(graph.Nodes);
    end
end

end

%------------- END OF CODE --------------