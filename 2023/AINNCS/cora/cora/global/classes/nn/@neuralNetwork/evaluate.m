function r = evaluate(obj, input, varargin)
% evaluate - compute the output of a neural network for the given input
%
% Syntax:
%    res = evaluate(obj, input)
%    res = evaluate(obj, input, evParams)
%
% Inputs:
%    obj - object of class neuralNetwork
%    input - input represented as a numeric or set
%    evParams - struct holding parameters for set-based evaluation
%       .bound_approx: bool whether bounds should be overapproximated,
%           or "sample" (not safe!)
%       .reuse_bounds: wheter bounds should be reused
%       .poly_method: how approximation polynomial is found in nonlinear
%           layers, e.g. 'regression', 'hand-crafted', 'taylor' ...
%       .num_generators: max number of generators for order reduction
%       .add_approx_error_to_Grest: whether 'd' should be added to Grest
%       .plot_multi_layer_approx_info: plotting for nnApproximationLayer
%       .max_bounds: max order used in refinement
%       .do_pre_order_reduction: wheter to do a order reduction before
%           evaluating the pZ using the polynomial
%       .max_gens_post: max num_generator before post order reduction
%       .remove_Grest: whether to restructue s.t. there remain no ind. gens
%       .force_approx_lin_at: (l, u) distance at which to use 'lin'
%           instead of respective order using 'adaptive'
%       .sort_exponents: whether exponents should be sorted
%       .propagate_bounds: whether bounds should be propagated to next
%           activation layer using interval arithmetic
%       .maxpool_type: for set-based prediction, 'project' or 'regression'
%       .G: graph object used for graph neural networks
%   idxLayer - indices of layers that should be evaluated
%
% Outputs:
%    res - output of the neural network
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralNetwork, nnHelper/validateEvaluateParams

% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  29-November-2022 (validateEvaluateParams)
%               16-February-2023 (re-organized structure)
% Last revision:---

%------------- BEGIN CODE --------------

if nargin < 2
    throw(CORAerror('CORA:notEnoughInputArgs', 2));
elseif nargin > 5
    throw(CORAerror('CORA:tooManyInputArgs', 5));
end

% validate parameters
[evParams, idxLayer] = setDefaultValues( ...
    {struct, 1:length(obj.layers)}, varargin);

% validate input
inputArgsCheck({ ...
    {obj, 'att', 'neuralNetwork'}; ...
    {input, 'att', {'numeric', 'interval', 'zonotope', 'polyZonotope', ...
                    'taylm', 'conZonotope'}}; ...
    {evParams, 'att', 'struct'}; ... 
    {idxLayer, 'att', 'numeric', 'vector'};
})
evParams = nnHelper.validateEvaluateParams(evParams);

isZonotope = isa(input, 'zonotope');
if isZonotope
    % reuse polyZonotope functions
    input = polyZonotope(center(input), [], generators(input), []);
    % only use independent generators
    evParams.add_approx_error_to_Grest = true;
    evParams.remove_Grest = false;
end

% execute -----------------------------------------------------------------

if isnumeric(input)
    % numeric
    r = input;
    for i = idxLayer
        evParams.i = i;
        layer_i = obj.layers{i};
        r = layer_i.evaluateNumeric(r, evParams);
    end

elseif isa(input, 'interval')
    % interval
    r = input;
    for i = idxLayer
        layer_i = obj.layers{i};
        r = layer_i.evaluateInterval(r, evParams);
    end

elseif isa(input, 'polyZonotope')
    % zonotope/polyZonotope
    c = input.c;
    G = input.G;
    Grest = input.Grest;
    expMat = input.expMat;
    id = input.id;
    id_ = max(id);
    if isempty(G)
        G = zeros(size(c, 1), 0);
    end
    if isempty(Grest)
        Grest = zeros(size(c, 1), 0);
    end
    ind = find(prod(ones(size(input.expMat))-mod(input.expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(input.expMat, 2), ind);

    for i = idxLayer
        evParams.i = i;
        layer_i = obj.layers{i};
        [c, G, Grest, expMat, id, id_, ind, ind_] = ...
            layer_i.evaluatePolyZonotope(c, G, Grest, expMat, id, id_, ind, ind_, evParams);
        obj.propagateBounds(i, evParams);
    end

    r = polyZonotope(c, G, Grest, expMat, id);

elseif isa(input, 'taylm')
    % taylm
    r = input;
    for i = idxLayer
        evParams.i = i;
        layer_i = obj.layers{i};
        r = layer_i.evaluateTaylm(r, evParams);
    end

elseif isa(input, 'conZonotope')
    % conZonotope

    % convert constrained zonotope to star set
    [c, G, C, d, l, u] = nnHelper.conversionConZonoStarSet(input);

    % predefine options for linear programming for speed-up
    options = optimoptions('linprog', 'display', 'off');

    for i = idxLayer
        evParams.i = i;
        layer_i = obj.layers{i};
        [c, G, C, d, l, u] = ...
                layer_i.evaluateConZonotope(c, G, C, d, l, u, options, evParams);
    end
    % convert star set to constrained zonotope
    r = nnHelper.conversionStarSetConZono(c, G, C, d, l, u);

else
    throw(CORAerror('CORA:notSupported',...
        ['Set representation ' class(input) ' is not supported.']));
end

if isZonotope
    % transform back to zonotope
    r = zonotope(r);
end

end

%------------- END OF CODE --------------