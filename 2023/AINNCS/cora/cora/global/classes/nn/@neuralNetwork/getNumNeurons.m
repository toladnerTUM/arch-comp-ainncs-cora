function numNeurons = getNumNeurons(obj)
% getNumNeurons - returns the number of neurons per layer
%
% Syntax:
%    pattern = getNumNeurons(obj)
%
% Inputs:
%    obj - object of class neuralNetwork
%
% Outputs:
%    pattern - list
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tobias Ladner
% Written:      26-August-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

numNeurons = zeros(1, size(obj.layers, 1)+1);

for i = 1:length(obj.layers)    
    if isempty(obj.layers{i}.inputSize)
        % try to set input size for all layers
        obj.setInputSize();
    end

    numNeurons(i) = prod(obj.layers{i}.inputSize);
end

numNeurons(end) = obj.neurons_out;

end

%------------- END OF CODE --------------