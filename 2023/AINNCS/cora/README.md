# ARCH-COMP 2023: AINNCS Category

<img title="CORA" alt="CORA - A Tool for Continuous Reachability Analysis" src="./CoraLogo.png"/>

A Tool for Continuous Reachability Analysis

The COntinuous Reachability Analyzer (CORA) is a collection of MATLAB classes for the formal verification of cyber-physical systems using reachability analysis:
https://tumcps.github.io/CORA/

## Installation
This folder contains the code as well as a docker file to run the AINNCS benchmarks in one click.

However, you need to provide a MATLAB Licence file `license.lic` to run the code:
- Create a MATLAB License file: 
	For the docker container to run MATLAB, one has to create a new license file for the container.
	Log in with your MATLAB account at https://www.mathworks.com/licensecenter/licenses/
	Click on your license, and then navigate to
	1. "Install and Activate"
	1. "Activate to Retrieve License File"
	1. "Activate a Computer"
	(...may differ depending on how your licensing is set up).
- Choose:
	- Release: `R2022a`
	- Operating System: `Linux`
	- Host ID: `0242AC110002` (= Default MAC-Adress of Docker Container) | your host MAC Adress
	- Computer Login Name: `matlab`
	- Activation Label: `<any name>`
- Download the file and place it next to the docker file

If you are having trouble, don't hesitate contacting us: <a href="mailto:tobias.ladner@tum.de">tobias.ladner@tum.de</a>

## Run the code

You can run all benchmarks in one click in a docker container using the `submit.sh` script.

	./submit.sh
	
The results will be stored to `./results`.


	
Alternatively, open this directory in MATLAB and run `run.m`.

	run()
	
Note that all required toolboxes have to be installed first (see <a href="https://tumcps.github.io/CORA/">CORA Manual</a>):

- Multi Parametric Toolbox (MPT)
- YALMIP Toolbox
- Symbolic Math Toolbox
- Optimization Toolbox
- Statistics Toolbox
- Multiple Precision Toolbox
- Deep learning Toolbox
- Deep Learning Toolbox Converter for ONNX Model Format

## Tool comparisons and plots

Note that the total time shown in `./results/results.csv` includes the time for verification as well as tries to violate a benchmark (see <a href="https://cps-vo.org/node/84803#comment-1301" target="_blank">forum discussion</a>), as shown in the example below.
For better comparison in the report, the times for each subtask is shown in the `./results/results.txt`.
	
	BENCHMARK: Adaptive Cruise Controller (ACC)
	Time to compute random simulations: 0.96427
	Time to check violation in simulations: 0.01122
	Time to compute reachable set: 2.3899
	Time to check verification: 0.049235
	Total Time: 3.4146
	Result: VERIFIED

Additionally, we provide the results from a reference run on our machine in `results_ref`.

## References

- [1] Althoff, M. (2015). An introduction to CORA 2015. In Proc. of the workshop on applied verification for continuous and hybrid systems (pp. 120-151).
- [2] Kochdumper, N., Schilling, C., Althoff, M., & Bak, S. (2022). Open- and closed-loop neural network verification using polynomial zonotopes. arXiv preprint arXiv:2207.02715.
- [3] Ladner, T., Althoff, M. (2023). Automatic abstraction refinement in neural network verification using sensitivity analysis. In Proc. of the 26th ACM International Conference on Hybrid Systems: Computation and Control, 2023.


