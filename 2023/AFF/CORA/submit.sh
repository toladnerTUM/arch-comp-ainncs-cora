#!/bin/bash

docker build . -t "dummy-submit-$1" --no-cache
docker run --mac-address 02:42:ac:11:00:0b -e MLM_LICENSE_FILE=/MATLAB/licenses/network.lic --name "dummy-submit-container-$1" "dummy-submit-$1" ./run

docker cp "dummy-submit-container-$1":/results/ .
docker rm --force "dummy-submit-container-$1"
docker image rm --force "dummy-submit-$1"