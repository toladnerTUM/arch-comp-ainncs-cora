An interval matrix is a special case of a matrix zonotope and specifies the interval of possible values for each matrix element:
\begin{equation*}
	\mathcal{A}_{[i]}=[\underline{A},\overline{A}], \quad  \forall i,j: \underline{a}_{ij}\leq\overline{a}_{ij}, \quad \underline{A},\overline{A}\in\mathbb{R}^{n \times n}.
\end{equation*}
The matrix $\underline{A}$ is referred to as the \textit{lower bound} and $\overline{A}$ as the \textit{upper bound} of $\mathcal{A}_{[i]}$. 

We support the following methods for interval matrices:
\begin{itemize}
 \item \texttt{abs} -- returns the absolute value bound of an interval matrix.
 \item \texttt{dependentTerms} -- considers dependency in the computation of Taylor terms for the interval matrix exponential according to \cite[Proposition 4.4]{Althoff2011b}. 
 \item \texttt{display} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{dominantVertices} -- computes the dominant vertices of an interval matrix zonotope according to a heuristics.
 \item \texttt{exactSquare} -- computes the exact square of an interval matrix.
 \item \texttt{expm} -- operator for the exponential matrix of an interval matrix, evaluated dependently.
 \item \texttt{expmAbsoluteBound} -- returns the over-approximation of the absolute bound
of the symmetric solution of the computation of the exponential matrix.
 \item \texttt{expmInd} -- operator for the exponential matrix of an interval matrix, evaluated independently.
 \item \texttt{expmIndMixed} -- dummy function for interval matrices.
 \item \texttt{expmMixed} -- dummy function for interval matrices.
 \item \texttt{expmNormInf} -- returns the over-approximation of the norm of the difference between the interval matrix exponential and the exponential from the center matrix according to \cite[Theorem 4.2]{Althoff2011b}.
 \item \texttt{expmVertex} -- computes the exponential matrix for a selected number of dominant vertices obtained by the \texttt{dominantVertices} method.
 \item \texttt{exponentialRemainder} -- returns the remainder of the exponential matrix according to \cite[Proposition 4.1]{Althoff2011b}.
 \item \texttt{interval} -- converts an interval matrix to an interval.
 \item \texttt{intervalMatrix} -- constructor of the class.
 \item \texttt{matPolytope} -- converts an interval matrix to a matrix polytope.
 \item \texttt{matZonotope} -- converts an interval matrix to a matrix zonotope.
 \item \texttt{mpower} -- overloaded '${}^{\wedge}$' operator for the power of interval matrices.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations} for numeric matrix multiplication or a multiplication with another interval matrix according to \cite[Equation 4.11]{Althoff2011b}.
 \item \texttt{norm} -- computes exactly the maximum norm value of all possible matrices.
 \item \texttt{plot} -- plots 2-dimensional projection of an interval matrix.
 \item \texttt{plus} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}. Addition is realized for two
interval matrices or an interval matrix with a matrix.
 \item \texttt{powers} -- computes the powers of an interval matrix up to a certain order.
 \item \texttt{randomSampling} -- creates random samples within a matrix zonotope.
 \item \texttt{randomIntervalMatrix} -- generates a random interval matrix with a specified center and a specified delta matrix or scalar. The number of elements of that matrix which are uncertain has to be specified, too.
 \item \texttt{subsref} -- overloads the operator that selects elements, e.g., A(1,2), where the element of the first row and second column is referred to.
 \item \texttt{vertices} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{volume} -- computes the volume of an interval matrix by computing the volume of the corresponding interval.
\end{itemize}


\subsubsection{Interval Matrix Example} \label{sec:intervalMatrixExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/intervalMatrix_test.tex}}

This produces the workspace output
\begin{verbatim}
dimension: 
     2

left limit: 
   -1.0000    3.5000
    3.5000    2.0000

right limit: 
    1.0000    4.5000
    6.5000    4.0000

dimension: 
     2

left limit: 
    1.0000   -3.0000
   -0.5000   -3.0000

right limit: 
    5.0000    3.0000
   10.5000    7.0000

dimension: 
     2

nr of generators: 
     3

center: 
     1     2
     3     4

generators: 
     1     0
     0     0

---------------
     0     0
     1     0

---------------
     0     0
     0     1

---------------
\end{verbatim}