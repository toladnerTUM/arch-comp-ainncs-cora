function [genLen] = generatorLength(Z)
% generatorLength - Returns the lengths of the generators
%
% Syntax:  
%    [genLen] = generatorLength(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%    genLen - vector of generator length
%
% Example: ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      19-July-2010
% Last update:  14-March-2019
% Last revision:---

%------------- BEGIN CODE --------------

genLen=vecnorm(Z.Z(:,2:end));

%------------- END OF CODE --------------