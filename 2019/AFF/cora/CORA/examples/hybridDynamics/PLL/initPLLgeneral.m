function [options] = initPLLgeneral(minPhase, maxPhase, options)
% updated: 22-December-2010, MA


%initial state
R0ih=interval([0.34; -0.01; -0.01; minPhase], [0.36; 0.01; 0.01; maxPhase]);
                       
R0{1}=zonotope(R0ih);    
%options.R0=zonotopeBundle(R0);
options.R0=R0{1};
                        
%goal region
options.Rlock=interval([-100; -100; -100; -1/3600],[100; 100; 100; 1/3600]); 
options.Rgoal{1}=interval([-0.03; -0.2; -0.2; -0.01], [0.03; 0.2; 0.2; 0.01]);  
options.Rgoal{2}=interval([-0.5; -0.5; -100; 0],[0.5; 0.5; 100; 1]);               
  
%goal region
options.RgoalSim=options.Rgoal{1};                                   
                        
%invariant regions
IHinv_noSat=interval([-0.5; -0.5; -100; -1],[0.35; 0.35; 100; 0]); 
IHinv_intSat=interval([0.3; -0.5; -100; -1],[0.5; 0.35; 100; 0]);                                            
IHinv_propSat=interval([-0.5; 0.3; -100; -1],[0.35; 0.5; 100; 0]);                     
IHinv_bothSat=interval([0.3; 0.3; -100; -1], [0.5; 0.5; 100; 0]);  
       
options.Rinv{1} = IHinv_noSat;       
options.Rinv{2} = IHinv_intSat; 
options.Rinv{3} = IHinv_propSat; 
options.Rinv{4} = IHinv_bothSat; 

for iSet = 1:length(options.Rinv)
    options.Pinv{iSet} = polytope(zonotope(options.Rinv{iSet}));
end


%guard regions
%it is assumed that proportional saturation occurs first
IHguard_noSat=interval([-0.5; 0.35; -100; -1],[0.5; 0.36; 100; 0]);   
                       
%is irrelevant                       
IHguard_intSat=IHguard_noSat;                        
                       
%it is assumed that stauration of both paths occurs next
IHguard_propSat=interval([0.35; -0.5; -100; -1],[0.36; 0.5; 100; 0]);
     
%is irrelevant  
IHguard_bothSat=interval([0.5; 0.5; -100; -1],[0.51; 0.51; 100; 0]);   

options.Rguard{1} = zonotope(IHguard_noSat);       
options.Rguard{2} = zonotope(IHguard_intSat); 
options.Rguard{3} = zonotope(IHguard_propSat); 
options.Rguard{4} = zonotope(IHguard_bothSat); 