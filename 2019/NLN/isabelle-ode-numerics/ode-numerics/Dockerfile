FROM ubuntu:18.04

# Install requirements
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install curl mercurial libfontconfig1 gnuplot

# Install isabelle
RUN hg clone http://isabelle.in.tum.de/repos/isabelle -r 3374d16efc61
RUN ./isabelle/bin/isabelle components -I
RUN ./isabelle/bin/isabelle components -a
RUN mkdir -p /root/.isabelle/etc

# install AFP
RUN hg clone https://bitbucket.org/isa-afp/afp-devel -r 069cc96a9c7d

# cause file output
RUN sed -i.orig -r 's/\(\*.*(".*").*\*\).*""/\1/g' \
    /afp-devel/thys/Ordinary_Differential_Equations/Ex/Examples_ARCH_COMP.thy

# build base image
RUN echo ML_system_64 = "true" > /root/.isabelle/etc/preferences
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b Pure
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b HOL
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b HOL-Analysis
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b Ordinary_Differential_Equations
RUN ./isabelle/bin/isabelle build -d afp-devel/thys -b HOL-ODE-Numerics

CMD ./isabelle/bin/isabelle build -d afp-devel/thys -b HOL-ODE-ARCH-COMP && \
  cd afp-devel/thys/Ordinary_Differential_Equations/Ex && \
  ./plot_arch_comp && \
  cp *.pdf /result
